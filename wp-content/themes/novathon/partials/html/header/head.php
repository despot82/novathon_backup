<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=0, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <title>Novathon #withPBZ – 23-24 September 2017, Zagreb</title>
    <meta name="description" content="Novathon #withPBZ is a 24-hour innovative marathon to create cutting-edge applications for responsive, mobile and branch banking and develop new banking solutions.">
    <meta name="keywords" content="Contextual Banking service, Augmented / Virtual reality service, Digital Branch, Digital Wallets & Money, Smart Mobile Banking APP service, team, contest, Bank, 24 hour">
    <!-- css -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,300italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()) ?>/css/style.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()) ?>/css/jquery.fancybox.css" type="text/css">
    <!-- js -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript">
      var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
    </script>
  </head>
