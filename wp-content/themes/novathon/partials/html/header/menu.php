<?php
$wall = Bitmama\Wordpress\Plugin\ResponsiveWalls\Template\Wall::loadFromPage(get_the_ID());
$bricks = $wall->loadBricks();
$loggedIn = is_user_logged_in();
$currentUser = wp_get_current_user();
$sub = new Wordpress\ORM\Subscriber();
$novathonUser = $sub->get_user_by_wordpress_user_id($currentUser->ID);

global $isSubscriptionsPage;

if (!$novathonUser)
{
    $loggedIn = false;
}
?>
  <nav class="header-menu">
    <ul>
      <?php if (is_front_page()): ?>
        <?php if (count($bricks && $bricks)): ?>
          <?php foreach ($bricks as $brick): ?>
            <?php
            $show_in_menu = $brick->shouldShowInMenu();
            $menu_title = $brick->getMenuTitle();
            $menu_anchor = $brick->getMenuAnchor();
            ?>
              <?php if ($show_in_menu && $menu_title): ?>
                <li>
                  <a class="j-scrollLink" href="#<?php echo $menu_anchor ?>">
                    <?php echo $menu_title; ?>
                  </a>
                </li>
              <?php endif; ?>
          <?php endforeach; ?>
        <?php endif; ?>
      <?php else:?>
        <li>
          <a class="" href="<?php echo get_home_url();?>/#event">
            <?php echo __("Event");?>
          </a>
        </li>
        <li>
          <a class="" href="<?php echo get_home_url();?>/#challenges">
            <?php echo __("Challenges");?>
          </a>
        </li>
        <li>
          <a class="" href="<?php echo get_home_url();?>/#speakers-and-mentors">
            <?php echo __("Speakers and Mentors");?>
          </a>
        </li>
        <li>
          <a class="" href="<?php echo get_home_url();?>/#program">
            <?php echo __("Program");?>
          </a>
        </li>
        <li>
          <a class="" href="<?php echo get_home_url();?>/#partners">
            <?php echo __("Partners");?>
          </a>
        </li>
      <?php endif; ?>

      <?php if (is_page_template('page-templates/wall.php') && !is_front_page()): ?>
        <li>
          <a class="active" href="<?php echo get_home_url();?>/previous-event">
            <?php echo __("Previous events");?>
          </a>
        </li>
      <?php else:?>
        <li>
          <a class="" href="<?php echo get_home_url();?>/previous-event">
            <?php echo __("Previous events");?>
          </a>
        </li>
      <?php endif; ?>

      <?php if ($loggedIn) { ?>
        <li class="profileLog">
          <?php if ($novathonUser->profile_image_url != "") { ?>
          <img src="<?php echo $novathonUser->profile_image_url ?>" alt="profile picture">
          <?php } ?>
          <span><a title="go to profile" href="/profile"><?php echo $novathonUser->name ?></a></span>
          <a href="/logout"><span class="icon-first-step"></span></a>
        </li>
      <?php } else { ?>
        <li class="loginButton"><a href="/login"><?php echo __("Login"); ?></a></li>
        <li class="signUpButton"><a href="/subscribe"><?php echo __("Registration"); ?></a></li>
      <?php } ?>
      <?php if (!$isSubscriptionsPage) { require get_template_directory() . '/partials/html/header/lang.php'; } ?>

    </ul>
  </nav>