<?php

use \Bitmama\Wordpress\Plugin\ResponsiveWalls\Definitions\BrickDefinition;

global $sitepress;
$current_lang = 'en';
if ($sitepress) {
  $current_lang = strtolower($sitepress->get_current_language());
}
?>

<body>
  <!-- Facebook Pixel Code -->
  <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window,document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '185399558616973');
  fbq('track', 'PageView');
  </script>

  <noscript>
   <img height="1" width="1" src="https://www.facebook.com/tr?id=185399558616973&ev=PageView&noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->

  <div class="wrapper <?php echo $current_lang ?>">
    <header id="header">
      <div class="bg"></div>
      <div class="content__wrapper">
        <div class="logo">
					<a href="<?php echo get_option('home') ?>">
          <img src="<?php echo get_option('_bitnovathon_header_logo'); ?>" alt="Novathon"></a>
        </div>
        <div class="mobile-toggle">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <!--?php if (is_page_template('page-templates/wall.php') || is_page_template('page-templates/privacy.php') || is_page_template('page-templates/media.php')): ?-->

        <!--?php endif; ?-->
				<!-- tolgo l'if per avere menu ovunque -->
				<?php require get_template_directory() . '/partials/html/header/menu.php'; ?>
      </div>
    </header>
