<li class="lang">
  <select onchange="window.location.href = this.value">
    <?php $langs = icl_get_languages('skip_missing=1'); ?>
    <?php foreach ($langs as $code => $lang): ?>
      <option value="<?php echo $lang['url'] ?>"<?php if ($lang['active']): ?> selected<?php endif; ?>><?php echo $code ?></option>
    <?php endforeach; ?>
  </select>
	<span><a class="color--orange" href="/en/">EN</a>&nbsp;<a href="/hr/">HR</a></span>
</li>
