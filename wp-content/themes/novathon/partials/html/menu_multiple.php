<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$individual_hp_url=get_home_url();
//indica se dobbiamo controllare quale menu utili
$multiple_menu = FALSE;
$section = get_field("bit_multi_section_type");
if ($section && !empty($section)) {
  $multiple_menu = TRUE;
  // indica il menu da utilizzare
  $menu_name = sprintf("hambuger_%s", $section);
//determino la versione del menu per il select
  $business = $section === 'business';
  $individual = !$business;

  $individual_hp_url = bit_get_localized_page_url('individual_home_page');
  $business_hp_url = bit_get_localized_page_url('business_home_page');
} else {
  $menu_name = "hambuger_individual";
}



$service_prefix = "";
$is_multilang = function_exists('icl_object_id');
$languages = array();
if ($is_multilang) {
  global $sitepress;
  if ($sitepress) {
//    $current_lang = $sitepress->get_current_language();
    $default_lang = $sitepress->get_default_language();
    $current_lang = $sitepress->get_current_language();
    
    if ($current_lang != $default_lang) {
      $service_prefix = strtolower($current_lang);
    }

    $all_languages = icl_get_languages('skip_missing=0&orderby=custom&order=asc');
    foreach ($all_languages as $lang) {
      if (!$lang['active']) {
        $link = ($lang['language_code'] === $default_lang) ? get_site_url() : get_site_url() . "/" . $lang['language_code'];
        $languages[] = array(
            "name" => $lang['native_name'],
            "link" => $link
        );
      }
    }
  }
}

$menu = wp_get_nav_menu_object($menu_name);
$last_parent = -1;
$items = array();


if (($_items = wp_get_nav_menu_items($menu)) != NULL) {
  foreach ($_items as $_item) {
    if (!$_item->menu_item_parent) {//Non ha parent, perciò si tratta di una intestazione del menu
      $_icon = array_pop(get_post_meta($_item->ID, 'menu-item-icon-field'));
      $icon = (empty($_icon) || 'none' === $_icon ) ? '' : $_icon . ' ';
      $last_parent = $_item->menu_order;
      $items[$last_parent] = array("icon" => $icon, "object" => $_item, "items" => array());
    } else {
      $items[$last_parent]["items"][] = $_item;
    }
  }
}

$total_elements = count($items);
// gestione della creazione delle righe
$per_row_elems = 3;
$elements_counter = 0;
$brand_version = get_option("branding_version");
$download_the_app = 'Download the app';
if ($brand_version == 'intesars') {
    $download_the_app = 'Download Intesa Mobi';
}

$menu_version=get_option('menu_version');
if (!$menu_version) {
    $menu_version=1;
}

$show_download_the_app = get_option("show_download_app_header");

if ($show_download_the_app && $show_download_the_app == "1")
{
  $show_download_the_app = true;
}

$show_introductory_video = true;

if (get_option("show_introductory_video"))
{
  $show_introductory_video = (get_option("show_introductory_video") == "yes" ? true : false);  
}

$show_internet_banking_url_always = false;

if (get_option("show_internet_banking_url_always") && get_option("show_internet_banking_url_always") == "1")
{
  $show_internet_banking_url_always = true;  
}

?>

<header class="mod modHeader j-modHeader">
  <div class="modWrap">
    <div class="modWrap__content">
      <?php if ($multiple_menu): ?>
        <div class="modHeader__uppermenu">
          <div class="modHeader__content">
            <a<?php if ($individual): ?> class="selected"<?php endif; ?> href="<?php echo $individual_hp_url ?>"><?php echo __('INDIVIDUALS') ?></a>
            <a<?php if ($business): ?> class="selected"<?php endif; ?> href="<?php echo $business_hp_url ?>"><?php echo __('BUSINESS USERS') ?></a>
          </div>
        </div>
      <?php endif; ?>
      <header class="modHeader__header v<?php echo $menu_version;?>">
        <div class="modHeader__content">
          <div class="modHeader__block logo">
            <a href="<?php echo $business?$business_hp_url:$individual_hp_url ?>" class="<?php echo get_logo_brand_class(); ?>"></a>
          </div>
          <div class="modHeader__block iconMenu">
            <div class="modHeader__blockList">
              <div class="modHeader__blockItem">
                <?php if(get_option('menu_version')==2) {?>
                      <?php if ($show_download_the_app) : ?>
                        <span class="modHeader__text download-the-app-link"><?php echo __($download_the_app) ?></span>
                      <?php endif; ?>
                     <!-- <?php if (get_option('ios_app_url')) : ?>
                        <a class="icon-apple" target="_blank" href="<?php echo get_option('ios_app_url'); ?>"></a>
                      <?php endif; ?>
                      <?php if (get_option('android_app_url')) : ?>
                        <a class="icon-android" target="_blank"  href="<?php echo get_option('android_app_url'); ?>"></a>
                      <?php endif; ?>
					            <?php if (get_option('wphone_app_url')) : ?>
						            <a class="icon-wphone" target="_blank"  href="<?php echo get_option('wphone_app_url'); ?>"></a>
					            <?php endif; ?>-->
                <?php } else {?>
                      <?php if (get_option('internet_banking_url')) { ?>
                        <a href="<?php echo get_option('internet_banking_url') ?>" target="_blank"><span class="text"><?php echo __("Internet Banking") ?></span><span class="text icon-lock"></span></a>
                      <?php } ?>
                <?php } ?>
              </div>
              <div class="modHeader__blockItem"><span class="hamIcon j-menu"><a href="javascript:;" class="modHeader__icon icon"></a></span></div>
              <div class="modHeader__blockItem">
                <?php if ($show_introductory_video) : ?>
                  <span class="icon-video text"></span><span class="text"><?php echo __("Watch introductory video") ?></span>
                <?php endif; ?>    
                  </div>
            </div>
          </div>
          <div class="modHeader__block download">
            <div class="modHeader__download">
              <?php if(get_option('menu_version')==2 || $show_internet_banking_url_always) {?>
                <?php if(get_option('internet_banking_url')) : ?>
                  <a href="<?php echo get_option('internet_banking_url') ?>" target="_blank"><span class="text"><?php echo __("Internet Banking") ?></span><span class="text icon-lock"></span></a>
                  <span class="<?php echo get_hash_brand_class(); ?>"></span>
                <?php endif; ?>
              <?php } else {?>
                <span class="<?php echo get_hash_brand_class(); ?>"></span>
              <?php if ($show_download_the_app) : ?>
                <span class="modHeader__text download-the-app-link"><?php echo __($download_the_app) ?></span>
              <?php endif; ?>
              <!--<?php if (get_option('ios_app_url')) : ?>
                <a class="icon-apple" target="_blank" href="<?php echo get_option('ios_app_url'); ?>"></a>
              <?php endif; ?>
              <?php if (get_option('android_app_url')) : ?>
                <a class="icon-android" target="_blank"  href="<?php echo get_option('android_app_url'); ?>"></a>
              <?php endif; ?>-->
              <?php } ?>


              <?php if (count($languages)): ?>
                <?php foreach ($languages as $_lang_link): ?>
                  <a class="modHeader__text language" href="<?php echo $_lang_link['link'] ?>"><?php echo $_lang_link['name'] ?></a>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </header>
      <!--menu elements-->
      <?php if ($total_elements): ?>
        <div class="modHeader__content">
          <div class="modHeader__menu">
            <!--begin menu-->
            <div class="modHeader__row">
              <?php foreach ($items as $parent_item) : ?>
                <div class="modHeader__menuBlock j-menuMob">
                  <?php if ($parent_item['object']->object === BIT_MENU_SECTION_TYPE_ID): ?>
                    <span class="modHeader__linkMenu icon-uni32">
                      <h2 class="<?php echo $parent_item['icon']; ?>modHeader__title"><?php echo $parent_item['object']->title ?></h2>
                    </span>
                  <?php else: ?>
                    <a class="modHeader__linkMenu icon-uni32 j-menuMob" href="<?php echo $parent_item['object']->url ?>">
                      <h2 class="<?php echo $parent_item['icon']; ?>modHeader__title"><?php echo $parent_item['object']->title ?></h2>
                    </a>
                  <?php endif; ?>
                  <?php if (count($parent_item['items'])): ?>
                    <ul class="modHeader__list">
                      <?php foreach ($parent_item['items'] as $item) : ?>
                        <li class="modHeader__item">
                          <a class="modHeader__linkVoice" href="<?php echo $item->url ?>"><?php echo $item->title ?></a>
                        </li>
                      <?php endforeach; ?>
                    </ul>
                  <?php endif; ?>
                </div>
                <?php $elements_counter++; ?>
                <!--end of element #<?php echo $elements_counter ?>-->
                <?php if ((($elements_counter % $per_row_elems) == 0) && ($elements_counter < $total_elements)): ?>
                </div>
                <div class="modHeader__row">
                <?php endif; ?>
              <?php endforeach; ?>
            </div><!--end of all the menu elements-->
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</header>