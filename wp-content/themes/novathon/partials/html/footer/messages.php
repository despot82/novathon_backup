<div class="message-wrapper">
  <div class="overlay"></div>
  <div class="form__inner">
    <div>
      <span class="close j-close"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/icon-close.png" alt="Close"/></span>
      <p class="txt">We’re sorry, but the registration is now closed.<br/><br/>Thank you very much for your interest in Novathon #withPBZ. We hope to see you at the future editions of this event!</p>
    </div>
  </div>
</div>
<?php if (get_option('_bitnovathon_show_pl_message') && is_front_page()): ?>
  <div class="pageload">
    <div class="overlay"></div>
    <div class="form__inner">
      <div>
        <span class="close j-close"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/icon-close.png" alt="Close"/></span>
        <div class="img__wrapper"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/withNovathon.jpg" alt="#with?"/></div>
        <p class="txt">
          <?php echo get_option('_bitnovathon_pl_message') ?>
        </p>
      </div>
    </div>
  </div>
<?php endif; ?>