
<div class="form__wrapper">
  <div class="overlay"></div>
  <div class="form__inner">
    <span class="close j-close"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/icon-close.png" alt="Close"/></span>

    <div class="j-wrapperFormSuccess">
      <?php echo __("Your message was sent successfully. Thanks!"); ?>
    </div>

    <div class="j-wrapperFormError">
      <?php echo __("Server errors. Try again"); ?>
    </div>		

    <div class="j-wrapperForm">
      <h4><?php echo __("Do you have any questions?"); ?><br/><?php echo __("Drop us a line."); ?></h4>
      <form id="contactsForm">
        <div class="content__row">
          <div class="content__column"><label><?php echo __("Name"); ?></label></div>
          <div class="content__column">
            <input type="text" name="name" class="j-validate" />
          </div>
        </div>
        <div class="content__row">
          <div class="content__column"><label><?php echo __("Email"); ?></label></div>
          <div class="content__column">
            <input type="text" name="email" class="j-validate"/>
          </div>
        </div>
        <div class="content__row">
          <div class="content__column">
            <label><?php echo __("Message"); ?></label>
          </div>
          <div class="content__column">
            <textarea name="comment" class="j-validate"></textarea>
          </div>
        </div>
        <input class="j-submit" type="submit" value="<?php echo __("Submit"); ?>">
        <?php if (get_option("_bitnovathon_recaptcha_sitekey")): ?>
          <div class="g-recaptcha" data-sitekey="<?php echo get_option("_bitnovathon_recaptcha_sitekey"); ?>"></div>
        <?php endif; ?>
      </form>
    </div>
  </div>
</div>