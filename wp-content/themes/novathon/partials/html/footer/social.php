	<div class="content__column">
		<?php echo __('Follow us for event-related news on'); ?>

		<?php if (get_option('_bitnovathon_facebook_link')): ?>
	    <a href="<?php echo get_option('_bitnovathon_facebook_link'); ?>" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/facebook-logo.png" alt="event facebook"/></a>
	  <?php endif; ?>

	  <a href="https://www.instagram.com/novathonwithpbz/" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/instagram-logo.png" alt="event instagram"/></a>
		<br>
	  <?php echo __('Follow PBZ on'); ?>
		<a href="http://pbzblog.pbz.hr/" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/blog_icon.png" alt="PBZ blog"/></a>
		<a href="https://www.facebook.com/privrednabankazagreb/" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/facebook-logo.png" alt="PBZ facebook"/></a>


	  <?php if (get_option('_bitnovathon_linkedin_link')): ?>
	    <a href="<?php echo get_option('_bitnovathon_linkedin_link'); ?>" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/linkedin-logo.png" alt="PBZ linkedin"/></a>
	  <?php endif; ?>

	  <?php if (get_option('_bitnovathon_youtube_link')): ?>
	    <a href="<?php echo get_option('_bitnovathon_youtube_link'); ?>" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/youtube-logo.png" alt="PBZ youtube"/></a>
	  <?php endif; ?>
	</div>
</div>
