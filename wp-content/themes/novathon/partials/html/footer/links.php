<div class="content__row">
	<div class="content__column">
	  <div class="footer__links">
	    <nav>
	      <ul>
	        <li><a class="j-formOpen" href="#"><?php echo __("Contacts") ?></a></li>
	        <li><a href="<?php echo _bitnovathon_user_get_privpol_page_permalink() ?>"><?php echo __("Terms and conditions") ?></a></li>
	        <li><a href="<?php echo _bitnovathon_user_get_privacy_page_permalink() ?>"><?php echo __("Privacy") ?></a></li>
	      </ul>
	    </nav>
	  </div>
	</div>
