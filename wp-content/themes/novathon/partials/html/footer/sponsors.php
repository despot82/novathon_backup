<?php

use Bitmama\Wordpress\Plugin\Sponsors\Template\Widget;
use Bitmama\Wordpress\Plugin\Sponsors\Definitions\SponsorsDefinition;

$main_sponsor = get_option('_bitnovathon_main_sponsor');
if ($main_sponsor) {
  $main_sponsor = new Widget($main_sponsor, TRUE);
}

$other_sponsors = get_option("_bitnovathon_other_sponsors");
$other_sponsors_ordered = array();
$other_sponsors_unordered = array();
$sponsors = array();
$has_other_sponsors = count($other_sponsors);
if ($has_other_sponsors) {
  foreach ($other_sponsors as $sponsor) {
    $ordering = get_field(SponsorsDefinition::getName('order'), $sponsor);
    if (!isset($other_sponsors_ordered[$ordering])) {
      $other_sponsors_ordered[$ordering] = new Widget($sponsor);
    } else {
      $other_sponsors_unordered[] = new Widget($sponsor);
    }
  }
  ksort($other_sponsors_ordered);
  foreach ($other_sponsors_ordered as $sponsor) {
    $sponsors[] = $sponsor;
  }
  foreach ($other_sponsors_unordered as $sponsor) {
    $sponsors[] = $sponsor;
  }
}
?>
<?php if ($main_sponsor): ?>
  <div class="content__row main">
    <div class="footer__sponsors">
      <?php $main_sponsor->toHtml(TRUE); ?>
    </div>
  </div>
<?php endif; ?>
<?php if ($has_other_sponsors): ?>
  <div class="content__row">
    <div class="footer__sponsors">
      <ul>
        <?php foreach ($sponsors as $sponsor) : ?>
          <li>
            <?php $sponsor->toHtml(true); ?>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
		<p class="text-center">
			<small><?php echo __("All rights reserved. The content of the site is informative in character and in no way binding to Privredna banka Zagreb d.d."); ?></small>
		</p>
  </div>
<?php endif; ?>
