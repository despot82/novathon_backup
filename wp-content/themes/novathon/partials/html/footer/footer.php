<footer>
  <?php
  require get_template_directory() . '/partials/html/footer/links.php';
  require get_template_directory() . '/partials/html/footer/social.php';
  require get_template_directory() . '/partials/html/footer/sponsors.php';
  ?>
</footer>
</div><!--/wrapper-->
<?php
require get_template_directory() . '/partials/html/footer/contacts.php';
require get_template_directory() . '/partials/html/footer/messages.php';
require get_template_directory() . '/partials/html/footer/scripts.php';
?>
</body>
</html>
