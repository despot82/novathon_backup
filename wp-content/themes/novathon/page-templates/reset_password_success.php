<?php

/**
* Template Name: Subscription - Reset Password Success
*
* Description:
* This template is used for user to login
*/
$isSubscriptionsPage = true;
get_header();

?>
<section class="main">
<section class="section--login">
    <div class="loginWrapper">
        <div class="loginWrapper__img">
            <img src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="">
        </div>
        <div class="regForm--resetPwNew">
          <div class="wrap">
            <div class="loginForm__intro">
              <div class="loginForm__intro--succ">
                    <img src="<?php echo get_template_directory_uri() ?>/images/mark-succes.png" alt="error">
                    <span class="line-reset"></span>
                    <span class="line-reset"></span>
                </div>
                <span class="underline-title-span">Password changed successfully</span>
            </div>
            <p>Your password has been changed successfully!</p><br><br>
              <a class="btn__submit--login notFloat" href="/login">continue</a>
          </div>
        </div>
    </div>
</section>
</section>

  <?php
get_footer();
?>
