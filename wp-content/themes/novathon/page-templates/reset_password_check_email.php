<?php

/**
* Template Name: Subscription - Reset Password Check Email
*
* Description:
* This template is used for user to login
*/
$isSubscriptionsPage = true;

get_header();
?>

  <section class="main">
    <section class="section--login">
      <div class="loginWrapper">
        <div class="loginWrapper__img">
          <img src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="">
        </div>
        <div class="regForm--resetPw">
          <div class="wrap">
            <div class="loginForm__intro">
              <div class="loginForm__intro--succ">
                <img src="<?php echo get_template_directory_uri() ?>/images/mark-setup.png" alt="setup">
                <span class="line-reset"></span>
                <span class="line-reset"></span>
              </div>
              <span class="underline-title-span">Reset Password</span>
            </div>
            <p class="infoText">Please check your mail and click the link we sent you to change your password.</p>
            <a class="btn__submit--returnhome notFloat" type="submit" name="" href="/">return to homepage</a>
          </div>
        </div>
      </div>
    </section>
  </section>
<?php
    get_footer();
?>
