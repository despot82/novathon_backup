<?php

/**
 * Template Name: Freehtml
 *
 * Description:
 * This template is used to compose a free HTML page
 */
get_header();
?>

<?php echo do_shortcode(html_entity_decode(get_post()->post_content)) ?>

<?php

get_footer();


