<?php

/**
 * Template Name: Media Page
 *
 * Description:
 * This template is used to compose the medias page
 */
use \Bitmama\Wordpress\Plugin\Media\Template\Widget;

get_header();

//Videos
$video_headline = get_field("media_page_video_headline");
$videos = get_field("media_page_videos");
$has_videos = count($videos);
$video_playlist_text = get_field("media_page_video_playlist_text");
$video_playlist_link = get_field("media_page_video_playlist_link");
$has_playlist = $video_playlist_text && $video_playlist_link;
//Photos
$photos_headline = get_field("media_page_photos_headline");
$photos = get_field("media_page_photos");
$has_photos = count($photos);
$photos_view_more_text = get_field("media_page_photos_view_more");
//$photos_show_at_start = get_field("media_page_photos_show_at_start");
//$photos_show_at_start = $photos_show_at_start ? $photos_show_at_start : 12;
$photos_shown = 0;
?>

<?php //echo do_shortcode(html_entity_decode(get_post()->post_content))     ?>
<article>
  <?php if ($has_videos): ?>
    <section class="content media__details videos">
      <div class="anchor" id="videos"></div>
      <div class="content__wrapper ">
        <a class="btn-back" href="<?php echo get_home_url(); ?>/previous-event">&lt; <?php echo __('Back') ?></a>
        <h1><?php echo $video_headline; ?></h1>
        <div class="gallery main-block">
          <ul>
            <?php foreach ($videos as $video): ?>
              <li>
                <?php
                $video_widget = new Widget($video, TRUE);
                $video_widget->toHtml(TRUE);
                ?>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
        <?php if ($has_playlist): ?>
          <a class="block__button" href="<?php echo $video_playlist_link; ?>" target="_blank"><?php echo $video_playlist_text; ?></a>
        <?php endif; ?>
      </div>
    </section>
  <?php endif; ?>
  <?php if ($has_photos): ?>
    <section class="content media__details photos">
      <div class="anchor" id="photos"></div>
      <div class="content__wrapper ">
        <h1><?php echo $photos_headline; ?></h1>
        <div class="gallery">
          <ul>
            <?php foreach ($photos as $photo): ?>
              <?php
              $src = wp_get_attachment_image_src($photo->ID);
              $thumb = (isset($src[0]) && $src[0]) ? $src[0] : false;
              $lsrc = wp_get_attachment_image_src($photo->ID, 'large');
              $large = (isset($lsrc[0]) && $lsrc[0]) ? $lsrc[0] : $thumb;
              ?>
              <?php if ($thumb): ?>
                                <!--<li<?php if ($photos_shown < $photos_show_at_start): ?> style="display: list-item;"<?php endif; ?>>-->
                <li>
                  <a class="fancybox" href="<?php echo $large ?>" data-fancybox-group="gallery"><img src="<?php echo $large ?>"/></a>
                </li>
                <?php $photos_shown++; ?>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
        </div>
        <a class="block__button j-viewMore" href="#"><?php echo $photos_view_more_text ?></a>
      </div>
    </section>
  <?php endif; ?>
</article>



<?php
get_footer();
