<?php

/**
* Template Name: Subscription - Complete Registration
*
* Description:
* This template is used for profile edit
*/

$isSubscriptionsPage = true;

$loggedIn = is_user_logged_in();

if ($loggedIn)
{
    $currentUser = wp_get_current_user();
    $sub = new Wordpress\ORM\Subscriber();
    $novathonUser = $sub->get_user_by_wordpress_user_id($currentUser->ID);

    if ($novathonUser->email_verified == '0')
    {
        header("Location: /email-not-verified");
    }

    if ($novathonUser->profile_completed == '1')
    {
        header("Location: /profile");
    }

    get_header();

    ?>
  <section class="main">
    <section class="section--login">
      <div class="loginWrapper">
        <div class="loginWrapper__img">

        </div>
        <form class="regForm" name="complete-registration-form">
         <input type="hidden" name="action" value="bit_novathon_subscriptions_complete_registration" />
          <div class="wrap">
            <div class="loginForm__intro">
              <div class="loginForm__intro--succ">
              <?php
    if ($novathonUser->profile_image_url != "")
    {
        ?>
            <img src="<?php echo $novathonUser->profile_image_url ?>" />
            <?php
    }
    else
    {
      ?>
      <img src="<?php echo get_template_directory_uri() ?>/images/mark-profile.png" alt="setup">
      <span class="line-reset"></span>
      <span class="line-reset"></span>
      <?php
    }
    ?>


              </div>
              <span class="underline-title-span">Hello <?php echo $novathonUser->name ?>,</span>
            </div>
            <p class="infoText">Complete your Profile Novathon #withPBZ!</p>
            <div class="regForm__form">
              <div class="regForm__form--wrap">
                <div class="regForm__form--selects">
                  <div class="regForm__form--selects--wrapper">
                    <label>Date of birth</label>
                    <div class="selectWrap">
                      <select name="birthdayday" class="required">
                    <?php
    for ($i=1; $i <= 31; $i++) {
        ?>
                        <option value="<?php echo $i ?>">
                        <?php echo $i ?>
                        </option>
                      <?php
    }
    ?>
                  </select>
                </div>
                <div class="selectWrap">
                  <select name="birthdaymonth" class="required">
                    <?php
    for ($i=1; $i < 13; $i++) {
        ?>
                      <option value="<?php echo $i ?>">
                        <?php echo substr(date('F', mktime(0, 0, 0, $i, 10)),0,3) ?>
                      </option>
                      <?php
    }
    ?>
                  </select>
                </div>
                <div class="selectWrap">
                  <select name="birthdayyear" class="required">
                    <?php
    for ($i=1940; $i < 2017; $i++) {
      if($i === 1980){
        ?>
                      <option value="<?php echo $i ?>" selected>
                        <?php echo $i ?>
                      </option>
                      <?php
    } else {
    ?>

                      <option value="<?php echo $i ?>">
                        <?php echo $i ?>
                      </option>
    <?php }
    } ?>
                  </select>
                </div>
              </div>
              <label>Phone number</label>
                <div class="regForm__form--tel">
                  <!--<div class="selectWrap">
                    <select name="mobilephoneprefix">
                      <option value="">Country</option>
                      <option value="+39">IT</option>
                      <option value="+34">ES</option>
                      <option value="+33">FR</option>
                      <option value="+44">UK</option>
                      <option value="+385">HR</option>
                    </select>
                  </div>-->
                  <input type="text" name="mobilephone" value="" class="required startWith" data-msg-digits="Please insert only digits">
                </div>
              </div>
              <div class="regForm__form--selects">
                <div class="selectWrap--role" data-cont="role">
                  <select name="role" class="required" >
                    <option value="">Select your role</option>
                    <option name="Developer">Developer</option>
                    <option name="Designer">Designer</option>
                    <option name="Marketer">Marketer</option>
                    <!--<option name="Other">Other</option>-->
                  </select>
                </div>
               <!-- <div class="selectWrap--otherInput" data-related="role">
                  <input placeholder="Please specify" class="required" type="text" name="role_other" value="">
                </div>-->
              </div>
              <div class="regForm__form--selects">
                <div class="selectWrap--profession" data-cont="profession">
                  <select name="profession" class="required">
                    <option value="">Select your profession</option>
                    <option value="Student">Student</option>
                    <option value="Self Employed">Self Employed</option>
                    <option value="Employed">Employed</option>
                    <option value="Other">Other</option>
                  </select>
                </div>
                <div class="selectWrap--otherInput" data-related="Other">
                  <input placeholder="Please specify" class="required" type="text" name="profession_other" value="">
                </div>
                <div class="selectWrap--otherInput" data-related="Employed">
                  <input placeholder="Company Name" class="required" type="text" name="companyname" value="">
                </div>
              </div>
            </div>
              <p>Select a challenge</p>
              <div class="regForm__form--radios">
                <div class="radioWrap">
                  <input id="a" type="radio" name="challenge" value="Smart Payments">
                  <label for="a">Smart Payments</label>
                  <ul>
                    <li>Build appealing features which make payments fast, engaging and fun.</li>
                    <li>Meet the needs of today’s sharing economy, via social media usage, producing Person 2 Person and Person 2 Group solutions for payments.</li>
                    <li>Enhance the shopping experience by identifying possible one-stop-shop solutions that offer the same instant and efficient practice to the customers, regardless of whether they are online or on-site.</li>
                  </ul>
                </div>
                <div class="radioWrap">
                  <input id="b" type="radio" name="challenge" value="Digital Wealth Management">
                  <label for="b">Digital Wealth Management</label>
                  <ul>
                    <li>Develop financial advisory models that can be introduced and become a significant part of digital channels.</li>
                    <li>Build interesting and interactive features that can transform the investment area from plain numbers and titles into a comprehensive and engaging platform in order to improve the finances of customers.</li>
                    <li>Create a unique and personalized digital experience based on the needs of affluent customers.</li>
                  </ul>
                </div>
                <div class="radioWrap">
                  <input id="c" type="radio" name="challenge" value="Smart Branch">
                  <label for="c">Smart Branch</label>
                  <ul>
                    <li>Reinvent the branches with innovative digital features that offer the customers an easy and exiting experience, making the branch as fun and smart as the digital channels are.</li>
                    <li>Help us creating and developing “signature” experiences and new sources of value for the customers that attend branches.</li>
                    <li>Develop digital tools that can assist branch personnel in their everyday job.</li>
                  </ul>
                </div>
                <div class="radioWrap">
                  <input id="d" type="radio" name="challenge" value="Bridge Solutions">
                  <label for="d">Bridge Solutions</label>
                  <ul>
                    <li>Develop bridge solutions between the physical and the virtual world of finances.</li>
                    <li>Explore the world of video banking that can help customers performing transactions or completing a sales process.</li>
                    <li>Automate processes by maintaining the human touch. Define the best mix between natural language processing, semantic search or knowledge base and propose a right blend between channels through which banks can manage customer interactions.</li>
                  </ul>
                </div>
                <div class="radioWrap">
                  <input id="e" type="radio" name="challenge" value="Outside-in banking">
                  <label for="e">Outside-in banking</label>
                  <ul>
                    <li>Explore new ways of relationship with consumers, not only by offering standard products but also by delivering value at each step of the daily customer journey outside banking.</li>
                    <li>Inspire banks using all relevant data to impact customer behavior through habit pattern analysis and therefore to deliver a personalized digital experience with appropriate and relevant offers/products.</li>
                    <li>Reveal patterns that are relevant to customers and give them possibility to respond immediately through engaging actions.</li>
                  </ul>
                </div>
                <div class="radioWrap">
                  <input id="f" type="radio" name="challenge" value="Big Financial Data">
                  <label for="f">Big Financial Data</label>
                  <ul>
                    <li>Rely on prepared big data set to implement innovative features</li>
                    <li>Use machine learning algorithms to offer a better user expirience and better understanding of user's needs</li>
                    <li>Produce stunning data visualizations to engage people and to better support business decision makers</li>
                  </ul>
                </div>
              </div>
              <input class="btn__submit--login" type="button" data-action="complete-registration-action" name="complete-registration-button" value="submit">
            </div>
          </div>
        </form>
      </div>
    </section>
  </section>

  <?php
    get_footer();
}
else
{
    header("Location: /login");
}
?>
