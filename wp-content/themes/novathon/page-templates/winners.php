<?php
/**
 * Template Name: Winners
 *
 * Description:
 * This template is used to compose the winners page
 */
get_header();
$winners = get_field("page_win_winners");
$odd = true;
$start = true;
?>

<?php //echo do_shortcode(html_entity_decode(get_post()->post_content))  ?>
<article>
  <?php foreach ($winners as $winner) : ?>
    <?php
    $position_text = "";
    if (strpos($_SERVER['REQUEST_URI'], 'hr') !== false) {

      switch ($winner['page_win_winners_place']) {
        case "first":
          $position_text = "<b>1.</b>";
          break;
        case "second":
          $position_text = "<b>2.</b>";
          break;
        case "third":
          $position_text = "<b>3.</b>";
          break;

        }
    } else {
      switch ($winner['page_win_winners_place']) {
        case "first":
          $position_text = "<b>1<sup>st</sup></b>";
          break;
        case "second":
          $position_text = "<b>2<sup>nd</sup></b>";
          break;
        case "third":
          $position_text = "<b>3<sup>rd</sup></b>";
          break;

        }

    }
    ?>
    <section class="content winners__details <?php if ($odd): ?>odd<?php else: ?>even<?php endif; ?>">
      <div class="anchor" id="<?php echo $winner['page_win_winners_place'] ?>"></div>
      <div class="content__wrapper ">
        <?php if ($start): ?>
          <a class="btn-back" href="<?php echo get_home_url(); ?>/previous-event">&lt; <?php echo __('Back') ?></a>
        <?php endif; ?>
        <?php if (!empty($position_text)): ?>
          <h1 class="color--orange"><?php echo $position_text ?> <?php echo __('Place') ?></h1>
        <?php endif; ?>
        <?php if (isset($winner['page_win_winners_place_image']) && isset($winner['page_win_winners_place_image']['url'])): ?>
          <img class="ico" src="<?php echo $winner['page_win_winners_place_image']['url']; ?>" alt="">
        <?php endif; ?>
        <div class="title color--orange"><?php echo $winner['page_win_winners_team_name']; ?></div>
        <div><?php echo __('Idea') ?>: <?php echo $winner['page_win_winners_team_idea']; ?></div>
        <div><?php echo __('Challenge') ?>: <?php echo $winner['page_win_winners_challenge']; ?></div>
        <p><?php echo $winner['page_win_winners_description']; ?></p>
        <div>
          <?php
          $members = explode(",", $winner['page_win_winners_team_members']);
          ?>
          <?php if (count($members)): ?>
            <ul>
              <?php foreach ($members as $member): ?>
                <li>
                  <p>
                    <b><?php echo trim($member) ?></b><br>
                  </p>
                </li>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
        </div>
      </div>
    </section>
    <?php
    $odd = !$odd;
    $start = false;
    ?>
  <?php endforeach; ?>
</article>
<?php
get_footer();
