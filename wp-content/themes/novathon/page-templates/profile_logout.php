<?php

/**
* Template Name: Subscription - Logout
*
* Description:
* This template is used for profile edit
*/

$isSubscriptionsPage = true;

wp_logout();
header("Location: /");

?>