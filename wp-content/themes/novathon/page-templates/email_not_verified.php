<?php

/**
* Template Name: Subscription - Email not verified
*
* Description:
* This template is used for profile edit
*/

$isSubscriptionsPage = true;

$loggedIn = is_user_logged_in();

if ($loggedIn)
{
    $currentUser = wp_get_current_user();
    $sub = new Wordpress\ORM\Subscriber();
    $novathonUser = $sub->get_user_by_wordpress_user_id($currentUser->ID);

    if ($novathonUser->email_verified == '1')
    {
        header("Location: /profile");
    }

    get_header();

    ?>
<section class="main">
<section class="section--login">
    <div class="loginWrapper">
        <div class="loginWrapper__img">
            <img src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="">
        </div>
        <div class="regForm--resetPwNew">
          <div class="wrap">
            <div class="loginForm__intro">
                <div class="loginForm__intro--succ">
                    <img src="<?php echo get_template_directory_uri() ?>/images/mark-error.png" alt="success">
                    <span class="line-reset"></span>
                    <span class="line-reset"></span>
                </div>
                <span class="underline-title-span">This mail has not been confirmed.</span>
            </div>
            <p class="infoText">Please check your mail and click the link we sent you to confirm your registration.</p>
            <a class="btn__submit--login notFloat" type="submit" name="" href="/login" >try again</a>
          </div>
        </div>
    </div>
</section>
</section>
  <?php
    get_footer();
}
else
{
    header("Location: /login");
}
?>
