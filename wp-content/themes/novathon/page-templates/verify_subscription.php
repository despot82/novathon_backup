<?php

/**
 * Template Name: Subscription - Verify Email
 *
 * Description:
 * This template is used for email verification
 */
$isSubscriptionsPage = true;
$verification_result = bit_novathon_subscriptions_verify_email();

header("Location: /complete-profile");

?>
