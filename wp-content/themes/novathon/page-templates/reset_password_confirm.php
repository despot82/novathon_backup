<?php

/**
* Template Name: Subscription - Reset Password Confirm
*
* Description:
* This template is used for user to login
*/
$isSubscriptionsPage = true;
$guid = $_GET["guid"];
$hashedEmail = $_GET["email"];

$guidVerified = false;

$sub = new Wordpress\ORM\Subscriber();
$user = $sub->get_user_by_reset_guid($guid);

if ($user)
{
    $guidVerified = true;
}

get_header();

?>

  <section class="main">
    <section class="section--login">
      <div class="loginWrapper">
        <div class="loginWrapper__img">
          <img src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="">
        </div>
        <div class="regForm--resetPwNew">
          <div class="wrap">
            <div class="loginForm__intro">
              <div class="loginForm__intro--succ">
                <img src="<?php echo get_template_directory_uri() ?>/images/mark-setup.png" alt="error">
                <span class="line-reset"></span>
                <span class="line-reset"></span>
              </div>
              <span class="underline-title-span">Reset password</span>
            </div>
            <?php
if ($guidVerified)
{
    ?>
              <p class="infoText">Enter your new password to receive your electronic ticket</p>
              <form class="regForm__form--new" name="reset-password-confirm-form">
                <input type="hidden" name="action" value="bit_novathon_subscriptions_reset_password_confirm" />
                <input type="hidden" name="guid" value="<?php echo $guid ?>" />
                <input type="hidden" name="email" value="<?php echo $hashedEmail ?>" />
                <div class="regForm__form__inputs">
                  <div class="loginForm__form--floating">
                    <input type="password" name="newpassword" id="newpassword" value="" required>
                    <label>New Password</label>
                  </div>
                  <div class="loginForm__form--floating">
                    <input type="password" name="newpasswordconfirm" value="" required equalTo="#newpassword" data-msg-equalTo="Please insert same password">
                    <label>Confirm Password</label>
                  </div>
                </div>
                <a class="btn__submit--resetPw" type="button" data-action="reset-password-confirm-action" name="">continue</a>
                <a class="btn__submit--cancel" href="/login">cancel</a>
              </form>
              <?php
}
else
{
    ?>
                <br/>
                <br/>
                <br/>
                <br/> Invalid input data
                <br/>
                <br/>
                <br/>
                <br/>
                <?php
}
?>
          </div>
        </div>
      </div>
    </section>
  </section>
  <?php
get_footer();
?>
