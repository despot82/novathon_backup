<?php

/**
* Template Name: Subscription - Reset Password
*
* Description:
* This template is used for user to login
*/
$isSubscriptionsPage = true;
get_header();
?>

  <section class="main">
    <section class="section--login">
      <div class="loginWrapper">
        <div class="loginWrapper__img">
          <img src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="user">
        </div>
        <div class="regForm--resetPw">
          <div class="wrap">
            <div class="loginForm__intro">
              <div class="loginForm__intro--succ">
                <img src="<?php echo get_template_directory_uri() ?>/images/mark-setup.png" alt="setup">
                <span class="line-reset"></span>
                <span class="line-reset"></span>
              </div>
              <span class="underline-title-span">Reset Password</span>
            </div>
            <p class="infoText">Enter your email address and then click on the link in the email we send you to reset your password.</p>
            <form class="regForm__form" name="reset-password-form">
              <input type="hidden" name="action" value="bit_novathon_subscriptions_reset_password" />
              <div class="loginForm__form--floating">
                <input type="email" name="email" value="" required>
                <label>Email</label>
              </div>
              <a class="btn__submit--resetPw" type="button" data-action="reset-password-action" name="" >continue</a>
              <a class="btn__submit--cancel" href="/login">cancel</a>
            </form>
          </div>
        </div>
      </div>
    </section>
  </section>

  <?php
get_footer();
?>
