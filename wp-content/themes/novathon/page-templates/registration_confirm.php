<?php

/**
* Template Name: Subscription - Registration confirm
*
* Description:
* This template is used for profile edit
*/

$isSubscriptionsPage = true;

$loggedIn = true; // is_user_logged_in();

if ($loggedIn)
{
    $currentUser = wp_get_current_user();
    $sub = new Wordpress\ORM\Subscriber();
    $novathonUser = $sub->get_user_by_wordpress_user_id($currentUser->ID);

    if ($novathonUser->email_verified == 1)
    {
        header("Location: /profile");
    }

    get_header();

    ?>

<section class="main">
<section class="section--login">
    <div class="loginWrapper">
        <div class="loginWrapper__img">
            <img src="<?php echo get_template_directory_uri() ?>/images/logo.png" alt="">
        </div>
        <div class="regForm--resetPwNew">
          <div class="wrap">
            <div class="loginForm__intro">
                <div class="loginForm__intro--succ">
                    <img src="<?php echo get_template_directory_uri() ?>/images/mark-succes.png" alt="success">
                    <span class="line-reset"></span>
                    <span class="line-reset"></span>
                </div>
                <span class="underline-title-span">Thank you for your registration!</span>
            </div>
            <p class="infoText">We’ve sent an email to: <strong><?php echo $novathonUser->email ?></strong></p>
            <p class="infoText">Haven't received the email?<br>Click <a data-action="resend-verify-email" href="#">here</a> to resend it.</p>
              <a class="btn__submit--returnhome notFloat" type="submit" name="" href="/" >return to homepage</a>
          </div>
        </div>
    </div>
</section>
</section>
  <?php
    get_footer();
}
else
{
    header("Location: /login");
}
?>
