<?php

/**
* Template Name: Subscription - Profile management

* Description:
* This template is used for profile edit
*/

$isSubscriptionsPage = true;

$loggedIn = bit_novathon_subscriptions_is_logged_in();

if ($loggedIn)
{
    $currentUser = wp_get_current_user();
    $sub = new Wordpress\ORM\Subscriber();
    $novathonUser = $sub->get_user_by_wordpress_user_id($currentUser->ID);

    //var_dump($novathonUser);

    if ($novathonUser->email_verified == '0')
    {
        header("Location: /email-not-verified");
    }

    if ($novathonUser->profile_completed == '0')
    {
        header("Location: /complete-profile");
    }

    get_header();

    ?>

  <section class="main">
    <section class="section--login">
      <div class="profileWrapper">
        <div class="profileWrapper--top">
          <div class="profileWrapper__img">
            <?php
    if ($novathonUser->profile_image_url != "")
    {
        ?>
              <img src="<?php echo $novathonUser->profile_image_url ?>" />
              <?php
    } else {
        ?>
                <img src="<?php echo get_template_directory_uri() ?>/images/mark-profile-big.png" alt="profile">
                <?php
    }
    ?>
          </div>
          <div class="profileWrapper_title">
            <span class="profileWrapper_title-t"><?php echo $novathonUser->name ?> <?php echo $novathonUser->last_name ?></span>
            <span class="profileWrapper_title-state">
              Registration Complete.
    <!-- <?php
    if ($novathonUser->confirmed == 0)
    {
        ?>
        Participation not confirmed.
        <?php
    }
    ?>
    <?php
     if ($novathonUser->confirmed == 1)
    {
        ?>
        Participation confirmed.
        <?php
    }
    ?> -->
    <?php
    if ($novathonUser->in_waitlist == 1)
    {
        ?>
        <br>You are in waiting list.
        <?php
    }
    ?>

    </span>
            <div class="profileWrapper__info">
              <a href="#_"><span class="icon-faq"></span></a>
              <span class="tooltip">You will recieve an email to confirm your presence 20 days before the event.</span>
            </div>
          </div>
          <div class="profileWrapper__download">
            <?php
            if ($novathonUser->in_waitlist == 0)
            {
                ?>
                <a href="/ticket" target="_blank"><span class="icon-download"></span> <span>Download your registration</span></a>
                <?php
            }
            ?>
          </div>
        </div>
        <div class="profileWrapper__generalblock">
          <h2 class="profileWrapper__generalblock__title">General information</h2>
          <form class="wrap" name="update-profile-form">
            <a href="#_" class="button--edit -js-edit">edit</a>
            <button href="#_" type="reset" class="button--edit -js-cancel" value="cancel">cancel</button>
            <ul>
              <li>
                <span class="icon-mail"></span>
                <span class="label">Email</span>
                <span class="mailText"><?php echo $novathonUser->email ?></span>
              </li>
              <li>
                <span class="icon-password"></span>
                <span class="label">Password</span>
                <input type="password" name="password" value="**********" readonly>
                <a class="profileChangePw" href="/reset-password">change password</a>
              </li>
              <?php
    /* mod sandro */
    // giorno mese anno da varibiale birthday
    $arrayDate = explode("/",$novathonUser->birthday);

    //arrray per select testuali role challenge profession
    $sRole = ['Developer', 'Designer', 'Marketer'];
    $sProfession = ['Student' , 'Self Employed' , 'Employed', 'Other'];
    $sChallenge = ['Smart Payments', 'Digital Wealth Management', 'Smart Branch' ,'Bridge Solutions' , 'Outside-in banking' , 'Big Financial Data'];

    $professionSelected = false;
    $roleSelected = false;

    if (in_array($novathonUser->profession, $sProfession))
        $professionSelected = true;

    if (in_array($novathonUser->role, $sRole))
        $roleSelected = true;

    $profession_value = $novathonUser->profession;
    $role_value = $novathonUser->role;

    if (!$professionSelected)
        $profession_value = "Other";

    if (!$roleSelected)
        $role_value = "Other";

    ?>
                <!-- inizio modulo duplicato -->
                <li>
                  <span class="icon-Birthday"></span>
                  <span class="label">Birthday</span>
                  <div class="birtdayWrapper">
                    <select name="birthdayday" disabled class="required">
                      <?php
    for ($i=1; $i <= 31; $i++) {
        if($i !== intval($arrayDate[0])){
            ?>
                        <option value="<?php echo $i ?>">
                          <?php echo $i ?>
                        </option>
                        <?php
        } else {
            ?>
                          <option value="<?php echo $i ?>" selected>
                            <?php echo $i ?>
                          </option>
                          <?php
        }
    }
    ?>
                    </select>
                    <select name="birthdaymonth" disabled class="required">
                      <?php
    for ($i=1; $i <= 12; $i++) {
        if($i !== intval($arrayDate[1])){
            ?>
                        <option value="<?php echo $i ?>">
                          <?php echo substr(date('F', mktime(0, 0, 0, $i, 10)),0,3) ?>
                        </option>
                        <?php
        } else {
            ?>
                          <option value="<?php echo $i ?>" selected>
                            <?php echo substr(date('F', mktime(0, 0, 0, $i, 10)),0,3) ?>
                          </option>
                          <?php
        }
    }
    ?>
                    </select>
                    <select name="birthdayyear" disabled>
                      <?php
    for ($i=1940; $i < 2017; $i++) {
        if($i !== intval($arrayDate[2])){
            ?>
                        <option value="<?php echo $i ?>">
                          <?php echo $i ?>
                        </option>
                        <?php
        } else {
            ?>
                          <option value="<?php echo $i ?>" selected>
                            <?php echo $i ?>
                          </option>
                          <?php
        }
    }
    ?>
                    </select>
                  </div>
                </li>
                <li>
                  <span class="icon-phone"></span>
                  <span class="label">Phone</span>
                  <input type="text" name="mobilephone" class="required startWith" value="<?php echo $novathonUser->mobilephone ?>" readonly data-msg-digits="Please insert only digits">
                </li>
                <!-- inizio modulo duplicato -->
                <li>
                  <span class="icon-challenge"></span>
                  <span class="label">Challenge</span>
                  <select name="challenge" disabled class="required">
                    <?php
    forEach($sChallenge as $val){
        if($val !== $novathonUser->challenge){
            ?>
                      <option value="<?php echo $val ?>">
                        <?php echo $val ?>
                      </option>
                      <?php
        } else {
            ?>
                        <option value="<?php echo $val ?>" selected>
                          <?php echo $val ?>
                        </option>
                        <?php
        }
    }
    ?>
                  </select>
                </li>
                <li>
                  <span class="icon-Role"></span>
                  <span class="label">Role</span>
                  <select name="role" disabled class="required">
                    <?php
    forEach($sRole as $val){
        if($val !== $role_value){
            ?>
                      <option value="<?php echo $val ?>">
                        <?php echo $val ?>
                      </option>
                      <?php
        } else {
            ?>
                        <option value="<?php echo $val ?>" selected>
                          <?php echo $val ?>
                        </option>
                        <?php
        }
    }
    ?>
                  </select>
                  <input readonly class="otherInput required" style="<?php echo ($roleSelected ? " display: none; " : " ") ?>" type="text" name="role_other" value="<?php echo $novathonUser->role ?>">
                </li>
                <li>
                  <span class="icon-ic_Profession"></span>
                  <span class="label">Profession</span>
                  <select name="profession" disabled>
                    <?php
    forEach($sProfession as $val){
        if($val !== $profession_value){
            ?>
                      <option value="<?php echo $val ?>">
                        <?php echo $val ?>
                      </option>
                      <?php
        } else {
            ?>
                        <option value="<?php echo $val ?>" selected>
                          <?php echo $val ?>
                        </option>
                        <?php
        }
    }
    ?>
                  </select>
                  <input placeholder="Secify other" readonly class="otherInput required" style="<?php echo ($professionSelected ? " display: none; " : " ") ?>" type="text" name="profession_other" value="<?php echo $novathonUser->profession ?>" data-related="Other" >

                  <input readonly class="otherInput required" placeholder="Company name" style="<?php echo ($novathonUser->profession != "Employed" ? " display: none; " : " ") ?>" type="text" name="companyname" value="<?php echo $novathonUser->companyname ?>" data-related="Employed">
                </li>
            </ul>
            <input type="hidden" name="action" value="bit_novathon_subscriptions_update_profile" />
          </form>
        </div>
        <div class="profileWrapper__generalblock">
          <h2 class="profileWrapper__generalblock__title">Create your team</h2>
          <div class="wrap">
            <p>The team formation session is not open yet. We will send you an email to remind you when you can create your team.</p>
            
          </div>
        </div>
      </div>
    </section>
  </section>

  <?php
    get_footer();
}
else
{
    header("Location: /login");
}
?>
