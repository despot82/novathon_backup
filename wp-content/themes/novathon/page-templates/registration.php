<?php

/**
* Template Name: Subscription - Form
*
* Description:
* This template is used for subscription form
*/
$isSubscriptionsPage = true;

$loggedIn = is_user_logged_in();


if ($loggedIn)
{
    $currentUser = wp_get_current_user();
    $sub = new Wordpress\ORM\Subscriber();
    $novathonUser = $sub->get_user_by_wordpress_user_id($currentUser->ID);

    if ($novathonUser->email_verified == '0')
    {
        header("Location: /email-not-verified");
    }

    if ($novathonUser->profile_completed == '1')
    {
        header("Location: /profile");
    } else {
        header("Location: /complete-profile");
    }
}

get_header();
?>
  

<section class="main">
  <section class="section--login">
    <div class="loginWrapper">
      <div class="loginWrapper__img">
        <img src="<?php echo get_template_directory_uri() ?>/dist/images/logo.png" alt="">
      </div>
      <div class="loginForm">
        <div class="wrap">
          <div class="loginForm__intro">
            Register for Novathon #withPBZ
          </div>
          <div class="loginForm__socialLogin">
            <a class="loginForm__socialLogin--link" data-action="subscribe-facebook" href="javascript:;"><span class="icon-facebook"></span>facebook</a>
            <a class="loginForm__socialLogin--link" data-action="subscribe-google" href="javascript:;"><span class="icon-40-google-plus"></span>google</a>
            <a class="loginForm__socialLogin--link" data-action="subscribe-google-plus" href="javascript:;"><span class="icon-40-google-plus"></span>google</a>
          </div>
          <input id="checkAgree_social" type="checkbox" name="checkAgree_social" value="1">
            <label class="terms" for="checkAgree_social">I agree to the <a href="/terms-and-conditions/">Terms and Conditions</a> and <a href="/privacy">Privacy Policy</a> of Novathon#withPBZ</label>
          <div class="loginForm__titleSection">
            <span>or complete this form</span>
          </div>
          <form class="loginForm__form" name="subscribe-form">
            <input type="hidden" name="action" value="bit_novathon_subscriptions_register_standalone" />
            <div class="loginForm__form--floating">
              <input type="name" name="name" value="" required="">
              <label>Name</label>
            </div>
            <div class="loginForm__form--floating">
              <input type="surname" name="lastname" value="" required="">
              <label>Surname</label>
            </div>
            <div class="loginForm__form--floating">
              <input type="email" id="email" name="email" value="" required="">
              <label>Email</label>
            </div>
            <div class="loginForm__form--floating">
              <input type="email" id="emailConfirm" name="emailConfrim" value="" required="" equalTo="#email" data-msg-equalTo="Please insert same email">
              <label>Confirm Email</label>
            </div>
            <div class="loginForm__form--floating">
              <input type="password" id="password" name="password" value="" required="">
              <label>Password</label>
            </div>
            <div class="loginForm__form--floating">
              <input type="password" id="passwordConfirm" name="passwordConfirm" value="" required="" equalTo="#password" data-msg-equalTo="Please insert same password">
              <label>Confirm Password</label>
            </div>
            <input id="checkAgree" type="checkbox" name="checkAgree" value="1">
            <label class="terms" for="checkAgree">I agree to the <a href="/terms-and-conditions/">Terms and Conditions</a> and <a href="/privacy">Privacy Policy</a> of Novathon#withPBZ</label>

            <input class="btn__submit--login" name="subscribe-action" data-action="subscribe" type="button" name="" value="register">
            <div class="g-recaptcha" data-sitekey="6LfoUyMUAAAAAJ1vjm3Iqu9MyJtZRBaLcLwz3k0b"></div>
            
          </form>
          <div class="loginForm__alredyR">
            Already have an account? <a href="/login">Log in</a>
          </div>
        </div>
      </div>
      <div class="loginForm__loader" data-action="loader">
        <img src="<?php echo get_template_directory_uri() ?>/images/loading.gif" alt="">
      </div>
    </div>
  </section>
</section>

  <?php
  
get_footer();

?>
