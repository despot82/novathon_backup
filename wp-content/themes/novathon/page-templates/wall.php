<?php
/**
 * Template Name: Wall
 *
 * Description:
 * This template is used to render a wall page.
 */
get_header();
?>

<?php
$wall = Bitmama\Wordpress\Plugin\ResponsiveWalls\Template\Wall::loadFromPage(get_the_ID());
?>
<?php $html = $wall->toHtml(TRUE); ?>

<?php
get_footer();