<?php

/**
* Template Name: Subscription - Profile Login
*
* Description:
* This template is used for user to login
*/

$isSubscriptionsPage = true;

get_header();
?>

<!--  <article>
    <section class="content terms-conditions">
      <div class="content__wrapper ">

        <form name="login-form" method="post" action="">

          <input type="hidden" name="action" value="bit_novathon_subscriptions_login_standalone" />

          <a href="#" data-action="login-facebook">Facebook</a>
          <br/>
          <a href="#" data-action="login-twitter">Twitter</a>
          <br/>
          <a href="#" data-action="login-google" id="login-with-google">Google</a>
          <br/>
          <br/> Email:
          <input name="email" type="email" class="email required">
          <br/> Password:
          <input name="password" type="password" class="password required">
          <br/>
          <br/>
          <br/>
          <input type="button" name="login-action" data-action="login" value="Login">
        </form>
      </div>
    </section>
  </article>-->

  <section class="main">
  <section class="section--login">
    <div class="loginWrapper">
      <div class="loginWrapper__img">
        <img src="<?php echo get_template_directory_uri() ?>/dist/images/logo.png" alt="">
      </div>
      <div class="loginForm">
        <div class="wrap">
          <div class="loginForm__intro">
            Log in to Novathon #withPBZ
          </div>
          <div class="loginForm__socialLogin">
            <a class="loginForm__socialLogin--link" data-action="login-facebook" href="#"><span class="icon-facebook"></span>facebook</a>
            <a class="loginForm__socialLogin--link" data-action="login-google" href="#"><span class="icon-40-google-plus"></span>google</a>
            <!-- a class="loginForm__socialLogin--link" data-action="login-twitter" href="#"><span class="icon-twitter"></span>twitter</a -->
          </div>
          <div class="loginForm__titleSection">
            <span>or log in with</span>
          </div>
          <form class="loginForm__form" name="login-form">
            <input type="hidden" name="action" value="bit_novathon_subscriptions_login_standalone" />
            <div class="loginForm__form--floating">
              <input type="email" name="email" value="" required="">
              <label>Email</label>
            </div>
            <div class="loginForm__form--floating">
              <input type="password" name="password" value="" required="" autocomplete="new-password">
              <label>Password</label>
              <!--<p>error</p>-->
            </div>
            <input class="btn__submit--login" name="subscribe-action" name="login-action" data-action="login" type="button" value="login">
          </form>
          <div class="loginForm__alredyR">
            Don't have an account? <a href="/subscribe">Register</a>
          </div>
          <div class="loginForm__alredyR">
           Password forgotten? <a href="/reset-password">Reset password</a>
          </div>
        </div>
      </div>
      <div class="loginForm__loader" data-action="loader">
        <img src="<?php echo get_template_directory_uri() ?>/images/loading.gif" alt="">
      </div>
    </div>
  </section>
</section>
  <?php
get_footer();
?>
