<?php
/**
 * Template Name: Privacy
 *
 * Description:
 * This template is used to compose the privacy page (basically Free HTML)
 */
get_header();
?>

<article>
  <section class="content terms-conditions">
		<a class="btn-back" href="<?php echo get_home_url(); ?>">&lt; <?php echo __('Back') ?></a>
    <div class="content__wrapper ">
      <?php echo do_shortcode(html_entity_decode(get_post()->post_content)) ?>
    </div>
    <a class="btn-back" href="<?php echo get_home_url(); ?>">&lt; <?php echo __('Back') ?></a>
  </section>
</article>
<?php
get_footer();
