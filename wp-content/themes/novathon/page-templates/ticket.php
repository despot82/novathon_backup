<?php
/**
* Template Name: Subscription - Ticket
*
* Description:
* This template is used for user to login
*/

$isSubscriptionsPage = true;

$loggedIn = bit_novathon_subscriptions_is_logged_in();

if ($loggedIn)
{
    bit_novathon_subscriptions_download_ticket();
}
?>