module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            files: [
                'gruntfile.js', '<%=pkg.directories.js_dev%>/*.js'
            ],
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                boss: true,
                eqnull: true,
                browser: true,
                globals: {
                    module: true,
                    //require: true,
                    //requirejs: true,
                    jQuery: true,
                    console: true,
                    define: true
                }
            }
        },
        nunjucks: {
            options: {
                data: grunt.file.readJSON('source/html/data/data.json')
            },
            render: {
                files: [
                    {
                        expand: true,
                        cwd: "source/html/",
                        src: "*.html",
                        dest: "dist/",
                        ext: ".html"
                    }
                ]
            }
        },
        sass: {
            dev: {
                options: {
                    style: 'expanded',
                    trace: true
                },
                files: {
                    'dist/<%= pkg.directories.css %>/style.css': '<%= pkg.directories.sass %>/style.sass',
                    'dist/<%= pkg.directories.css %>/admin.css': '<%= pkg.directories.sass %>/admin.sass'
                }
            },
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    '<%= pkg.directories.css %>/style.min.css': '<%= pkg.directories.sass %>/style.sass',
                    '<%= pkg.directories.css %>/admin.min.css': '<%= pkg.directories.sass %>/admin.sass'
                }
            }
        },
        // concat: {
        //     dev: {
        //         src: ['<%= pkg.directories.js_dev %>/vendor/jquery-1.9.1.js', '<%= pkg.directories.js_dev %>/vendor/modernizr.js', '<%= pkg.directories.js_dev %>/**/*.js'],
        //         dest: '<%= pkg.directories.js %>/script.js'
        //     }
        // },
        import: {
            dist: {
                options: {},
                files: {
                    '<%= pkg.directories.js %>/script.js': '<%= pkg.directories.js_dev %>/script.js',
                    'dist/js/script.js': '<%= pkg.directories.js %>/script.js'
                }
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            my_target: {
                files: {
                    '<%= pkg.directories.js %>/script.min.js': ['<%= pkg.directories.js %>/script.js']
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: [
                    'last 10 versions', 'ie 8', 'ie 9'
                ],
                map: true
            },
            default: {
                src: '<%= pkg.directories.css %>/*.css'
            }
        },
        copy: {
            fonts: {
                expand: true,
                cwd: 'source/fonts/',
                src: '**',
                dest: 'dist/css/fonts/'
            },
			fonts2: {
                expand: true,
                cwd: 'source/fonts/',
                src: '**',
                dest: 'css/fonts/'
            },
            img: {
                expand: true,
                cwd: 'source/images/',
                src: '**',
                dest: 'dist/images/'
            },
			img2: {
                expand: true,
                cwd: 'source/images/',
                src: '**',
                dest: 'images/'
            }
        },
        watch: {
            options: {
                livereload: true
            },
            nunjucks: {
                files: ['source/html/**/*.html'],
                tasks: ['nunjucks']
            },
            sass: {
                files: ['<%= pkg.directories.sass %>/**/*.sass'],
                tasks: ['sass:dev', 'autoprefixer']
            },
            import: {
                files: ['<%= pkg.directories.js_dev %>/*.js'],
                tasks: ['import']
            },
            uglify: {
                files: ['<%= pkg.directories.js_dev %>/*.js'],
                tasks: ['uglify']
            },
            copy: {
                files: ['source/fonts/**.*'],
                tasks: ['copy']
            },
            copyiMG:{
              files: ['source/images/**/**.*'],
              tasks: ['copy']
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: ['source/**.*']
                },
                options: {
                    watchTask: true,
                    server: './dist'
                }
            }
        }
    });
    /* Load Task(s) */
    require('load-grunt-tasks')(grunt);
    /* Register Task(s) */
    grunt.registerTask('default', [
        'nunjucks',
        'sass:dist',
        'autoprefixer',
        'import',
        'uglify',
        'copy'
    ]);
    grunt.registerTask('novathon', ['browserSync', 'watch']);

}
