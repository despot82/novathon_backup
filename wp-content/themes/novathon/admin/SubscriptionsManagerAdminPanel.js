var SubscriptionsManagerAdminPanel = function () {

    this.init();

    jQuery.ajaxSetup({cache: false});

    SubscriptionManagerInstance = this;
}

SubscriptionsManagerAdminPanel.prototype.init = function () {

    var instance = this;

    this.search();

    jQuery('[data-action=filter-waitlist]').click(function () {

        jQuery('.adminPanel__tab__nav--control > a').removeClass('active');
        jQuery(this).addClass('active');

        instance.search();

    });

    jQuery('[data-action=filter-active]').click(function () {

        jQuery('.adminPanel__tab__nav--control > a').removeClass('active');
        jQuery(this).addClass('active');

        instance.search();

    });

    jQuery('[name=textSearch]').blur(function () {

        instance.search();
    });

    jQuery('[name=filterSearch]').change(function () {
        instance.search();
    });

    this.bindAdminBarActions();

    var importDialog;

    jQuery('[data-action=import]').click(function () {

        importDialog = jQuery('#importDialog').dialog();
    });

    jQuery('[data-action=import-csv-action]').click(function () {

        var btn = jQuery(this);

        btn.html('Importing..');

        jQuery('form[name=import]').ajaxForm({
            dataType: 'json',
            success: function (response) {

                btn.html('Import');

                alert('Imported: ' + response.imported + ', not imported: ' + response.not_imported);

                instance.search();

                return false;
            }
        });
    });

    jQuery('[data-action=send-mail-action]').click(function () {
        instance.sendMail();
    });
    
    jQuery('[data-action=send-newsletter-action]').click(function () {
        instance.sendNewsletter();
    });

    jQuery('[data-action=add-participiant-dialog]').click(function () {

        instance.openEditProfile(-1);
    });

    jQuery('[name=textSearch]').keydown(function (e) {
        if (e.keyCode == 13) {
            instance.search();
        }
    });

    jQuery('[data-action=export-action]').click(function () {

        var qs = '/wp-admin/index.php?page=novathon-subscribers-export&waitlist=' + instance.isWaitListSearchTypeActive() + "&textSearch=" + escape(jQuery('[name=textSearch]').val()) + '&status=' + jQuery('[name=filterSearch]').val();

        document.location.href = qs;

        return false;

    });
}

SubscriptionsManagerAdminPanel.prototype.bindAdminBarActions = function () {

    var instance = this;

    jQuery('[data-action=massive-confirm]').click(function () {

        instance.confirmUsers(instance.getUsersSelected());

    });

    jQuery('[data-action=massive-delete]').click(function () {

        instance.deleteUsers(instance.getUsersSelected());

    });

    jQuery('[data-action=massive-send-mail]').click(function () {
    	
        instance.openSendMailDialog(-1);

    });
    
    jQuery('[data-action=massive-send-mail-newsletter]').click(function () {
    
    	instance.openSendNewsletterMailDialog(-1);
        
    });

    jQuery('[data-action=massive-add-to-participant]').click(function () {

        instance.addToPartipant(instance.getUsersSelected());

    });
}

SubscriptionsManagerAdminPanel.prototype.openSendMailDialog = function (userid) {
    
    jQuery('[name=send-mail-userid]').val(userid);
    jQuery('#sendMailDialog').dialog();
}

SubscriptionsManagerAdminPanel.prototype.openSendNewsletterMailDialog = function (userid) {
	
    jQuery('[name=send-mail-userid]').val(userid);
    jQuery('#sendNewsletterMailDialog').dialog();
}

SubscriptionsManagerAdminPanel.prototype.getUsersSelected = function () {

    var users = new Array();

    jQuery('[data-action=subscriptions-select-user]:checked').each(function (index, element) {
        users.push(jQuery(element).data('user-id'));
    });

    return users;
}

SubscriptionsManagerAdminPanel.prototype.bindListActions = function () {

    var instance = this;

    jQuery('[data-action=subscriptions-select-user]').click(function () {

        if (jQuery('[data-action=subscriptions-select-user]:checked').size() > 0) {

            instance.adminPanelShow();

            jQuery('[data-content=total-selected]').html(jQuery('[data-action=subscriptions-select-user]:checked').size());
        } else {
            jQuery('.adminPanel_banner').hide();
        }
    });

    jQuery('[data-action=subscriptions-select-all]').click(function () {

        if (jQuery(this).is(':checked')) {
            if (jQuery('[name=subscriptions-select-user-check]').size() > 0) {

                instance.adminPanelShow();

                jQuery('[data-content=total-selected]').html(jQuery('[name=subscriptions-select-user-check]').size());

                jQuery('[name=subscriptions-select-user-check]').prop('checked', true);
            }
        } else {
            jQuery('.adminPanel_banner').hide();
            jQuery('[name=subscriptions-select-user-check]').removeAttr('checked');
        }

    });

    jQuery('[data-action=pager]').click(function () {

        var page = jQuery(this).data('page');

        jQuery('#currentPage').val(page);

        instance.search();
    });
}

SubscriptionsManagerAdminPanel.prototype.adminPanelShow = function () {
	
    jQuery('.adminPanel_banner').show();
    if (this.isWaitListSearchTypeActive() == 1) {
        jQuery('[data-action=massive-add-to-participant]').show();
        jQuery('[data-action=massive-confirm]').hide();
        jQuery('[data-action=massive-send-mail]').hide();
    } else {
        jQuery('[data-action=massive-add-to-participant]').hide();
        jQuery('[data-action=massive-confirm]').show();
        jQuery('[data-action=massive-send-mail]').show();
    }
    
}

SubscriptionsManagerAdminPanel.prototype.isWaitListSearchTypeActive = function () {

    //debugger;
    var tab = jQuery('.adminPanel__tab__nav--control > a.active');

    if (tab.attr('data-action') == 'filter-active')
        return 0;
    else
        return 1;
    }

SubscriptionsManagerAdminPanel.prototype.search = function () {

    var instance = this;

    jQuery.post(ajaxurl, {
        action: 'bit_novathon_subscriptions_admin_search',
        currentPage: jQuery('#currentPage').val(),
        textSearch: jQuery('[name=textSearch]').val(),
        orderBy: jQuery('#orderBy').val(),
        orderByVersus: jQuery('#orderByVersus').val(),
        waitlist: this.isWaitListSearchTypeActive(),
        status: jQuery('[name=filterSearch]').val()
    }, function (response) {
        jQuery('#subscriptions-admin-results').html(response);
        instance.bindListActions();

        instance.bindContextualMenuActions();
    });
}

SubscriptionsManagerAdminPanel.prototype.bindContextualMenuActions = function () {

    var instance = this;

    jQuery('[data-action=single-user-confirm]').click(function () {

        var userid = jQuery(this).data('target');

        instance.confirmUsers([userid]);
    });

    jQuery('[data-action=single-user-delete]').click(function () {

        var userid = jQuery(this).data('target');

        instance.deleteUsers([userid]);
    });

    jQuery('[data-action=single-user-send-mail]').click(function () {

        var userid = jQuery(this).data('target');

        instance.openSendMailDialog(userid);
    });

    jQuery('[data-action=single-user-edit]').click(function () {

        var userid = jQuery(this).data('target');

        instance.openEditProfile(userid);
    });
}

SubscriptionsManagerAdminPanel.prototype.confirmUsers = function (users) {

    var instance = this;

    jQuery.post(ajaxurl, {
        action: 'bit_novathon_subscriptions_admin_confirm_users',
        users: JSON.stringify(users)
    }, function (response) {

        alert('Users confirmed');

        instance.search();

        jQuery('.adminPanel_banner').hide();

    });
}

SubscriptionsManagerAdminPanel.prototype.deleteUsers = function (users) {

    var instance = this;

    if (confirm('Are you sure to delete selected users? Action cannot be undone')) {
        jQuery
            .post(ajaxurl, {
                action: 'bit_novathon_subscriptions_admin_delete_users',
                users: JSON.stringify(users)
            }, function (response) {

                alert('Users deleted');

                instance.search();

                jQuery('.adminPanel_banner').hide();

            });
    }
}

SubscriptionsManagerAdminPanel.prototype.sendMail = function () {
    
    var instance = this;

    var users = new Array();

    if (jQuery('[name=send-mail-userid]').val() == -1) {
        users = this.getUsersSelected();
    } else {
        users = [parseInt(jQuery('[name=send-mail-userid]').val())];
    }

    jQuery
        .post(ajaxurl, {
            action: 'bit_novathon_subscriptions_admin_send_mail',
            users: JSON.stringify(users),
            template: jQuery('#mailTemplate').val()
        }, function (response) {

            alert('Mails sent');

            jQuery('.adminPanel_banner').hide();

        });
}

SubscriptionsManagerAdminPanel.prototype.sendNewsletter = function () {
	
    var instance = this;

    var users = new Array();

    if (jQuery('[name=send-mail-userid]').val() == -1) {
        users = this.getUsersSelected();
    } else {
        users = [parseInt(jQuery('[name=send-mail-userid]').val())];
    }

    jQuery
        .post(ajaxurl, {
            action: 'bit_novathon_subscriptions_admin_send_mail',
            users: JSON.stringify(users),
            template: jQuery('#newsletterTemplate').val()
        }, function (response) {

            alert('Mails sent');

            jQuery('.adminPanel_banner').hide();

        });
}

SubscriptionsManagerAdminPanel.prototype.openEditProfile = function (userid) {

    var instance = this;

    jQuery.post(ajaxurl, {
        action: 'bit_novathon_subscriptions_admin_edit_profile_dialog',
        userid: userid
    }, function (response) {

        jQuery('body').append(response);

        var dialog = jQuery('#edit-profile-dialog').dialog({
            width: 800,
            close: function () {
                jQuery(this)
                    .dialog('destroy')
                    .remove();
            }
        });

        instance.bindEditProfileActions(dialog);
    })
}

SubscriptionsManagerAdminPanel.prototype.bindEditProfileActions = function (dialog) {

    var instance = this;

    jQuery('[data-action=edit-profile-close-dialog]').click(function () {
        dialog
            .dialog('destroy')
            .remove();
    })

    jQuery('[name=profession], [name=role]').change(function () {
        var val = jQuery(this).val();
        var name = jQuery(this).attr('name');

        if (val == 'Other') {
            jQuery('[data-related=' + name + ']').show();
        } else {
            jQuery('[data-related=' + name + ']').hide();
        }

        if (name == "profession") {
            if (val == "Employed") {
                jQuery('[name=companyname]').show();
            } else {
                jQuery('[name=companyname]')
                    .val('')
                    .hide();
            }
        }
    });

    jQuery('[data-action=edit-profile-save]').click(function () {

        var form = jQuery('[name=edit-profile-form]');

        if (form.valid()) {
            jQuery
                .post(ajaxurl, form.serialize(), function (response) {

                    if (response.result) {

                        instance.search();

                        var isEdit = false;

                        if (parseInt(jQuery('[name=userid]', form).val()) > -1) {
                            isEdit = true;
                        }

                        if (isEdit) {
                            alert('Profile saved');

                            dialog
                                .dialog('destroy')
                                .remove();
                        } else {
                            jQuery('[data-target=add-partecipant]').hide();

                            if (response.addedToWaitinglist) {
                                jQuery('[data-target=add-partecipant-waiting]').show();
                            } else {
                                jQuery('[data-target=add-partecipant-success]').show();
                            }
                        }
                    } else {
                        if (response.invaliddate) {
                            alert('Please specify a valid birth date');
                        }

                        if (response.userExists) {
                            alert('Email exists');
                        }
                    }

                }, 'json');
        }
    });
}

SubscriptionsManagerAdminPanel.prototype.addToPartipant = function (users) {
    var instance = this;

    jQuery.post(ajaxurl, {
        action: 'bit_novathon_subscriptions_admin_add_to_participant',
        users: JSON.stringify(users)
    }, function (response) {

        if (response.result) {
            alert('Users added to participants: ' + response.added + '. Not added: ' + response.not_added);
            instance.search();

            jQuery('.adminPanel_banner').hide();
        }

    }, 'json');
}
