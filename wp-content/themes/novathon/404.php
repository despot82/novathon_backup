<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<article>
	<section class="content canvas">
				<div class="content__wrapper">
					<h2 class="greyTitle">Ops, We can not find what you are looking for</h2><br>
					<div class="logo"><img src="<?php echo esc_url(get_template_directory_uri()) ?>/images/logo.png" alt="Novathon" width="150"></div>
					<br>
					<div class="content__title j-animate anim-zoom inView">
						<h3 class="greyTitle">Think, innovate and create</h3>
						<h4 class="greyTitle">23-24 September 2017, Zagreb</h4>
						<a class="color--orange" href="/">Go to site</a>
					</div>
				</div>
	</section>
</article>


<?php get_footer(); ?>
