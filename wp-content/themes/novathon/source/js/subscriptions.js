var SubscriptionManagerInstance;

var SubscriptionManager = function () {

    this.bindActions();

    SubscriptionManagerInstance = this;
}

SubscriptionManager.prototype.checkTermsAgreed = function () {

    if ($('#checkAgree').is(':checked')) {
        return true;
    } else {
        //alert('You must agree to terms and condition');
        this.showErrors($('.btn__submit--login'), 'You must agree to terms and condition');
    }
}
 
SubscriptionManager.prototype.bindActions = function () {

    var instance = this;

    var registrationForm = $('[name=subscribe-form]');

    registrationForm.validate();
 
    $('[data-action=subscribe]').click(function () {
         
        instance.hideErrors();

        if (instance.checkTermsAgreed()) {
            var form = $('[name=subscribe-form]');

            if (form.valid()) {

                var data = form.serialize();
                
                gcrValue = $("#g-recaptcha-response").val(); // getGoogle reCaptcha Response html element value
                
                gcr_success=false;
                   
                //Do the GOOGLE reCaptcha testing, ping the web service with our user's parameters.
                $.post( ajaxurl ,{"action":"gcr_check", "g-recaptcha-response":gcrValue})
                .done(function( response) {
                     
                	 var ajaxResponse = JSON.parse(response); 
                     gcr_success = ajaxResponse["success"];
                     
                     if(gcr_success) { 
                         //alert("GCR verification successfull !");
                         $.post(ajaxurl, data, function (ajaxResponse) {
                                			
                             instance.parseRegistrationResponse(ajaxResponse);
                                              
                         }, "json"); 
                                       
                     } else {
                         //alert("GCR verification unsuccessfull !");
                         instance.showErrors($('.btn__submit--login'), 'You must validate that you are not a robot');
                     } 
                }) 
                .fail(function() {  
                  //alert( "Error" );
                }); 
                 
                
            }
        }
    });

    $('[data-action=subscribe-facebook]').click(function () {

        instance.hideErrors();

        if ($('#checkAgree_social').is(':checked')) {
            instance.bindFacebookLogin(SubscriptionManagerInstance.registerWithFacebook);
        } else {
            instance.showErrors($('#checkAgree_social'), 'You must agree to terms and condition');
        }
        
        
    });
 
    $('[data-action=subscribe-twitter]').click(function () {

            $.post(ajaxurl, {
                action: 'bit_novathon_subscriptions_register_twitter'
            }, function (response) {

                //console.log(response.oauth_url); alert(response.oauth_url);
                if (response.result) {
                    document.location.href = response.oauth_url;
                }
            }, 'json');
    });

    $('[data-action=login-facebook]').click(function () {
    	//alert("in [data-action=login-facebook].click handler");
        instance.bindFacebookLogin(SubscriptionManagerInstance.loginWithFacebook);
    });

    $('[data-action=subscribe-google-plus]').click(function () {

      if($('#checkAgree_social').is(':checked')) {
        instance.hideErrors();
      } else {
        instance.showErrors($('#checkAgree_social'), 'You must agree to terms and condition');
      }
    });

    $('#checkAgree_social').change(function(){
      if($(this).is(':checked')){
        $('[data-action=subscribe-google-plus]').hide();
        instance.hideErrors();
      } else {
        $('[data-action=subscribe-google-plus]').show();
      }
    })
    if($('[data-action=subscribe-google]').length > 0){
      this.bindGoogleLogin($('[data-action=subscribe-google]')[0], this.registerWithGoogle, true);
    }

    if($('[data-action=login-google]').length > 0){
      this.bindGoogleLogin($('[data-action=login-google]')[0], this.loginWithGoogle, false);
    }

    $('[data-action=login]').click(function () {

        var form = $('[name=login-form]');

        if (form.valid()) {

            var data = form.serialize();

            $.post(ajaxurl, data, function (ajaxResponse) {
                SubscriptionManagerInstance.parseLoginResponse(ajaxResponse);
            }, "json");
        }
    });

    $('[data-action=delete-profile]').click(function () {

        instance.cancelRegistration();

    });

    $('[data-action=send-ticket-email]').click(function () {

        instance.sendTicketEmail();

    });

    this.bindCompleteRegistration();

    this.bindResetPassword();

    $('[data-action=resend-verify-email]').click(function () {

        $.post(ajaxurl, {
                action: 'bit_novathon_subscriptions_resend_verify_email'
            }, function (ajaxResponse) {

                if (ajaxResponse.result) {
                    alert('Email verification sent');
                }

            }, "json");
    });

    /*$(document).on('click', '.submitClick.active', function () {
        instance.updateProfile();
    });*/
}

SubscriptionManager.updateProfile = function (callback) {

    //alert('x');

    var form = $('[name=update-profile-form]');

    if (form.valid()) {
        $
            .post(ajaxurl, form.serialize(), function (response) {
                if (response.invalidinput) {
                    if (response.invaliddate) {
                        alert('Pleace specify a valid birth date');
                    }
                } else {
                    if (response.result) {
                        //alert('Profile updated');
                        callback();
                        document
                            .location
                            .reload();
                    }
                }
            }, 'json');
    }
};

SubscriptionManager.prototype.bindResetPassword = function () {

    var instance = this;

    $('[data-action=reset-password-action]').click(function () {

        instance.hideErrors();

        var form = $('[name=reset-password-form]');

        if (form.valid()) {

            var data = form.serialize();

            $.post(ajaxurl, data, function (ajaxResponse) {

                if (ajaxResponse.result) {
                    document.location.href = '/reset-password-check-email';
                } else {
                    //alert("Email not found");
                    instance.showErrors($('.btn__submit--resetPw'), 'Email not found');
                }

            }, "json");
        }

    });

    $('[data-action=reset-password-confirm-action]').click(function () {

        instance.hideErrors();
        var form = $('[name=reset-password-confirm-form]');
        var newpassword = $('[name=newpassword]').val();
        var newpasswordconfirm = $('[name=newpasswordconfirm]').val();

        if (form.valid()) {

            if (newpassword == newpasswordconfirm) {
                var data = form.serialize();

                $.post(ajaxurl, data, function (ajaxResponse) {

                    if (ajaxResponse.result) {
                        document.location.href = '/reset-password-success';
                    } else {
                        alert("Update error");
                    }

                }, "json");
            } else {
                alert('New passwords and confirm password must be the same');
                instance.showErrors($('.btn__submit--resetPw'), 'New passwords and confirm password must be the same');
            }
        }

    });

}

SubscriptionManager.prototype.bindCompleteRegistration = function () {
    var instance = this;

    $('[data-action=complete-registration-action]').click(function () {
        instance.completeRegistration();
    });

    $('[name="mobilephone"]').blur(function(a) {
        var form = $(this).closest('form')
        var validator = form.validate()
        validator.element($(this));
        $('[name=mobilephone]').val($(this).val());
    });

    $('[name=profession]').change(function () {
        var val = $(this).val();
        var name = $(this).attr('name');

        if (val == 'Other') {
            $('[data-related=' + val + ']').show();
        } else {
            $('[data-related="Other"]').hide();
        }

        if (val == 'Employed') {
            $('[data-related=' + val + ']').show();
        } else {
            $('[data-related="Employed"]').hide();
            $('[data-related="Employed"]').val('');
        }

        // if (name == "profession") {
        //     if (val == 'Employed') {
        //         $('[name=companyname]').show();
        //     } else {
        //         $('[name=companyname]').val('');
        //         $('[name=companyname]').hide();
        //     }
        // }
    });
}

SubscriptionManager.prototype.completeRegistration = function () {
    var form = $('[name=complete-registration-form]');

    this.hideErrors();

    var instance = this;

    if (form.valid()) {

        if ($('[name=challenge]:checked').size() == 0) {
            alert('Please select a challenge');
            this.showErrors($('.btn__submit--login'), 'Please select a challenge');
        } else {

            var data = form.serialize();

            $.post(ajaxurl, data, function (ajaxResponse) {

                if (ajaxResponse.invalidinput) {
                    if (ajaxResponse.invaliddate) {
                        instance.showErrors($('.btn__submit--login'), 'Please insert a valid birth date');
                    }
                } else {
                    if (ajaxResponse.result) {
                        document.location.href = '/profile';
                    } else {
                        alert("Update error");
                    }
                }

            }, "json");
        }
    }
}

SubscriptionManager.prototype.bindFacebookLogin = function (callback) {
    var instance = this;
 
    //FB.login(callback, {scope: 'public_profile,email'} );
    FB.login(callback, {auth_type:"rerequest", scope: 'public_profile,email'});
}

SubscriptionManager.prototype.registerWithFacebook = function (response) {
	    //alert("in registerWithFacebook");
        $('[data-action=loader]').show()
        
        if(!response.authResponse) {
            //alert("xing out");   
        	$('[data-action=loader]').hide();  
        	return false; // When user clicks cancel or x-s out of the popup
        }
        
        $.post(ajaxurl, {
            action: 'bit_novathon_subscriptions_register_facebook',
            accessToken: response.authResponse.accessToken
        }, function (ajaxResponse) {
            SubscriptionManagerInstance.parseRegistrationResponse(ajaxResponse);
        }, "json");

} 

SubscriptionManager.prototype.bindGoogleLogin = function (element, callback, isRegistration) {

    var instance = this;

    gapi.load('auth2', function () {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        auth2 = gapi
            .auth2
            .init({
                client_id: '309671304789-an0k9g7562d5gvrptot5u8smgsc54b55.apps.googleusercontent.com', cookiepolicy: 'single_host_origin',
                // Request scopes in addition to 'profile' and 'email' scope: 'additional_scope'
            });
        instance.attachSignin(element, callback, isRegistration);
    });
}

SubscriptionManager.prototype.registerWithGoogle = function (id_token) {

    var instance = this;
    $('[data-action=loader]').show()
    $.post(ajaxurl, {
        action: 'bit_novathon_subscriptions_register_google',
        token: id_token
    }, function (ajaxResponse) {
        SubscriptionManagerInstance.parseRegistrationResponse(ajaxResponse);
    }, "json");
}

SubscriptionManager.prototype.loginWithFacebook = function (response) {
	    //alert("in loginWithFacebook");
        $('[data-action=loader]').show()
        $.post(ajaxurl, {
            action: 'bit_novathon_subscriptions_login_facebook',
            accessToken: response.authResponse.accessToken
        }, function (ajaxResponse) {
            SubscriptionManagerInstance.parseLoginResponse(ajaxResponse);
        }, "json");
}

SubscriptionManager.prototype.loginWithGoogle = function (id_token) {
    $('[data-action=loader]').show()
    $.post(ajaxurl, {
            action: 'bit_novathon_subscriptions_login_google',
            token: id_token
        }, function (ajaxResponse) {
            SubscriptionManagerInstance.parseLoginResponse(ajaxResponse);
        }, "json");
}

SubscriptionManager.prototype.attachSignin = function (element, callback, isRegistration) {

    var instance = this;

    auth2.attachClickHandler(element, {}, function (googleUser) {

        instance.hideErrors();

        if (isRegistration) {
            if ($('#checkAgree_social').is(':checked')) {
                var id_token = googleUser
                    .getAuthResponse()
                    .id_token;

                callback(id_token);
            } else {
                instance.showErrors($('#checkAgree_social'), 'You must agree to terms and condition');
            }
        } else {
            var id_token = googleUser
                .getAuthResponse()
                .id_token;

            callback(id_token);
        }

    }, function (error) {
        //alert(JSON.stringify(error, undefined, 2));
    });
}

SubscriptionManager.prototype.parseRegistrationResponse = function (response) {
  //alert("in parseRegistrationResponse");
  //console.log("response is" + response.result);
   
  $('[data-action=loader]').hide()
    if (response.result) { 
        if (response.user_exists) {
            alert('User exists');
            grecaptcha.reset(); 
            this.showErrors($('.btn__submit--login'), 'Email exists');
        } else {
            window.location.href = '/subscribe-success';
        }
    } else {
        alert("In order to finalize your registration, please permit Novathon to retrieve your e-mail address.");
        FB.login(SubscriptionManagerInstance.registerWithFacebook, {auth_type:"rerequest", scope: 'public_profile,email'});
    }
}
 
SubscriptionManager.prototype.parseLoginResponse = function (response) {
    $('[data-action=loader]').hide()
    if (response.result) {
        //alert('Logged in');

        window.location.href = "/profile";
    } else {
        alert("Credentials error");
        this.showErrors($('.btn__submit--login'), 'Credentials error');
    }  
}

SubscriptionManager.prototype.cancelRegistration = function () {

    //if (confirm('Are you sure to cancel your registration?')) {
    $
        .post(ajaxurl, {
            action: 'bit_novathon_subscriptions_cancel_registration'
        }, function (ajaxResponse) {

            if (ajaxResponse.result) {
                //alert('Profile cancelled');

                document.location.href = '/';
            }

        }, "json");
    //}
}

SubscriptionManager.prototype.sendTicketEmail = function () {

    $
        .post(ajaxurl, {
            action: 'bit_novathon_subscriptions_send_ticket'
        }, function (ajaxResponse) {

            if (ajaxResponse.result) {
                alert('Ticket sent');
            }

        }, "json");
}

SubscriptionManager.prototype.showErrors = function (referenceObject, errors) {
    this.hideErrors();

    if (typeof(errors) == 'string')
        errors = [errors];

    var $container = $('<div />').addClass('loginForm__errorBox');
    var $ul = $('<ul />');

    $(errors).each(function (index, error) {

        var $li = $('<li />').html(error);
        $ul.append($li);

    });

    $container.append($ul);

    if(referenceObject.selector === '#checkAgree_social'){
      $container.insertAfter($('[for="checkAgree_social"]'));
    } else {
      $container.insertAfter($(referenceObject));
    }
}

SubscriptionManager.prototype.hideErrors = function () {
    $('.loginForm__errorBox').remove(); 
}

window.fbAsyncInit = function () {
    FB.init({appId: '717581245080040', xfbml: true, version: 'v2.8'});
    FB
        .AppEvents
        .logPageView();
};

(function (d, s, id) {
    var js,
        fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs
        .parentNode
        .insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
