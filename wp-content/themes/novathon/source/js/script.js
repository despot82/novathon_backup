@import "vendor/jquery-1.9.1.js";
@import "vendor/modernizr.js";
@import "libs/scrollrestoration_polyfill.js";
@import "libs/transition.js";
@import "libs/carousel.js";
@import "libs/pagination.js"
@import "libs/jquery.fancybox.js"
@import "libs/jquery.selectric.js"
@import "libs/jquery.validate.js";
@import "libs/additional-methods.js";
@import "subscriptions.js";

/* polyfill*/
if (!Object.assign) {
    Object.defineProperty(Object, 'assign', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function (target, firstSource) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
            }

            var to = Object(target);
            for (var i = 1; i < arguments.length; i++) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                    continue;
                }
                nextSource = Object(nextSource);

                var keysArray = Object.keys(Object(nextSource));
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                    var nextKey = keysArray[nextIndex];
                    var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                    if (desc !== undefined && desc.enumerable) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
            return to;
        }
    });
}

/**
 * Number.isFinite
 * Copyright (c) 2014 marlun78
 * MIT License, https://gist.github.com/marlun78/bd0800cf5e8053ba9f83
 *
 * Spec: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-number.isfinite
 */
if (typeof Number.isFinite !== 'function') {
    Number.isFinite = function isFinite(value) {
        // 1. If Type(number) is not Number, return false.
        if (typeof value !== 'number') {
            return false;
        }
        // 2. If number is NaN, +∞, or −∞, return false.
        if (value !== value || value === Infinity || value === -Infinity) {
            return false;
        }
        // 3. Otherwise, return true.
        return true;
    };
}

/* ====================================
   Onload functions
   ==================================== */
if ('scrollRestoration' in history) {
    history.scrollRestoration = 'manual';
}

"use strict";

(function () {

    new SubscriptionManager();

    //gestione section active
    var window_height,
        window_top_position,
        window_bottom_position;
    var $animation_elements = $('.j-animate');
    var $section__linked = $('.j-scrollLinked');

    var current_element
    var current_hash;
    var $window = $(window);
    var marginScroll = 100;
    marginScroll = 0;
    var enableStoreAdress = false;

    var parser = document.createElement('a');
    parser.href = window.location.href;
    var paths = parser
        .pathname
        .split("/");
    var current_hash = paths[paths.length - 1];

    function startCarousel() {
        $('.carousel').carousel({interval: 2000});
    }

    function getHeaderHeight() {
        return $('#header').outerHeight();
    }

    function saveState(path) {
        if (path) {
            if (path != current_hash) {
                if (history.pushState) {
                    var stateData = {
                        path: path
                    };
                    history.pushState({
                        path: path
                    }, null, path);
                }
            }
        }
    }

    function getPartVisible(el) {
        var $el = el,
            scrollTop = window_top_position,
            scrollBot = scrollTop + window_height,
            elTop = $el
                .offset()
                .top,
            elBottom = elTop + $el.outerHeight(),
            visibleTop = elTop < scrollTop
                ? scrollTop
                : elTop,
            visibleBottom = elBottom > scrollBot
                ? scrollBot
                : elBottom;
        return visibleBottom - visibleTop;
    }

    function checkIfElementIsVisible() {
        //main variable
        window_height = $window.height();
        window_top_position = $window.scrollTop();
        window_bottom_position = (window_top_position + window_height);
        var vish = 0;
        var hash;
        $.each($section__linked, function () {
            var $element = $(this);
            var th = getPartVisible($element);
            if (th > vish && th > 80) {
                vish = th;
                hash = $element.attr('id');
            }
        });

        if (hash != current_hash) {
            current_hash = hash;
            $('.j-scrollLink').removeClass("active");
            $('.j-scrollLink[href="#' + hash + '"]').addClass("active");
        }

        $
            .each($animation_elements, function () {
                var $element = $(this);
                isOnViewport($element)
                    ? $element.addClass('inView')
                    : $element.removeClass('inView');
            });

    }
    function isOnViewport(el) {
        var $element = $(el);
        var element_height = $element.outerHeight();
        var element_top_position = $element
            .offset()
            .top;
        var element_bottom_position = (element_top_position + element_height);
        return ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position));
    }
    function mobileNavigation() {
        $('.mobile-toggle')
            .click(function () {
                if ($('header').hasClass('open-nav')) {
                    $('header').removeClass('open-nav');
                } else {
                    $('header').addClass('open-nav');
                }
            });
        $('.header-menu a').click(function () {
            if ($('header').hasClass('open-nav')) {
                $('header').removeClass('open-nav');
            }
        });
    }
    function isEmailValid(o) {
        var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var valid = regex.test(o.val());
        if (!valid) {
            o
                .closest('.content__row')
                .addClass('error')
            return false
        }
    }

    function isValid() {
        var inputs = $('.j-validate'),
            error = $('.error'),
            valid = false;
        $('.content__row').removeClass('error');
        $.each(inputs, function (i, o) {
            var input = $(o)
            /*input.focus(function(){
				$(this).val('');
			})*/
            if (input.val() === '') {
                input
                    .closest('.content__row')
                    .addClass('error')
                valid = false;
            } else if (input.attr('name') === 'email') {
                valid = isEmailValid(input);
            }
        });

        if ($('.error').length === 0) {
            valid = true
        }
        return valid
    }
    function prepareForm() {
        $('.j-wrapperForm').attr("style", '');
        $('.j-wrapperFormError').attr("style", '');
        $('.j-wrapperFormSuccess').attr("style", '');

    }
    function closeForm() {
        $('.j-close')
            .unbind()
            .bind('click', function () {
                $('.form__wrapper').fadeOut();
            })
    }
    function openPopup() {
        $('.j-formOpen')
            .unbind()
            .on('click', function (e) {
                e.preventDefault();
                $('.form__wrapper').fadeIn(function () {
                    closeForm();
                    prepareForm();
                })

            });
        $('.j-popup')
            .unbind()
            .on('click', function (e) {
                e.preventDefault();
                $('.message-wrapper').fadeIn(function () {
                    $('.j-close')
                        .unbind()
                        .bind('click', function () {
                            $('.message-wrapper').fadeOut();
                        })
                })

            });
    }
    $(function () {
        $(".photos li")
            .slice(0, 12)
            .show();
        $(".j-viewMore").on('click', function (e) {
            e.preventDefault();
            $(".photos li:hidden")
                .slice(0, 12)
                .slideDown();
            if ($(".photos li:hidden").length == 0) {
                $(".j-viewMore").fadeOut('slow');
            }
        });
    });

    $(function () {
        //prepareform
        var contactForm = $('#contactsForm');
        contactForm
            .unbind()
            .submit(function (e) {
                $('#contactsForm').addClass('submitted');
                $('#contactsForm .text').remove();
                var valid = isValid();
                if (valid) {
                    $.ajax({
                        type: "POST",
                        url: "/wp-content/themes/novathon/php/mail.php",
                        data: contactForm.serialize(),
                        dataType: 'json',
                        success: function (data) {
                            $('.j-wrapperForm').hide();
                            $('.j-wrapperFormSuccess').show();
                        },
                        error: function (data) {
                            $('.j-wrapperForm').hide();
                            $('.j-wrapperFormError').show();
                        }
                    });
                }
                e.preventDefault();
            });

        $('.j-validate').blur(function () {
            if (contactForm.hasClass("submitted")) {
                isValid();
            }
        });

        //init carousel
        $('.j-scrollLink').click(function (e) {
            e.preventDefault();
            var dest_name = $(this).attr('href');
            var dest = $(dest_name);
            if (dest.length > 0) {
                saveState(dest_name);
                $("html, body").animate({
                    scrollTop: dest
                        .offset()
                        .top - getHeaderHeight()
                });
            }
        });
        openPopup();
        mobileNavigation();
        $window
            .on('scroll', checkIfElementIsVisible)
            .trigger('scroll');
        var pattern = /contacts/i;
        var path = window.location.pathname;
        if (pattern.test(path)) {
            $('.j-formOpen').trigger('click');
        }

        $('.fancybox')
            .on('click', function (e) {
                e.preventDefault();
            })
            .fancybox();

        if ($('.pageload').length > 0) {
            $('.pageload').fadeIn();
            $('.j-close')
                .unbind()
                .bind('click', function () {
                    $('.pageload').fadeOut();
                });
            /*setTimeout(function(){
					$('.pageload').fadeOut();
				},15000)*/
        }
        //custom select
        if ($('.section--login select').length > 0) {
            $('.section--login select').selectric({responsive: true, arrowButtonMarkup: '<span class="button icon-uni32"></span>'});
        }

        //overlay--profileSection
        if ($('.-js-open').length > 0) {
            $('.-js-open')
                .click(function (e) {
                    e.preventDefault();
                    $('.section--overlay')
                        .fadeIn(500)
                        .find('.-js-close')
                        .click(function () {
                            $('.section--overlay').fadeOut(300);
                        })
                })
        }
        //floting label
        if ($('.loginForm__form--floating').length > 0) {
            $('.loginForm__form--floating')
                .focusin(function () {
                    $(this)
                        .find('label:not(.error)')
                        .addClass('floating');
                })
                .focusout(function () {
                    var val = $(this).find('input')[0].value;
                    if (val === '') {
                        $(this)
                            .find('label:not(.error)')
                            .removeClass('floating');
                    }
                })
            setTimeout(function () {
                $('.loginForm__form--floating')
                    .each(function () {
                        var val = $(this).find('input')[0].value
                        if (val !== '') {
                            $(this)
                                .find('label:not(.error)')
                                .addClass('floating')
                                .css('opacity', '1')
                        } else {
                            $(this)
                                .find('label:not(.error)')
                                .css('opacity', '1');
                        }
                    })
            }, 100)
        }

        //page profile gestione input campo otherInput
        if ($('.profileWrapper form.wrap').length > 0) {
            $('select[name="role"], select[name="profession"]')
                .change(function () {
                    if (this.value === 'Other') {
                        $(this)
                            .closest('li')
                            .find('input[name=profession_other]')
                            .fadeIn(300);
                    } else {
                        $(this)
                            .closest('li')
                            .find('input[name=profession_other]')
                            .fadeOut(300);
                    }
                })
        }
        if ($('.-js-edit').length > 0) {
            $('.-js-edit')
                .click(function (e) {
                    e.preventDefault();
                    if ($(this).hasClass('submitClick') != true) {
                        $(this)
                            .addClass('submitClick')
                            .text('save')
                            .addClass('active')
                            .parent()
                            .find('.-js-cancel')
                            .fadeIn(300)
                            .click(function () {
                                $(this)
                                    .fadeOut(200)
                                    .parent()
                                    .find('.-js-edit')
                                    .text('edit')
                                    .removeClass('active submitClick')
                                    .parent()
                                    .find('select')
                                    .each(function () {
                                        this.disabled = true;
                                        $(this).selectric('refresh');
                                    })
                                $(this)
                                    .parent()
                                    .find('input')
                                    .each(function () {
                                        if (this.name != 'password') {
                                            this.readOnly = true;
                                        } else {
                                            this.style.display = '';
                                            $('.profileChangePw').removeClass('show');
                                        }
                                    })
                            })
                            .parent()
                            .find('input')
                            .each(function (e) {
                                if (this.name != 'password') {
                                    this.readOnly = false;
                                } else {
                                    this.style.display = 'none';
                                    $('.profileChangePw').addClass('show');
                                }
                            })
                        $(this)
                            .parent()
                            .find('select')
                            .each(function (e) {
                                this.disabled = false;
                                $(this).selectric('refresh');
                            })
                    } else {
                        $(this).removeClass('active submitClick');

                        var t = $(this);

                        SubscriptionManager.updateProfile(function () {
                            t
                                .text('edit')
                                .parent()
                                .find('.-js-cancel')
                                .fadeOut(200)
                                .parent()
                                .find('input')
                                .each(function (e) {
                                    this.readOnly = true
                                })
                            t
                                .parent()
                                .find('select')
                                .each(function (e) {
                                    this.disabled = true;
                                    $(this).selectric('refresh');
                                })
                        });
                    }
                })
        }

        //errore su select con selectric
        $('[name=complete-registration-button]')
            .click(function () {
                $('select[aria-required="true"]')
                    .each(function () {
                        var name = this.name
                        $('#' + name + '-error').appendTo('[data-cont="' + name + '"]');
                        $(this).selectric('refresh');
                    })
            })

    });

    window.onpopstate = function (event) {
        var e = event.state;
        if (e) {
            var d = document.getElementById(e.path);
            var topPos = d.offsetTop - getHeaderHeight();
        } else {
            var topPos = 0;
        }
        window.scrollTo(0, topPos);
    };

    //initial check
    if (current_hash.length > 0) {
        var d = document.getElementById(current_hash);
        if (d) {
            var topPos = d.offsetTop - getHeaderHeight();
            window.scrollTo(0, topPos);
            $('.j-scrollLink[href="#' + current_hash + '"]').addClass("active");
        }
    }

}());
