<?php

/**
 * @package Novathon
 */
/**
 * Nasconde il tedioso messaggio di aggiornamento WP
 */
add_action('admin_menu', 'bit_wphidenag');

/**
 * Nasconde il tedioso messaggio di aggiornamento WP
 */
function bit_wphidenag() {
  remove_action('admin_notices', 'update_nag', 3);
}

/**
 * Aggiunge il supporto menu a BO
 */
//add_theme_support('menus');

/**
 * Non caricare in pagina CSS e JS per il language selector, che non ci servono
 */
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_LANGUAGES_JS', true);

global $sitepress;
/**
 * Rimuove il meta generator di WPML
 */
remove_action('wp_head', array($sitepress, 'meta_generator_tag'));


/**
 * Rimuove la admin bar di WordPress
 */
show_admin_bar(false);

