<?php
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////  SETTINGS  //////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Aggiunge i campi per la pagina dei settings del tema
 */
function _bitnovathon_display_theme_settings_fields() {


  if (function_exists('wp_enqueue_media')) {
    wp_enqueue_media();
  } else {
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
  }

  //General section
  add_settings_section("general_section", "General Settings", null, "theme-options");
  //Pages section
  add_settings_section("pages_section", "Pages Settings", null, "theme-options");
  //Pageload message section
  add_settings_section("pageload_section", "Pageload message", null, "theme-options");
  //Sponsors section
  add_settings_section("sponsors_section", "Sponsors", null, "theme-options");
  //Social section
  add_settings_section("social_section", "Social", null, "theme-options");
  //Contacts section
  add_settings_section("contacts_section", "Contacts", null, "theme-options");


  //General section
  add_settings_field("_bitnovathon_header_logo", "Logo", "_bitnovathon_display_header_logo_element", "theme-options", "general_section");
  //Pages section
  add_settings_field("_bitnovathon_privpol_page", "Terms & Conditions page", "_bitnovathon_display_privpol_page_element", "theme-options", "pages_section");
  add_settings_field("_bitnovathon_privacy_page", "Privacy page", "_bitnovathon_display_privacy_page_element", "theme-options", "pages_section");
  add_settings_field("_bitnovathon_media_page", "Media page", "_bitnovathon_display_media_page_element", "theme-options", "pages_section");
  //Pageload message section
  add_settings_field("_bitnovathon_show_pl_message", "Show pageload message", "_bitnovathon_display_show_pl_message_element", "theme-options", "pageload_section");
  add_settings_field("_bitnovathon_pl_message", "Pageload message", "_bitnovathon_display_pl_message_element", "theme-options", "pageload_section");
  //Sponsors section
  add_settings_field("_bitnovathon_main_sponsor", "Main sponsors", "_bitnovathon_display_main_sponsor_element", "theme-options", "sponsors_section");
  add_settings_field("_bitnovathon_other_sponsors", "Other sponsors", "_bitnovathon_display_other_sponsors_element", "theme-options", "sponsors_section");
  //Social section
  add_settings_field("_bitnovathon_linkedin_link", "LinkedIn Link", "_bitnovathon_display_linkedin_element", "theme-options", "social_section");
  add_settings_field("_bitnovathon_facebook_link", "Facebook Link", "_bitnovathon_display_facebook_element", "theme-options", "social_section");
  add_settings_field("_bitnovathon_instagram_link", "Instagram Link", "_bitnovathon_display_instagram_element", "theme-options", "social_section");
  add_settings_field("_bitnovathon_youtube_link", "YouTube Link", "_bitnovathon_display_youtube_element", "theme-options", "social_section");
  //Contacts section
  add_settings_field("_bitnovathon_mail_to", "Mail to send user messages", "_bitnovathon_display_mail_to_element", "theme-options", "contacts_section");
  add_settings_field("_bitnovathon_recaptcha_sitekey", "Recaptcha sitekey", "_bitnovathon_display_recaptcha_sitekey_element", "theme-options", "contacts_section");
  add_settings_field("_bitnovathon_recaptcha_secret", "Recaptcha secret", "_bitnovathon_display_recaptcha_secret_element", "theme-options", "contacts_section");

  //General section
  register_setting("section", "_bitnovathon_header_logo");
	//Pages section
  register_setting("section", "_bitnovathon_privpol_page");
  register_setting("section", "_bitnovathon_privacy_page");
  register_setting("section", "_bitnovathon_media_page");
  //Pageload message section
  register_setting("section", "_bitnovathon_show_pl_message");
  register_setting("section", "_bitnovathon_pl_message");
  //Sponsors section
  register_setting("section", "_bitnovathon_main_sponsor");
  register_setting("section", "_bitnovathon_other_sponsors");
  //Social section
  register_setting("section", "_bitnovathon_linkedin_link");
  register_setting("section", "_bitnovathon_facebook_link");
  register_setting("section", "_bitnovathon_instagram_link");
  register_setting("section", "_bitnovathon_youtube_link");
  //Contacts section
  register_setting("section", "_bitnovathon_mail_to");
  register_setting("section", "_bitnovathon_recaptcha_sitekey");
  register_setting("section", "_bitnovathon_recaptcha_secret");
}

// Registra la funzione per visualizzare le opzioni del tema
add_action("admin_init", "_bitnovathon_display_theme_settings_fields");

/**
 * Aggiunge la pagina con i settings del tema
 */
function _bitnovathon_add_theme_menu_item() {
  add_menu_page("Theme Settings", "Theme Settings", "manage_options", "theme-settings", "_bitnovathon_theme_settings_page", null, 99);
}

function _bitnovathon_display_privpol_page_element() {
  ?>

  <select name="_bitnovathon_privpol_page" id="bit_privpol_page" class="regular-text">
    <?php foreach (_bit_settings_get_pages() as $page): ?>
      <option value="<?php echo $page->ID ?>"<?php if (get_option('_bitnovathon_privpol_page') == $page->ID): ?> selected="selected"<?php endif ?>><?php echo $page->post_title ?></option>
    <?php endforeach ?>
  </select>


  <?php
}


function _bitnovathon_display_privacy_page_element() {
  ?>

  <select name="_bitnovathon_privacy_page" id="bit_privacy_page" class="regular-text">
    <?php foreach (_bit_settings_get_pages() as $page): ?>
      <option value="<?php echo $page->ID ?>"<?php if (get_option('_bitnovathon_privacy_page') == $page->ID): ?> selected="selected"<?php endif ?>><?php echo $page->post_title ?></option>
    <?php endforeach ?>
  </select>


  <?php
}

function _bitnovathon_display_media_page_element() {
  ?>

  <select name="_bitnovathon_media_page" id="bit_media_page" class="regular-text">
    <?php foreach (_bit_settings_get_pages() as $page): ?>
      <option value="<?php echo $page->ID ?>"<?php if (get_option('_bitnovathon_media_page') == $page->ID): ?> selected="selected"<?php endif ?>><?php echo $page->post_title ?></option>
    <?php endforeach ?>
  </select>


  <?php
}

function _bitnovathon_display_header_logo_element() {
  ?>
  <img class="header_logo" src="<?php echo get_option('_bitnovathon_header_logo'); ?>" width="250"/>
  <input class="header_logo_url" type="text" name="_bitnovathon_header_logo" size="60" value="<?php echo get_option('_bitnovathon_header_logo'); ?>" style="display: none;">
  <div><a href="#" class="header_logo_upload">Change</a></div>
  </p>
  <?php
}

function _bitnovathon_display_show_pl_message_element() {
  ?>
  <input type="checkbox" name="_bitnovathon_show_pl_message" value="1" <?php checked(1, get_option('_bitnovathon_show_pl_message'), true); ?> />
  <?php
}

function _bitnovathon_display_pl_message_element() {
  ?>
  <textarea name="_bitnovathon_pl_message" rows="10" cols="55"><?php echo get_option('_bitnovathon_pl_message') ?></textarea>
  <?php
}

function _bitnovathon_display_main_sponsor_element() {
  ?>
  <select name="_bitnovathon_main_sponsor" id="bit_sponsor_main" class="regular-text">
    <?php foreach (_bit_settings_get_sponsors() as $sponsor): ?>
      <option value="<?php echo $sponsor->ID ?>"<?php if (get_option('_bitnovathon_main_sponsor') == $sponsor->ID): ?> selected="selected"<?php endif ?>><?php echo $sponsor->post_title ?></option>
    <?php endforeach ?>
  </select>
  <?php
}

function _bitnovathon_display_other_sponsors_element() {
  ?>
  <select multiple="multiple" name="_bitnovathon_other_sponsors[]" id="bit_sponsor_other" class="regular-text">
    <?php foreach (_bit_settings_get_sponsors() as $sponsor): ?>
      <option value="<?php echo $sponsor->ID ?>"<?php if (in_array($sponsor->ID, get_option('_bitnovathon_other_sponsors'))): ?> selected="selected"<?php endif ?>><?php echo $sponsor->post_title ?></option>
    <?php endforeach ?>
  </select>
  <?php
}

/**
 * Inizializza gli elementi per la pagina di settings
 */
function _bitnovathon_theme_settings_page() {
  ?>
  <div class="wrap">
    <h1>Novathon theme settings</h1>
    <form method="post" action="options.php">
      <?php
      settings_fields("section");
      do_settings_sections("theme-options");
      submit_button();
      ?>
    </form>
  </div>
  <?php
}

function _bitnovathon_display_linkedin_element() {
  ?>
  <input type="text" name="_bitnovathon_linkedin_link" value="<?php echo get_option('_bitnovathon_linkedin_link'); ?>" />
  <?php
}

function _bitnovathon_display_facebook_element() {
  ?>
  <input type="text" name="_bitnovathon_facebook_link" value="<?php echo get_option('_bitnovathon_facebook_link'); ?>" />
  <?php
}

function _bitnovathon_display_instagram_element() {
  ?>
  <input type="text" name="_bitnovathon_instagram_link" value="<?php echo get_option('_bitnovathon_instagram_link'); ?>" />
  <?php
}

function _bitnovathon_display_youtube_element() {
  ?>
  <input type="text" name="_bitnovathon_youtube_link" value="<?php echo get_option('_bitnovathon_youtube_link'); ?>" />
  <?php
}

function _bitnovathon_display_mail_to_element() {
  ?>
  <input type="text" name="_bitnovathon_mail_to" value="<?php echo get_option('_bitnovathon_mail_to'); ?>" />
  <?php
}

function _bitnovathon_display_recaptcha_sitekey_element() {
  ?>
  <input type="text" name="_bitnovathon_recaptcha_sitekey" value="<?php echo get_option('_bitnovathon_recaptcha_sitekey'); ?>" />
  <?php
}

function _bitnovathon_display_recaptcha_secret_element() {
  ?>
  <input type="text" name="_bitnovathon_recaptcha_secret" value="<?php echo get_option('_bitnovathon_recaptcha_secret'); ?>" />
  <?php
}

//Registra la funzione per aggiunge l'elemento delle opzioni a menu
add_action("admin_menu", "_bitnovathon_add_theme_menu_item");

/**
 * Restituisce come risultato l'elenco delle pagine nella lingua di default
 *
 * @global  $sitepress
 * @return array
 */
function _bit_settings_get_pages() {
  global $sitepress;
  if ($sitepress) {
    $current_lang = $sitepress->get_current_language();
    $new_lang = $sitepress->get_default_language();
    $sitepress->switch_lang($new_lang);
  }
  $posts = get_posts(array(
      'posts_per_page' => '-1',
      'post_type' => 'page',
      'orderby' => 'post_title',
      'suppress_filters' => 0,
      'order' => 'ASC',
  ));
  if ($sitepress) {
    $sitepress->switch_lang($current_lang);
  }
  return $posts;
}

function _bit_settings_get_sponsors() {
  global $sitepress;
  if ($sitepress) {
    $current_lang = $sitepress->get_current_language();
    $new_lang = $sitepress->get_default_language();
    $sitepress->switch_lang($new_lang);
  }
  $posts = get_posts(array(
      'posts_per_page' => '-1',
      'post_type' => \Bitmama\Wordpress\Plugin\Sponsors\Definitions\SponsorsDefinition::TYPE_IDENTIFIER,
      'orderby' => 'post_title',
      'suppress_filters' => 0,
      'order' => 'ASC',
  ));
  if ($sitepress) {
    $sitepress->switch_lang($current_lang);
  }
  return $posts;
}

function _bitnovathon_get_media_page_permalink() {
  return get_permalink(get_option('_bitnovathon_media_page'));
}

function _bitnovathon_user_get_privpol_page_permalink() {
  return get_permalink(get_option('_bitnovathon_privpol_page'));
}

function _bitnovathon_user_get_privacy_page_permalink() {
  return get_permalink(get_option('_bitnovathon_privacy_page'));
}
