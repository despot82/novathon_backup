<?php

/*
  Plugin Name: Bitmama Fast ACF
  Description: Versitile tool to define custom post types
  Version: 1.0
  Author: Bitmama
  Author URI: http://www.bitmama.it
 */

//Un poco di sicurezza che non si sa mai
if (!function_exists('add_action')) {
  header('Status: 403 Forbidden');
  header('HTTP/1.1 403 Forbidden');
  exit();
}

//require_once ABSPATH . "../vendor/autoload.php";

function BitFastAcfAutoloader($classname) {
  $_fl = str_replace("\\", "/", sprintf("%s/%s.php", __DIR__, $classname));
  if (file_exists($_fl)) {
    include_once $_fl;
  }
}

spl_autoload_register('BitFastAcfAutoloader');

add_action('wp_loaded', '_fast_acf_load_cpt');

function _fast_acf_load_cpt() {
  $loader = new Bitmama\Wordpress\Plugin\FastAcf\DefinitionsLoader();
  $loader->loadDefinitions();
}

require_once(__DIR__ . "/inc/settings.php");

function header_logo_script_enqueue($hook) {
  wp_enqueue_script('header_logo_script_inclusion', get_template_directory_uri() . '/admin/header_logo.js');
}

add_action('admin_enqueue_scripts', 'header_logo_script_enqueue');

