<?php

namespace Bitmama\Wordpress\Plugin\FastAcf;

/**
 * Description of BitFastAcf
 *
  $ff = Bit\BitWp::getHelper('FastAcf', array('cpt' => 'bit_video'));
  $f = $ff->getTrueFalseField('pippo', 'bit_pippo');
 *
 * @author andou
 */
class FastAcf {

  protected static $_key_prefix = 'field';
  protected $_cpt = FALSE;

  public function __construct($construct_arguments = NULL) {
    if (is_array($construct_arguments) && isset($construct_arguments['cpt'])) {
      $this->setCpt($construct_arguments['cpt']);
    }
  }

  /**
   * Sets the cpt slug to with produce the keys
   *
   * @param type $cpt
   * @return \Bit\Core\BitFastAcf
   */
  public function setCpt($cpt) {
    $this->_cpt = $cpt;
    return $this;
  }

  /**
   * Generates an unique key for this field
   *
   * @param type $name
   * @return type
   */
  public function getKey($name) {
    return sprintf("%s_%s", self::$_key_prefix, $this->_hash($this->_cpt, $name));
  }

  /**
   * Generates a field name
   *
   * @param type $name
   * @return type
   */
  public function getName($name) {
    return sprintf("%s_%s", $this->_cpt, $name);
  }

  /**
   * True/False field generator
   *
   * @param type $label
   * @param type $name
   * @param type $conditional_logic
   * @param type $message
   * @param type $default
   * @return type
   */
  public function getTrueFalseField($label, $name, $conditional_logic = FALSE, $message = '', $default = 0) {
    $header = $this->getFieldHeader($label, $name, 'true_false');
    $res = array_merge($header, array('message' => $message, 'default_value' => $default));
    $this->addConditionalLogic($res, $conditional_logic);
    return $res;
  }

  /**
   * Text field generator
   *
   * @param type $label
   * @param type $name
   * @param type $conditional_logic
   * @param type $formatting
   * @param type $placeholder
   * @param type $default
   * @param type $prepend
   * @param type $append
   * @param type $maxlenght
   * @return type
   */
  public function getTextField($label, $name, $conditional_logic = FALSE, $formatting = 'none', $placeholder = '', $default = '', $prepend = '', $append = '', $maxlenght = '') {
    $header = $this->getFieldHeader($label, $name, 'text');
    $res = array_merge($header, array(
        'default_value' => $default,
        'placeholder' => $placeholder,
        'prepend' => $prepend,
        'append' => $append,
        'formatting' => $formatting,
        'maxlength' => $maxlenght,
            )
    );
    $this->addConditionalLogic($res, $conditional_logic);
    return $res;
  }

  /**
   * Textarea generator
   *
   * @param type $label
   * @param type $name
   * @param type $conditional_logic
   * @param type $formatting
   * @param type $placeholder
   * @param type $default
   * @param type $column_width
   * @param type $rows
   * @param type $maxlenght
   * @return type
   */
  public function getTextAreaField($label, $name, $conditional_logic = FALSE, $formatting = 'none', $placeholder = '', $default = '', $column_width = '', $rows = '', $maxlenght = '') {
    $header = $this->getFieldHeader($label, $name, 'textarea');
    $res = array_merge($header, array(
        'default_value' => $default,
        'placeholder' => $placeholder,
        'column_width' => $column_width,
        'rows' => $rows,
        'formatting' => $formatting,
        'maxlength' => $maxlenght,
            )
    );
    $this->addConditionalLogic($res, $conditional_logic);
    return $res;
  }

  /**
   * Number field generator
   *
   * @param type $label
   * @param type $name
   * @param type $conditional_logic
   * @param type $default
   * @param type $placeholder
   * @param type $prepend
   * @param type $append
   * @param type $min
   * @param type $max
   * @param type $step
   * @return type
   */
  public function getNumberField($label, $name, $conditional_logic = FALSE, $default = 0, $placeholder = '', $prepend = '', $append = '', $min = '', $max = '', $step = '') {
    $header = $this->getFieldHeader($label, $name, 'number');
    $res = array_merge($header, array(
        'default_value' => $default,
        'placeholder' => $placeholder,
        'prepend' => $prepend,
        'append' => $append,
        'min' => $min,
        'max' => $max,
        'step' => $step,
            )
    );
    $this->addConditionalLogic($res, $conditional_logic);
    return $res;
  }

  /**
   * Select field generator
   *
   * @param type $label
   * @param type $name
   * @param type $choices
   * @param type $conditional_logic
   * @param type $default
   * @param type $allow_null
   * @param type $multiple
   * @return type
   */
  public function getSelectField($label, $name, $choices, $conditional_logic = FALSE, $default = '', $allow_null = 0, $multiple = 0) {
    $header = $this->getFieldHeader($label, $name, 'select');
    $res = array_merge($header, array(
        'choices' => $choices,
        'default_value' => $default,
        'allow_null' => $allow_null,
        'multiple' => $multiple,
            )
    );
    $this->addConditionalLogic($res, $conditional_logic);
    return $res;
  }

  public function getRadioField($label, $name, $choices, $conditional_logic = FALSE, $default = '', $allow_null = 0, $multiple = 0) {
      $header = $this->getFieldHeader($label, $name, 'radio');
      $res = array_merge($header, array(
              'choices' => $choices,
              'default_value' => $default,
              'allow_null' => $allow_null,
              'multiple' => $multiple,
      )
      );
      $this->addConditionalLogic($res, $conditional_logic);
      return $res;
  }

  /**
   * Ritorna un conditional logic array
   *
   * @param type $rules
   * @param type $status
   * @param type $allorany
   * @return type
   */
  public function getConditionalLogic($rules = array(), $status = 1, $allorany = 'all') {

    $_rules = array();

    foreach ($rules as $rule) {
      $_rules[] = $this->getConditionalLogicRule($rule['field'], $rule['value'], $rule['operator']);
    }

    return array(
        'status' => $status,
        'rules' => $_rules,
        'allorany' => $allorany,
    );
  }

  /**
   * Ritorna una regola di conditional logic
   *
   * @param type $field_name
   * @param type $value
   * @param type $operator
   * @return type
   */
  public function getConditionalLogicRule($field_name, $value, $operator = "==") {
    return
            array(
                'field' => $this->getKey($field_name),
                'operator' => $operator,
                'value' => $value,
    );
  }


  /**
   * Image field generator
   *
   * @param type $label
   * @param type $name
   * @param type $conditional_logic
   * @param type $save_format
   * @param type $preview
   * @param type $library
   * @return type
   */
  public function getFileField($label, $name, $conditional_logic = FALSE, $save_format = 'object', $library = 'all') {
      $header = $this->getFieldHeader($label, $name, 'file');
      $res = array_merge($header, array(
              'save_format' => $save_format,
              'library' => $library,
      )
      );
      $this->addConditionalLogic($res, $conditional_logic);
      return $res;
  }

  /**
   * Image field generator
   *
   * @param type $label
   * @param type $name
   * @param type $conditional_logic
   * @param type $save_format
   * @param type $preview
   * @param type $library
   * @return type
   */
  public function getImageField($label, $name, $conditional_logic = FALSE, $save_format = 'object', $preview = 'thumbnail', $library = 'all') {
    $header = $this->getFieldHeader($label, $name, 'image');
    $res = array_merge($header, array(
        'save_format' => $save_format,
        'preview_size' => $preview,
        'library' => $library,
            )
    );
    $this->addConditionalLogic($res, $conditional_logic);
    return $res;
  }

  /**
   * Tab field generator
   *
   * @param type $label
   * @param type $conditional_logic
   * @return type
   */
  public function getTab($label, $conditional_logic = FALSE) {
    $res = array(
        'key' => $this->getKey($label),
        'label' => $label,
        'name' => '',
        'type' => 'tab',
    );
    $this->addConditionalLogic($res, $conditional_logic);
    return $res;
  }

  /**
   * Adds the conditional logic
   *
   * @param type $field
   * @param type $conditional_logic
   */
  public function addConditionalLogic(&$field, $conditional_logic = FALSE) {
    if ($conditional_logic) {
      $field['conditional_logic'] = $conditional_logic;
    }
  }

  /**
   * Returns a generic header
   *
   * @param type $label
   * @param type $name
   * @param type $type
   * @return type
   */
  public function getFieldHeader($label, $name, $type) {
    return array(
        'key' => $this->getKey($name),
        'label' => $label,
        'name' => $name,
        'type' => $type,
    );
  }

  /**
   * Generates an hash for a field key
   *
   * @return type
   */
  protected function _hash() {
    $arg_list = func_get_args();
    $hashed = implode("-", $arg_list);
    return substr(dechex(crc32($hashed)), 0, 13);
  }

}

