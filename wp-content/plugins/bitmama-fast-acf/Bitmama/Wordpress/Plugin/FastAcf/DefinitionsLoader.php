<?php

namespace Bitmama\Wordpress\Plugin\FastAcf;

class DefinitionsLoader {

  const HOOK_NAME = "bitmama_fast_acf_pre_folder_loading";

  /**
   *
   * @var boolean
   */
  protected static $_definitions_loaded = FALSE;

  /**
   *
   * @var string
   */
  protected static $_definitions_suffix = "Definition";

  /**
   *
   * @var string 
   */
  protected static $_definition_abstract = 'CustomTypeDefinition.php';

  /**
   *
   * @var array
   */
  protected $_locations = array();

  /**
   * 
   * @return boolean
   */
  public function loadDefinitions() {
//    require_once ABSPATH . "../vendor/autoload.php";
    if (!self::$_definitions_loaded) {
      foreach ($this->getDefinitionsLocations() as $fetching_folder) {
        $_definitions = scandir($fetching_folder[0] . $fetching_folder[1]);
        foreach ($_definitions as $def) {
          if (preg_match("/^(.+)" . self::$_definitions_suffix . ".php$/", $def)) {
            if ($def != self::$_definition_abstract) {
              $class = sprintf("%s\\%s", str_replace("/", "\\", $fetching_folder[1]), str_replace(".php", "", $def));
              $class::defineType();
            }
          }
        }
      }
      self::$_definitions_loaded = TRUE;
    }
    return self::$_definitions_loaded;
  }

  /**
   * 
   * @return array
   */
  protected function getDefinitionsLocations() {
    $locations = $this->_locations;
    $locs = apply_filters(self::HOOK_NAME, $locations);
    return $locs;
  }

}