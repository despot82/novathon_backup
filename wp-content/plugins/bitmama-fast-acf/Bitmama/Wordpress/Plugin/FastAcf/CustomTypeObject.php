<?php

namespace Bitmama\Wordpress\Plugin\FastAcf;

abstract class CustomTypeObject {

  /**
   *
   * @var string
   */
  protected $_id;

  /**
   *
   * @var WpPost
   */
  protected $_post;

  const CAN_USE_TRANSLATION_FALLBACK = TRUE;

  /**
   * Restituisce il type identifier
   */
  public abstract function getTypeIdentifier();

  /**
   * Costruttore della classe
   * 
   * @param WP_Post $post
   */
  public function __construct($post, $use_fallback = FALSE) {
    if (is_object($post)) {
      $this->_id = $post->ID;
      $this->_post = $post;
    } else {
      $this->_id = $this->translateId($post, $use_fallback);
      $this->_post = get_post($this->_id);
    }
  }

  /**
   * Restituisce un array di informazioni con tutti i dati relativi a questo 
   * oggetto
   * 
   * @return array
   */
  public function getObjectDataArray() {
    $fields = get_fields($this->_id);
    if ($fields && is_array($fields)) {
      $fields['ID'] = $this->_id;
      $fields['post'] = $this->_post;
    } else {
      $fields = array();
    }
    return $fields;
  }

  /**
   * Ritorna l'id dell'oggetto
   * 
   * @return type
   */
  public function getTheId() {
    return $this->_id;
  }

  /**
   * Ritorna il path della pagina
   * 
   * @return type
   */
  public function getPath() {
    if (is_page()) {
      return get_page_uri($this->_id);
    } else {
      $post_type = get_post_type_object($this->getTypeIdentifier());
      return $post_type->rewrite['slug'] . '/' . $this->getPostName();
    }
  }

  /**
   * Traduce l'ID dell'oggetto sulla base dello store in cui ci si trova
   * 
   * @param int $id
   * @return int
   */
  protected function translateId($id, $use_fallback = FALSE) {
    if (function_exists('icl_object_id')) {
      $ret = icl_object_id($id, $this->getTypeIdentifier(), false);
      if (NULL != $ret && FALSE !== $ret) {
        return $ret;
      } else {
        return ($use_fallback && self::CAN_USE_TRANSLATION_FALLBACK) ? $id : NULL;
      }
    } else {
      return $id;
    }
  }

  /**
   * Is the object valid?
   * 
   * @return boolean
   */
  public function isValid() {
    if (isset($this->_post)) {
      return true;
    }
    return false;
  }

  /**
   * 
   * @param string $field_name
   * @param array $options
   * @return mixed
   */
  public function loadField($field_name, $options = array('load_value', TRUE)) {
    if (is_null($options)) {
      $options = array('load_value', TRUE);
    }
    $res = get_field_object($field_name, $this->_id, $options);
    return $res['value'];
  }

  /**
   * 
   * @param string $field_name
   * @return mixed
   */
  public function getField($field_name) {
    $res = get_field($field_name, $this->_id);
    return $res;
  }

  /**
   * Get attribute wrapper
   * 
   * This method check if the transport post object has the requested property(e.g. post_name)
   * if not, it loads the ACF field
   *
   * @param   string $method
   * @param   array $args
   * @return  mixed
   */
  public function __call($method, $args) {
    switch (substr($method, 0, 3)) {
      case 'get' :
        $key = $this->_underscore(substr($method, 3));
        if (property_exists($this->_post, $key)) {
          return $this->_post->$key;
        } else {
          $data = $this->getField($key);
        }
        return $data;
      case '_get' :
        $key = $this->_underscore(substr($method, 3));
        if (property_exists($this->_post, $key)) {
          return $this->_post->$key;
        } else {
          $data = $this->loadField($key, isset($args[0]) ? $args[0] : null);
        }
        return $data;
    }
  }

  /**
   * Cache for underscoring
   *
   * @var array
   */
  protected static $_underscoreCache = array();

  protected function _underscore($name) {
    if (isset(self::$_underscoreCache[$name])) {
      return self::$_underscoreCache[$name];
    }
    $result = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $name));
    self::$_underscoreCache[$name] = $result;
    return $result;
  }

  /**
   * Camelize a name/method
   * 
   * @param string $name
   * @return string
   */
  protected function _camelize($name) {
    return uc_words($name, '');
  }

}