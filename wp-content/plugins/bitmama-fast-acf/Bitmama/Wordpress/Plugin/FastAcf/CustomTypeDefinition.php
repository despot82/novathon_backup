<?php

namespace Bitmama\Wordpress\Plugin\FastAcf;

abstract class CustomTypeDefinition {

  /**
   * Returns the unique type identifier
   */
  public abstract static function getIdentifier();

  /**
   * Register the custom post type in WordPress
   */
  public abstract static function registerType();

  /**
   * Defines the custom post type fields
   */
  public abstract static function defineFields();

  /**
   * Defines the custom post type
   */
  public abstract static function defineType();

  /**
   * Returns an instance of FastAcf to define fields
   * 
   * @return \Bitmama\Wordpress\Plugin\FastAcf\FastAcf
   */
  protected static function getFastAcf($construct_arguments = NULL) {
    return new FastAcf($construct_arguments);
  }

  protected static function isAcfActive() {
    return function_exists("register_field_group");
  }

}