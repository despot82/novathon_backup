<?php

namespace Bitmama\Wordpress\Plugin\FastAcf;

abstract class CustomTypeObjectPrintable extends CustomTypeObject {

  /**
   * The template to be used
   */
  public abstract function getTemplate();

  /**
   * Outputs the HTML for this object
   * 
   * @param boolean $echo
   * @param boolean $clean
   * @return string
   */
  public function toHtml($echo = FALSE, $clean = TRUE) {
    $res = '';
    ob_start();
    require $this->getTemplate();
    $out = ob_get_contents();
    if (!empty($out) && $clean) {
      $res = str_replace(array("\n", "\r", "\t"), "", $out);
    }
    ob_end_clean();
    if ($echo) {
      echo $res;
    }
    return $res;
  }

}
