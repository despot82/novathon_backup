<?php

/*
  Plugin Name: Bitmama Speaker
  Description: A general WordPress plugin to manage speakers
  Version: 3.0
  Author: Bitmama
  Author URI: http://www.bitmama.it
 */

//Un poco di sicurezza che non si sa mai
if (!function_exists('add_action')) {
  header('Status: 403 Forbidden');
  header('HTTP/1.1 403 Forbidden');
  exit();
}

//require_once ABSPATH . "../vendor/autoload.php";

function BitSpeakersAutoloader($classname) {
  $_fl = str_replace("\\", "/", sprintf("%s/%s.php", __DIR__, $classname));
  if (file_exists($_fl)) {
    include_once $_fl;
  }
}

spl_autoload_register('BitSpeakersAutoloader');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////  CUSTOM TYPE DEFINITIONS  ////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Loads Custom Types definitions
 * 
 * @param array $locations
 * @return array
 */
function _speakers_load_definitions($locations) {
  $locations[] = array(__DIR__, "/Bitmama/Wordpress/Plugin/Speakers/Definitions");
  return $locations;
}

add_action('init', '_speakers_register_definitions_hook');

/**
 * Hooks in the custom type definition loading
 */
function _speakers_register_definitions_hook() {
  add_filter(Bitmama\Wordpress\Plugin\FastAcf\DefinitionsLoader::HOOK_NAME, '_speakers_load_definitions');
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////  TEMPLATING  ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the path for a template
 * 
 * @global string $_bit_api_suite_base_dir
 * @param string $template_name
 * @return string
 */
function speakers_get_template_file($template_name) {
  $_bit_api_suite_base_dir = __FILE__;
  return sprintf("%stemplates/%s.phtml", plugin_dir_path($_bit_api_suite_base_dir), $template_name);
}
