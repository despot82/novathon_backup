<?php

namespace Bitmama\Wordpress\Plugin\Speakers\Definitions;

use \Bitmama\Wordpress\Plugin\FastAcf\CustomTypeDefinition;

class SpeakersDefinition extends CustomTypeDefinition {
  /**
   * Define the type identifier
   */

  const TYPE_IDENTIFIER = 'b_speakers';
  /**
   * Slug
   */
  const SLUG = 'speakers';

  /**
   * Returns the identifier
   *
   * @return string
   */
  public static function getIdentifier() {
    return self::TYPE_IDENTIFIER;
  }

  public static function registerType() {
    add_action('init', __CLASS__ . '::defineType');
  }

  public static function getName($name) {
    return sprintf("%s_%s", self::TYPE_IDENTIFIER, $name);
  }

  public static function defineFields() {
    if (self::isAcfActive()) {

      $acf = self::getFastAcf(array("cpt" => self::TYPE_IDENTIFIER));
      $fields = array(
          $acf->getTextField('Name', self::getName('name'), FALSE, 'html'),
          $acf->getTextField('Speaker/mentor type', self::getName('speaker_or_mentor'), FALSE, 'html'),
          $acf->getTextField('Job Title', self::getName('title'), FALSE, 'html'),
          $acf->getTextAreaField('Bio', self::getName('description'), FALSE, 'html'),
          $acf->getImageField('Photo', self::getName('image')),
      );

      register_field_group(array(
          'id' => 'acf_speakers-content',
          'title' => 'Speaker/mentor item',
          'fields' => $fields,
          'location' => array(
              array(
                  array(
                      'param' => 'post_type',
                      'operator' => '==',
                      'value' => self::TYPE_IDENTIFIER,
                      'order_no' => 0,
                      'group_no' => 0,
                  ),
              ),
          ),
          'options' => array(
              'position' => 'normal',
              'layout' => 'default',
              'hide_on_screen' => array(
                  0 => 'the_content',
              ),
          ),
          'menu_order' => 0,
      ));
    }
  }

  /**
   * Defines the custom post type
   */
  public static function defineType() {
    $singular = "Speaker / Mentor";
    $plural = "Speakers / Mentors";
    $labels = array(
        'name' => _x($singular, 'post type general name'),
        'singular_name' => _x("$singular", 'post type singular name'),
        'add_new' => _x("Add New $singular", 'home item'),
        'add_new_item' => __("Add $singular"),
        'edit_item' => __("Edit $singular Item"),
        'new_item' => __("New $singular Item"),
        'view_item' => __("View $singular Item"),
        'search_items' => __("Search $plural"),
        'not_found' => __("No $singular found"),
        'not_found_in_trash' => __("No $plural found in Trash"),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => false,
        'capability_type' => array('b_speakers', 'b_speakerss'),
        'hierarchical' => false, //non presenta gerarchia
        'menu_position' => 5,
        'has_archive' => false,
        'supports' => array('title', 'editor')
    );

    register_post_type(self::getIdentifier(), $args);
    self::defineFields();
  }

}