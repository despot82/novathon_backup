<?php

namespace Bitmama\Wordpress\Plugin\Speakers\Template;

use Bitmama\Wordpress\Plugin\FastAcf\CustomTypeObjectPrintable;
use Bitmama\Wordpress\Plugin\Speakers\Definitions\SpeakersDefinition;

/**
 * Description of Product
 *
 * @author andou
 */
class Widget extends CustomTypeObjectPrintable {

  const TEMPLATE_NAME = 'widget';

  public function getTemplate() {
    return speakers_get_template_file(self::TEMPLATE_NAME);
  }

  /**
   * 
   * @return string
   */
  public function getTypeIdentifier() {
    return SpeakersDefinition::TYPE_IDENTIFIER;
  }

}

