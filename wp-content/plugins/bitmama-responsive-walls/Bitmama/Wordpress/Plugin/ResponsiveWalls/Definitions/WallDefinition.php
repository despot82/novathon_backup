<?php

namespace Bitmama\Wordpress\Plugin\ResponsiveWalls\Definitions;

use \Bitmama\Wordpress\Plugin\FastAcf\CustomTypeDefinition;

class WallDefinition extends CustomTypeDefinition {
  /**
   * Define the type identifier
   */

  const TYPE_IDENTIFIER = 'b_wall';
  /**
   * Slug
   */
  const SLUG = 'bw';


  /**
   * The field name that determines the wall linked to a page
   */
  const TEMPLATE_PAGE_FIELD = 'wall_template_link';


  /**
   *  The field name for the row type
   */
  const WALL_BRICK_FIELD = 'bit_wall_bricks';

  /**
   * The wall template name
   *
   * @var string
   */
  protected static $template_name = 'page-templates/wall.php';

  /**
   * Max rows number
   *
   * @var int
   */
  protected static $row_max = 10;

  /**
   * Returns the identifier
   * 
   * @return string
   */
  public static function getIdentifier() {
    return self::TYPE_IDENTIFIER;
  }

  public static function registerType() {
    add_action('init', __CLASS__ . '::defineType');
  }

  public static function getName($name) {
    return sprintf("%s_%s", self::TYPE_IDENTIFIER, $name);
  }

  public static function defineFields() {
    if (self::isAcfActive()) {

      $acf = self::getFastAcf(array("cpt" => self::TYPE_IDENTIFIER));

      $fields = array(
          $acf->getTab('Modules'),
          array(
              'key' => $acf->getKey(self::WALL_BRICK_FIELD),
              'label' => 'Modules',
              'name' => self::WALL_BRICK_FIELD,
              'type' => 'relationship',
              'column_width' => 60,
              'return_format' => 'id',
              'post_type' => array(
                  0 => BrickDefinition::TYPE_IDENTIFIER,
              ),
              'taxonomy' => array(
                  0 => 'all',
              ),
              'filters' => array(
                  0 => 'search',
              ),
              'result_elements' => array(
                  0 => 'post_type',
                  1 => 'post_title',
              ),
              'max' => 20,
          )
      );

      register_field_group(array(
          'id' => 'acf_wall-content',
          'title' => 'Responsive page',
          'fields' => $fields,
          'location' => array(
              array(
                  array(
                      'param' => 'post_type',
                      'operator' => '==',
                      'value' => self::TYPE_IDENTIFIER,
                      'order_no' => 0,
                      'group_no' => 0,
                  ),
              ),
          ),
          'options' => array(
              'position' => 'normal',
              'layout' => 'default',
              'hide_on_screen' => array(
                  0 => 'the_content',
              ),
          ),
          'menu_order' => 0,
      ));
    }
  }

  /**
   * Defines the custom post type
   */
  public static function defineType() {
    $singular = "Wall";
    $plural = "Walls";
    $labels = array(
        'name' => _x($singular, 'post type general name'),
        'singular_name' => _x("$singular", 'post type singular name'),
        'add_new' => _x("Add New $singular", 'home item'),
        'add_new_item' => __("Add $singular"),
        'edit_item' => __("Edit $singular Item"),
        'new_item' => __("New $singular Item"),
        'view_item' => __("View $singular Item"),
        'search_items' => __("Search $plural"),
        'not_found' => __("No $singular found"),
        'not_found_in_trash' => __("No $plural found in Trash"),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => false,
        'capability_type' => array('b_wall', 'b_walls'),
        'hierarchical' => false, //non presenta gerarchia
        'menu_position' => 5,
        'has_archive' => false,
        'supports' => array('title', 'editor')
    );

    register_post_type(self::getIdentifier(), $args);
    self::defineFields();
    self::defineFieldsForTemplate();
    self::registerColumns();
  }

  /**
   * Defines the fields in the wall template page
   */
  public static function defineFieldsForTemplate() {
    $acf = self::getFastAcf(array("cpt" => self::TYPE_IDENTIFIER . "_pagetemplate"));
    if (self::isAcfActive()) {
      register_field_group(array(
          'id' => 'acf_template-wall',
          'title' => 'Template Wall',
          'fields' => array(
              array(
                  'key' => $acf->getKey(self::TEMPLATE_PAGE_FIELD),
                  'label' => 'Wall',
                  'name' => self::TEMPLATE_PAGE_FIELD,
                  'type' => 'relationship',
                  'required' => 1,
                  'return_format' => 'id',
                  'post_type' => array(
                      0 => WallDefinition::TYPE_IDENTIFIER,
                  ),
                  'taxonomy' => array(
                      0 => 'all',
                  ),
                  'filters' => array(
                      0 => 'search',
                  ),
                  'result_elements' => array(
                      0 => 'post_type',
                      1 => 'post_title',
                  ),
                  'max' => 1,
              ),
          ),
          'location' => array(
              array(
                  array(
                      'param' => 'post_type',
                      'operator' => '==',
                      'value' => 'page',
                      'order_no' => 0,
                      'group_no' => 0,
                  ),
                  array(
                      'param' => 'page_template',
                      'operator' => '==',
                      'value' => self::$template_name,
                      'order_no' => 1,
                      'group_no' => 0,
                  ),
              ),
          ),
          'options' => array(
              'position' => 'normal',
              'layout' => 'no_box',
              'hide_on_screen' => array(
                  0 => 'the_content',
              ),
          ),
          'menu_order' => 0,
      ));
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////  COLUMNS IN THE BACKOFFICE  ////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   * Aggiunge i filtri necessari per aggiungere le colonne a backoffice
   */
  public static function registerColumns() {
    add_filter('manage_b_wall_posts_columns', __CLASS__ . '::addColumns');
    add_action('manage_posts_custom_column', __CLASS__ . '::populateCustomColumns', 10, 2);
  }

  /**
   * Aggiunge la colonna con la tipologia di template
   * 
   * @return string
   */
  public static function addColumns() {
    $cols = array(
        'cb' => '<input type="checkbox" />',
        'title' => __('Title'),
        'bit_wall_linked page' => __('Linked Page'),
        'date' => __('Date')
    );
    return $cols;
  }

  /**
   * Popola le colonne custom definite
   * 
   * @param type $column
   * @param type $post_id
   */
  public static function populateCustomColumns($column, $post_id) {
    switch ($column) {
      case 'bit_wall_linked page' :
        //Let's have a look on the pages that links this wall :)
        $pages = self::retrievePagesThatLinkWall($post_id);
        if (count($pages)) {
          echo "<ul>";
          foreach (self::retrievePagesThatLinkWall($post_id) as $page) {
            echo "<li>" . $page->post_name . " - [<a href=\"" . get_edit_post_link($page->ID) . "\">edit</a>]</li>";
          }
          echo "</ul>";
        } else {
          echo "None of the pages is linking this wall";
        }
        break;
    }
  }

  /**
   * Restituisce un array di pagine che linkano un determinato wall
   * 
   * @param int $wall_id
   * @return array
   */
  public static function retrievePagesThatLinkWall($wall_id) {
    // args
    $args = array(
        'numberposts' => -1,
        'post_type' => 'page',
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key' => self::TEMPLATE_PAGE_FIELD,
                'value' => sprintf("\"%s\";", $wall_id),
                'compare' => 'LIKE'
            ),
            array(
                'key' => 'location',
                'value' => 'Sydney',
                'compare' => 'LIKE'
            )
        )
    );

    $query = new \WP_Query($args);
    return $query->get_posts();
  }

}