<?php

namespace Bitmama\Wordpress\Plugin\ResponsiveWalls\Definitions;

use \Bitmama\Wordpress\Plugin\FastAcf\CustomTypeDefinition;
use \Bitmama\Wordpress\Plugin\Partners\Definitions\PartnersDefinition;
use \Bitmama\Wordpress\Plugin\Speakers\Definitions\SpeakersDefinition;

class BrickDefinition extends CustomTypeDefinition {
  /**
   * Define the type identifier
   */

  const TYPE_IDENTIFIER = 'b_brick';
  const BRICK_TYPE_FIELD = 'brick_type';
  const DOWNLOAD_TEMPLATE_FIELD = 'download_template';
  const WHITEBANNER_TEMPLATE_FIELD = 'white_template';
  /**
   * Slug
   */
  const SLUG = 'bb';
  /**
   * Brick type canvas
   */
  const TYPE_CANVAS = 'canvas';
	/**
   * Brick type canvas previos
   */
  const TYPE_CANVASPREVIOUS = 'canvas_previous';
   /**
   * Brick about event
   */
  const TYPE_ABOUTEVENT = 'about_event';
  /**
   * Brick the event
   */
  const TYPE_THEEVENT = 'the_event';
  /**
   * Brick challenges
   */
  const TYPE_CHALLENGES = 'challenges';
  /**
   * Brick winners
   */
  const TYPE_WINNERS = 'winners';
  /**
   * Brick media
   */
  const TYPE_MEDIA = 'media';
  /**
   * Brick mentors
   */
  const TYPE_MENTORS = 'mentors';
  /**
   * Brick speakers
   */
  const TYPE_SPEAKERS = 'speakers';
  /**
   * Brick program
   */
  const TYPE_PROGRAMME = 'programme';
  /**
   * Brick partners
   */
  const TYPE_PARTNERS = 'partners';
    /**
     * Brick booking
     */
    const TYPE_BOOKING = 'booking';
  /**
   * Returns the identifier
   *
   * @return string
   */
  public static function getIdentifier() {
    return self::TYPE_IDENTIFIER;
  }

  public static function registerType() {
    add_action('init', __CLASS__ . '::defineType');
  }

  public static function getName($name) {
    return sprintf("%s_%s", self::TYPE_IDENTIFIER, $name);
  }

  public static function defineFields() {

    if (self::isAcfActive()) {
      $acf = self::getFastAcf(array("cpt" => self::TYPE_IDENTIFIER));

      // CONDITIONAL LOGIC DEFS

      $aboutevent_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_ABOUTEVENT
              ),
          ),
          'allorany' => 'all',
      );

      $thevent_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_THEEVENT
              ),
          ),
          'allorany' => 'all',
      );

      $challenges_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_CHALLENGES
              ),
          ),
          'allorany' => 'all',
      );

      $winners_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_WINNERS
              ),
          ),
          'allorany' => 'all',
      );

      $media_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_MEDIA
              ),
          ),
          'allorany' => 'all',
      );

      $mentors_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_MENTORS
              ),
          ),
          'allorany' => 'all',
      );

      $speakers_form_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_SPEAKERS
              ),
          ),
          'allorany' => 'all',
      );

      $canvas_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_CANVAS
              ),
          ),
          'allorany' => 'all',
      );

      $canvasprevious_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_CANVASPREVIOUS
              ),
          ),
          'allorany' => 'all',
      );

      $programme_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_PROGRAMME
              ),
          ),
          'allorany' => 'all',
      );


      $partners_fields_conditional_logic = array(
          'status' => 1,
          'rules' => array(
              array(
                  'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                  'operator' => '==',
                  'value' => self::TYPE_PARTNERS
              ),
          ),
          'allorany' => 'all',
      );

      $booking_fields_conditional_logic = array(
        'status' => 1,
        'rules' => array(
            array(
                'field' => $acf->getKey(self::BRICK_TYPE_FIELD),
                'operator' => '==',
                'value' => self::TYPE_BOOKING
            ),
        ),
        'allorany' => 'all',
    );

      $fields = array(
          $acf->getTab("Module"),
          array(
              'key' => $acf->getKey(self::BRICK_TYPE_FIELD),
              'label' => 'Module type',
              'name' => self::BRICK_TYPE_FIELD,
              'type' => 'radio',
              'instructions' => 'Select the module type',
              'required' => 1,
              'choices' => array(
                  self::TYPE_CANVAS => 'Canvas/actual event',
                  self::TYPE_CANVASPREVIOUS => 'Canvas/previous event',
                  self::TYPE_ABOUTEVENT => 'Event description',
                  self::TYPE_THEEVENT => 'Data/Location/Prizes',
                  self::TYPE_CHALLENGES => 'Challenges',
                  self::TYPE_WINNERS => 'Winners',
                  self::TYPE_MEDIA => 'Media',
                  self::TYPE_MENTORS => 'Speakers/Mentors',
                  self::TYPE_SPEAKERS => 'Innovation Center',
                  self::TYPE_PROGRAMME => 'Program',
                  self::TYPE_PARTNERS => 'Partners',
                  self::TYPE_BOOKING => 'Booking'
              ),
              'other_choice' => 0,
              'save_other_choice' => 0,
              'default_value' => 'canvas',
              'layout' => 'horizontal',
          ),
          //CANVAS
          $acf->getTextField('Title', self::getName('canvas_title'), $canvas_fields_conditional_logic, "html"),
          $acf->getTextField('Subtitle', self::getName('canvas_subtitle'), $canvas_fields_conditional_logic, "html"),
          $acf->getImageField('Image', self::getName('canvas_image'), $canvas_fields_conditional_logic),
					//CANVAS OREVIOUS
          $acf->getTextField('Year', self::getName('canvasprevious_title'), $canvasprevious_fields_conditional_logic, "html"),
          $acf->getTextField('Subtitle', self::getName('canvasprevious_subtitle'), $canvasprevious_fields_conditional_logic, "html"),
          $acf->getImageField('Image', self::getName('canvasprevious_image'), $canvasprevious_fields_conditional_logic),
					$acf->getTextAreaField('Description', self::getName('canvasprevious_description'), $canvasprevious_fields_conditional_logic, "html"),
					$acf->getTextField('Extra info', self::getName('canvasprevious_extrainfo'), $canvasprevious_fields_conditional_logic, "html"),
          //ABOUT EVENT
          $acf->getTextField('Title', self::getName('aboutevent_title'), $aboutevent_fields_conditional_logic, "html"),
          $acf->getTextField('Subtitle', self::getName('aboutevent_subtitle'), $aboutevent_fields_conditional_logic, "html"),
          //$acf->getTextField('Videos Headline', self::getName('aboutevent_videos_headline'), $aboutevent_fields_conditional_logic, "html"),
          array(
              'key' => $acf->getKey(self::getName('aboutevent_hp_video')),
              'label' => 'Video',
              'name' => self::getName('aboutevent_hp_video'),
              'type' => 'relationship',
              'return_format' => 'object',
              'post_type' => array(
                  0 => 'b_media',
              ),
              'taxonomy' => array(
                  0 => 'all',
              ),
              'filters' => array(
                  0 => 'search',
              ),
              'result_elements' => array(
                  0 => 'post_type',
                  1 => 'post_title',
              ),
              'max' => 1,
              'conditional_logic' => $aboutevent_fields_conditional_logic,
          ),
          $acf->getTextAreaField('Text field 1', self::getName('aboutevent_description'), $aboutevent_fields_conditional_logic, "html"),
          $acf->getTextField('Text field 2', self::getName('aboutevent_challenges_title'), $aboutevent_fields_conditional_logic, "html"),
          $acf->getTextField('Text field 3', self::getName('aboutevent_challenges_description'), $aboutevent_fields_conditional_logic, "html"),
          //THE EVENT
          $acf->getTextField('Title', self::getName('event_title'), $thevent_fields_conditional_logic, "html"),
          $acf->getTextAreaField('Description', self::getName('event_descr_a'), $thevent_fields_conditional_logic, "html"),
          $acf->getImageField('Background Image', self::getName('event_image'), $thevent_fields_conditional_logic),
          $acf->getImageField('Image for date', self::getName('event_when_image'), $thevent_fields_conditional_logic),
          $acf->getTextAreaField('Description for date', self::getName('event_when_descr'), $thevent_fields_conditional_logic, "html"),
          $acf->getImageField('Image for location', self::getName('event_where_image'), $thevent_fields_conditional_logic),
          $acf->getTextAreaField('Description for location', self::getName('event_where_descr'), $thevent_fields_conditional_logic, "html"),
          $acf->getImageField('Image for prizes', self::getName('event_what_image'), $thevent_fields_conditional_logic),
          $acf->getTextAreaField('Description for prizes', self::getName('event_what_descr'), $thevent_fields_conditional_logic, "html"),
          $acf->getTextAreaField('Text field 1', self::getName('event_descr_b'), $thevent_fields_conditional_logic, "html"),
          $acf->getTextField('Text field 2', self::getName('event_challenges_title'), $thevent_fields_conditional_logic, "html"),
          $acf->getTextAreaField('Text field 3', self::getName('event_challenges_description'), $thevent_fields_conditional_logic, "html"),
          //CHALLENGES
          $acf->getTextField('Title', self::getName('challenges_title'), $challenges_fields_conditional_logic, "html"),
          $acf->getTextField('Subtitle', self::getName('challenges_subtitle'), $challenges_fields_conditional_logic, "html"),
					array(
              'key' => $acf->getKey(self::getName('challenges_pointlist')),
              'label' => 'List of challenges',
              'name' => self::getName('challenges_pointlist'),
              'type' => 'repeater',
              'instructions' => 'Add Points Row',
              'sub_fields' => array(
                  $acf->getTextField('Challenge name', self::getName('challenges_pointlist_title_name'), $challenges_fields_conditional_logic, "html"),
                  $acf->getTextAreaField('Challenge description', self::getName('challenges_pointlist_html_list'), $challenges_fields_conditional_logic, "html"),
              ),
              'row_min' => '',
              'row_limit' => '',
              'layout' => 'row',
              'button_label' => 'Add challenge',
              'conditional_logic' => $challenges_fields_conditional_logic,
          ),
          //WINNERS
          $acf->getTextField('Title', self::getName('winners_title'), $winners_fields_conditional_logic, "html", "Winners", "Winners"),
          $acf->getTextField('Subtitle', self::getName('winners_subtitle'), $winners_fields_conditional_logic, "html"),
          $acf->getImageField('Background Image', self::getName('winners_image'), $winners_fields_conditional_logic),
          array(
              'key' => $acf->getKey(self::getName('winner_groups')),
              'label' => 'Winners',
              'name' => self::getName('winner_groups'),
              'type' => 'repeater',
              'instructions' => 'Add Winner',
              'sub_fields' => array(
                  $acf->getTextField('Group Name', self::getName('winner_groups_name')),
                  $acf->getImageField('Group Logo', self::getName('winner_groups_image')),
                  $acf->getTextAreaField('Idea Description', self::getName('winner_groups_about'), FALSE, "html"),
                  $acf->getTextField('Title of "Member list"', self::getName('winner_groups_members_title'), false, "html", "Team members", "Team members"),
                  $acf->getTextAreaField('Members list', self::getName('winner_groups_members_list'), false, "html"),
                  $acf->getTextField('Displayed text for CTA', self::getName('winner_groups_details_text'), false, "html", "Details", "Details"),
                  $acf->getTextField('URL for CTA', self::getName('winner_groups_details_href')),
              ),
              'row_min' => '',
              'row_limit' => 3,
              'layout' => 'row',
              'button_label' => 'Add Winner',
              'conditional_logic' => $winners_fields_conditional_logic,
          ),
          //PROGRAMME
          $acf->getTextField('Title', self::getName('prog_title'), $programme_fields_conditional_logic, 'html', 'Preliminary program', 'Preliminary program'),
          $acf->getTextField('Subtitle', self::getName('prog_subtitle'), $programme_fields_conditional_logic, 'html'),
          $acf->getTextField('Disclaimer', self::getName('prog_disclaimer'), $programme_fields_conditional_logic, 'html'),
          array(
              'key' => $acf->getKey(self::getName('prog_days')),
              'label' => 'Days',
              'name' => self::getName('prog_days'),
              'type' => 'repeater',
              'instructions' => 'Add Days',
              'sub_fields' => array(
                  $acf->getTextField('Date (es. dd/mm)', self::getName('prog_day_name')),
                  $acf->getTextField('Day (es. day 01)', self::getName('prog_day_number')),
                  array(
                      'key' => $acf->getKey(self::getName('prog_day_schedule')),
                      'label' => 'Schedule',
                      'name' => self::getName('prog_day_schedule'),
                      'type' => 'repeater',
                      'instructions' => 'Add Schedule',
                      'sub_fields' => array(
                          $acf->getTextField('Time', self::getName('prog_day_schedule_time')),
                          $acf->getTextField('Program title', self::getName('prog_day_schedule_title'), false, "html"),
                          $acf->getTrueFalseField('Bold?', self::getName('prog_day_schedule_title_bold')),
                          $acf->getTextField('Speaker', self::getName('prog_day_schedule_subtitle')),
                      ),
                      'row_min' => '',
                      'row_limit' => 20,
                      'layout' => 'row',
                      'button_label' => 'Add program element'
                  )
              ),
              'row_min' => '',
              'row_limit' => 2,
              'layout' => 'row',
              'button_label' => 'Add new Day',
              'conditional_logic' => $programme_fields_conditional_logic,
          ),
          //MEDIA
          $acf->getTextField('Title', self::getName('media_title'), $media_fields_conditional_logic, "html", "Media", "Media"),
          $acf->getTextField('Subtitle', self::getName('media_subtitle'), $media_fields_conditional_logic, "html", "Check out our media library and relive the best moments of the event!", "Check out our media library and relive the best moments of the event!"),
          $acf->getTextField('Title of Video block', self::getName('media_videos_headline'), $media_fields_conditional_logic, "html", "Media", "Media"),
          array(
              'key' => $acf->getKey(self::getName('media_hp_video')),
              'label' => 'Videos',
              'name' => self::getName('media_hp_video'),
              'type' => 'relationship',
              'return_format' => 'object',
              'post_type' => array(
                  0 => 'b_media',
              ),
              'taxonomy' => array(
                  0 => 'all',
              ),
              'filters' => array(
                  0 => 'search',
              ),
              'result_elements' => array(
                  0 => 'post_type',
                  1 => 'post_title',
              ),
              'max' => 1,
              'conditional_logic' => $media_fields_conditional_logic,
          ),
          $acf->getTextField('Displayed text for Video CTA', self::getName('media_videos_all'), $media_fields_conditional_logic, "html", "ALL VIDEOS", "ALL VIDEOS"),
          $acf->getTextField('Title of photo block', self::getName('media_photos_headline'), $media_fields_conditional_logic, "html", "Photos", "Photos"),
          array(
              'key' => $acf->getKey(self::getName('media_hp_photo')),
              'label' => 'Photos',
              'name' => self::getName('media_hp_photo'),
              'type' => 'relationship',
              'return_format' => 'object',
              'post_type' => array(
                  0 => 'attachment',
              ),
              'taxonomy' => array(
                  0 => 'all',
              ),
              'filters' => array(
                  0 => 'search',
              ),
              'result_elements' => array(
                  0 => 'post_type',
                  1 => 'post_title',
              ),
              'max' => 1,
              'conditional_logic' => $media_fields_conditional_logic,
          ),
          $acf->getTextField('Displayed text for Photos CTA', self::getName('media_photos_all'), $media_fields_conditional_logic, "html", "ALL PHOTOS", "ALL PHOTOS"),
          //SPEAKERS
          $acf->getTextField('Title', self::getName('speakers_title'), $speakers_form_fields_conditional_logic, 'html', 'Innovation Center of Intesa Sanpaolo', 'Innovation Center of Intesa Sanpaolo'),
          $acf->getTextAreaField('Description', self::getName('speakers_description'), $speakers_form_fields_conditional_logic),
          $acf->getTextField('Displayed text for CTA', self::getName('speakers_readmore_text'), $speakers_form_fields_conditional_logic, 'html', 'Read more', 'Read more'),
          $acf->getTextField('URL for CTA', self::getName('speakers_readmore_href'), $speakers_form_fields_conditional_logic, 'html', 'http://www.world.intesasanpaolo.com/en/call_to_action/innovation-a-world-without-frontiers', 'http://www.world.intesasanpaolo.com/en/call_to_action/innovation-a-world-without-frontiers'),
          $acf->getTextField('Title of Speakers block', self::getName('speakers_meet_text'), $speakers_form_fields_conditional_logic, 'html', 'Meet our speakers during the event!', 'Meet our speakers during the event!'),
	$acf->getTextField('Disclaimer', self::getName('speakers_wainting_text'), $speakers_form_fields_conditional_logic, 'html', 'Our speakers will be announced soon.', 'Our speakers will be announced soon.'),
	array(
              'key' => $acf->getKey(self::getName('speakers')),
              'label' => 'Speakers/Innovation Center',
              'name' => self::getName('speakers'),
              'type' => 'repeater',
              'instructions' => 'Add speakers',
              'sub_fields' => array(
                  array(
                      'key' => $acf->getKey(self::getName('mentors_column')),
                      'label' => 'Select speaker or mentor from the list',
                      'name' => self::getName('mentors_column'),
                      'type' => 'relationship',
                      'column_width' => 60,
                      'return_format' => 'id',
                      'post_type' => array(
                          0 => SpeakersDefinition::TYPE_IDENTIFIER,
                      ),
                      'taxonomy' => array(
                          0 => 'all',
                      ),
                      'filters' => array(
                          0 => 'search',
                      ),
                      'result_elements' => array(
                          0 => 'post_type',
                          1 => 'post_title',
                      ),
                      'max' => 2
                  )
              ),
              'row_min' => '',
              'row_limit' => 15,
              'layout' => 'row',
              'button_label' => 'Add New',
              'conditional_logic' => $speakers_form_fields_conditional_logic,
          ),
          //MENTORS
          $acf->getTextField('Title', self::getName('mentors_title'), $mentors_fields_conditional_logic, 'html', 'Speakers and Mentors', 'Speakers and Mentors'),
          $acf->getTextField('Subtitle', self::getName('mentors_subtitle'), $mentors_fields_conditional_logic, 'html', 'They will join us to inspire you and to help you exploiting your full potential.', 'They will join us to inspire you and to help you exploiting your full potential.'),
          $acf->getTextField('Displayed text for CTA', self::getName('mentors_fulllist_text'), $mentors_fields_conditional_logic, 'html', 'Click here for the list of mentors.', 'Click here for the list of mentors.'),
          $acf->getTextField('URL for CTA', self::getName('mentors_fulllist_href'), $mentors_fields_conditional_logic, 'html', 'http://www.novathon.net/pdf/Novathon_mentors_ENG.pdf', 'http://www.novathon.net/pdf/Novathon_mentors_ENG.pdf'),
	$acf->getTextField('Disclaimer ', self::getName('mentors_wainting_text'), $mentors_fields_conditional_logic, 'html', 'Further speakers and mentors will be announced soon.', 'Further speakers and mentors will be announced soon.'),
	array(
              'key' => $acf->getKey(self::getName('mentors')),
              'label' => 'Speakers & Mentors',
              'name' => self::getName('mentors'),
              'type' => 'repeater',
              'instructions' => 'Add mentors',
              'sub_fields' => array(
                  array(
                      'key' => $acf->getKey(self::getName('mentors_column')),
                      'label' => 'Select speaker or mentor from the list',
                      'name' => self::getName('mentors_column'),
                      'type' => 'relationship',
                      'column_width' => 60,
                      'return_format' => 'id',
                      'post_type' => array(
                          0 => SpeakersDefinition::TYPE_IDENTIFIER,
                      ),
                      'taxonomy' => array(
                          0 => 'all',
                      ),
                      'filters' => array(
                          0 => 'search',
                      ),
                      'result_elements' => array(
                          0 => 'post_type',
                          1 => 'post_title',
                      ),
                      'max' => 2
                  )
              ),
              'row_min' => '',
              'row_limit' => 15,
              'layout' => 'row',
              'button_label' => 'Add new',
              'conditional_logic' => $mentors_fields_conditional_logic,
          ),
          //PARTNERS
          $acf->getTextField('Gold Partners Title', self::getName('gold_partners_title'), $partners_fields_conditional_logic, 'html', 'Gold Partners', 'Gold Partners'),
          array(
              'key' => $acf->getKey(self::getName('gold_partners')),
              'label' => 'Gold Partners',
              'name' => self::getName('gold_partners'),
              'type' => 'relationship',
              'column_width' => 60,
              'return_format' => 'id',
              'post_type' => array(
                  0 => PartnersDefinition::TYPE_IDENTIFIER,
              ),
              'taxonomy' => array(
                  0 => 'all',
              ),
              'filters' => array(
                  0 => 'search',
              ),
              'result_elements' => array(
                  0 => 'post_type',
                  1 => 'post_title',
              ),
              'max' => 10,
              'conditional_logic' => $partners_fields_conditional_logic
          ),
          $acf->getTextField('Partners Title', self::getName('standard_partners_title'), $partners_fields_conditional_logic, 'html', 'Partners', 'Partners'),
          array(
              'key' => $acf->getKey(self::getName('standard_partners')),
              'label' => 'Partners',
              'name' => self::getName('standard_partners'),
              'type' => 'relationship',
              'column_width' => 60,
              'return_format' => 'id',
              'post_type' => array(
                  0 => PartnersDefinition::TYPE_IDENTIFIER,
              ),
              'taxonomy' => array(
                  0 => 'all',
              ),
              'filters' => array(
                  0 => 'search',
              ),
              'result_elements' => array(
                  0 => 'post_type',
                  1 => 'post_title',
              ),
              'max' => 10,
              'conditional_logic' => $partners_fields_conditional_logic
          ),
          $acf->getTab("Menu"),
          $acf->getTrueFalseField('Show in menu', self::getName('menu_show')),
          $acf->getTextField('Title in menu', self::getName('menu_title'), array(
              'status' => 1,
              'rules' => array(
                  array(
                      'field' => $acf->getKey(self::getName('menu_show')),
                      'operator' => '==',
                      'value' => '1'
                  ),
              ),
              'allorany' => 'all',
          )),
          $acf->getTextField('Anchor in menu', self::getName('menu_anchor'), array(
              'status' => 1,
              'rules' => array(
                  array(
                      'field' => $acf->getKey(self::getName('menu_show')),
                      'operator' => '==',
                      'value' => '1'
                  ),
              ),
              'allorany' => 'all',
          )),
          //BOOKING
          $acf->getTextField('Title', self::getName('booking_title'), $booking_fields_conditional_logic, "html"),
          $acf->getTextAreaField('Text', self::getName('booking_text'), $booking_fields_conditional_logic, "html"),
          $acf->getImageField('Image', self::getName('booking_image'), $booking_fields_conditional_logic),
      );

      register_field_group(array(
          'id' => 'acf_brick-content',
          'title' => 'General Module',
          'fields' => $fields,
          'location' => array(
              array(
                  array(
                      'param' => 'post_type',
                      'operator' => '==',
                      'value' => self::TYPE_IDENTIFIER,
                      'order_no' => 0,
                      'group_no' => 0,
                  ),
              ),
          ),
          'options' => array(
              'position' => 'normal',
              'layout' => 'default',
              'hide_on_screen' => array(
                  0 => 'the_content',
              ),
          ),
          'menu_order' => 0,
      ));
    }
  }

  /**
   * Defines the custom post type
   */
  public static function defineType() {
    $singular = "Module";
    $plural = "Modules";
    $labels = array(
        'name' => _x($plural, 'post type general name'),
        'singular_name' => _x("$singular", 'post type singular name'),
        'add_new' => _x("Add New $singular", 'home item'),
        'add_new_item' => __("Add $singular"),
        'edit_item' => __("Edit $singular Item"),
        'new_item' => __("New $singular Item"),
        'view_item' => __("View $singular Item"),
        'search_items' => __("Search $plural"),
        'not_found' => __("No $singular found"),
        'not_found_in_trash' => __("No $plural found in Trash"),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => false,
        'capability_type' => array('b_brick', 'b_bricks'),
        'hierarchical' => false, //non presenta gerarchia
        'menu_position' => 5,
        'has_archive' => false,
        'supports' => array('title', 'editor')
    );

    register_post_type(self::getIdentifier(), $args);
    self::defineFields();
    self::registerColumns();
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////  COLUMNS IN THE BACKOFFICE  ////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   * Aggiunge i filtri necessari per aggiungere le colonne a backoffice
   */
  public static function registerColumns() {
    add_filter('manage_b_brick_posts_columns', __CLASS__ . '::addColumns');
    add_action('manage_posts_custom_column', __CLASS__ . '::populateCustomColumns', 10, 2);
    //add_filter("manage_edit-b_brick_sortable_columns", __CLASS__ . "::registerSortable");
  }

  /**
   * Registra le colonne ordinabili
   *
   * @param array $columns
   * @return string
   */
  public static function registerSortable($columns) {
    $columns['bit_brick_template'] = self::BRICK_TYPE_FIELD;
    return $columns;
  }

  /**
   * Aggiunge la colonna con la tipologia di template
   *
   * @return string
   */
  public static function addColumns() {
    $cols = array(
        'cb' => '<input type="checkbox" />',
        'title' => __('Title'),
        'bit_brick_template' => __('Template'),
        'date' => __('Date')
    );
    return $cols;
  }

  /**
   * Popola le colonne custom definite
   *
   * @param type $column
   * @param type $post_id
   */
  public static function populateCustomColumns($column, $post_id) {
    switch ($column) {
      case 'bit_brick_template' :
        $template = get_field(self::BRICK_TYPE_FIELD, $post_id);

        switch ($template) {
          case self::TYPE_CANVAS:
            echo 'Canvas/actual event';
            break;
          case self::TYPE_CANVASPREVIOUS:
            echo 'Canvas/previous event';
            break;
          case self::TYPE_ABOUTEVENT:
            echo 'Event description';
            break;
          case self::TYPE_CHALLENGES:
            echo 'Challenges';
            break;
          case self::TYPE_THEEVENT:
            echo 'Data/Location/Prizes';
            break;
          case self::TYPE_WINNERS:
            echo 'Winners';
            break;
          case self::TYPE_MEDIA:
            echo 'Media';
            break;
          case self::TYPE_MENTORS:
            echo 'Speakers/Mentors';
            break;
          case self::TYPE_SPEAKERS:
            echo 'Innovation Center';
            break;
          case self::TYPE_PROGRAMME:
            echo 'Program';
            break;
          case self::TYPE_PARTNERS:
            echo 'Partners';
            break;
          case self::TYPE_BOOKING:
            echo 'Booking';
            break;
          default:
            break;
        }
        break;
    }
  }

}
