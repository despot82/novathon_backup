<?php

namespace Bitmama\Wordpress\Plugin\ResponsiveWalls\Template;

use Bitmama\Wordpress\Plugin\FastAcf\CustomTypeObjectPrintable;
use Bitmama\Wordpress\Plugin\ResponsiveWalls\Definitions\WallDefinition;

/**
 * Description of Product
 *
 * @author andou
 */
class Wall extends CustomTypeObjectPrintable {

  const HOOK_NAME = "bitmama_wall_load_template";

  /**
   * 
   * @return string
   */
  public function getTypeIdentifier() {
    return WallDefinition::TYPE_IDENTIFIER;
  }

  public function getTemplate() {
    $template = responsive_walls_get_template_file("wall");
    return apply_filters(self::HOOK_NAME, $template);
  }

  public static function loadFromPage($page_id) {
    $_def_wall_obj = get_field(WallDefinition::TEMPLATE_PAGE_FIELD, $page_id);
    $wall_id = $_def_wall_obj[0];
    return new Wall((int) $wall_id, TRUE);
  }

  public function loadBricks() {
    $res = array();
    $bricks = $this->getField(WallDefinition::WALL_BRICK_FIELD);
    if ($bricks && count($bricks)) {
      foreach ($bricks as $brick_id) {
        $res[] = new Brick($brick_id, TRUE);
      }
    }
    return $res;
  }

}
