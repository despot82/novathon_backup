<?php

namespace Bitmama\Wordpress\Plugin\ResponsiveWalls\Template;

use Bitmama\Wordpress\Plugin\FastAcf\CustomTypeObjectPrintable;
use Bitmama\Wordpress\Plugin\ResponsiveWalls\Definitions\BrickDefinition;
use Bitmama\Wordpress\Plugin\News\Template\Widget;

/**
 * Description of Product
 *
 * @author andou
 */
class Brick extends CustomTypeObjectPrintable {

  public function getTemplate() {
    return responsive_walls_get_template_file('bricks/' . $this->getField(BrickDefinition::BRICK_TYPE_FIELD));
  }

  /**
   * 
   * @return string
   */
  public function getTypeIdentifier() {
    return BrickDefinition::TYPE_IDENTIFIER;
  }

  /**
   * Ritorna tutte le news associate a questo brick
   * 
   * @return \Bitmama\Wordpress\Plugin\News\Template\Widget
   */
  public function getNews() {
    $res = array();
    foreach ($this->getField(BrickDefinition::getName("news_brick_rel")) as $news) {
      $res[] = new Widget($news, TRUE);
    }
    return $res;
  }

  public function shouldShowInMenu() {
    $menu_show = $this->getField(BrickDefinition::getName('menu_show'));
    return $menu_show;
  }

  public function getMenuTitle() {
    $menu_title = $this->getField(BrickDefinition::getName('menu_title'));
    return $menu_title;
  }

  public function getMenuAnchor() {
    $anchor = $this->getField(BrickDefinition::getName('menu_anchor'));
    if (!$anchor) {
      $anchor = $this->getMenuTitle();
    }
    return sanitize_title($anchor);
  }

  public function getProductLink() {
    $prod_id = array_pop($this->getField(BrickDefinition::getName('prod_rel')));
    $prod = new \Bitmama\Wordpress\Plugin\Bankingproduct\Template\Single($prod_id, TRUE);
    $translated_id = $prod->getTheId();
    return $translated_id ? get_permalink($translated_id) : false;
  }

  public function getRelatedProducts() {
    return $this->getField(BrickDefinition::getName('related_products'));
  }

}

