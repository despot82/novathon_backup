<?php

namespace Bitmama\Wordpress\Plugin\ResponsiveWalls\Template;

use Bitmama\Wordpress\Plugin\FastAcf\CustomTypeObjectPrintable;
use Bitmama\Wordpress\Plugin\ResponsiveWalls\Definitions\ProductbrickDefinition;
use Bitmama\Wordpress\Plugin\News\Template\Widget;

/**
 * Description of Product
 *
 * @author andou
 */
class Prodbrick extends CustomTypeObjectPrintable {

  public function getTemplate() {
    return responsive_walls_get_template_file('bricks/' . $this->getField(ProductbrickDefinition::BRICK_TYPE_FIELD));
  }

  /**
   * 
   * @return string
   */
  public function getTypeIdentifier() {
    return ProductbrickDefinition::TYPE_IDENTIFIER;
  }

  /**
   * Ritorna tutte le news associate a questo brick
   * 
   * @return \Bitmama\Wordpress\Plugin\News\Template\Widget
   */
  public function getNews() {
    $res = array();
    foreach ($this->getField(ProductbrickDefinition::getName("news_brick_rel")) as $news) {
      $res[] = new Widget($news, TRUE);
    }
    return $res;
  }

  public function getProductLink() {
    $prod_id = array_pop($this->getField(ProductbrickDefinition::getName('prod_rel')));
    $prod = new \Bitmama\Wordpress\Plugin\Bankingproduct\Template\Single($prod_id, TRUE);
    $translated_id = $prod->getTheId();
    return $translated_id ? get_permalink($translated_id) : false;
  }

  public function getRelatedProducts() {
    return $this->getField(ProductbrickDefinition::getName('related_products'));
  }

}

