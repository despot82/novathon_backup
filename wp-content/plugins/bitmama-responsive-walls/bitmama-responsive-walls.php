<?php

/*
  Plugin Name: Bitmama Responsive Walls
  Description: A general WordPress plugin to manage responsive walls
  Version: 3.0
  Author: Bitmama
  Author URI: http://www.bitmama.it
 */

//Un poco di sicurezza che non si sa mai
if (!function_exists('add_action')) {
  header('Status: 403 Forbidden');
  header('HTTP/1.1 403 Forbidden');
  exit();
}

//require_once ABSPATH . "../vendor/autoload.php";

function BitResWallAutoloader($classname) {
  $_fl = str_replace("\\", "/", sprintf("%s/%s.php", __DIR__, $classname));
  if (file_exists($_fl)) {
    include_once $_fl;
  }
}

spl_autoload_register('BitResWallAutoloader');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////  CUSTOM TYPE DEFINITIONS  ////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Loads Custom Types definitions
 * 
 * @param array $locations
 * @return array
 */
function _responsive_walls_load_definitions($locations) {
  $locations[] = array(__DIR__, "/Bitmama/Wordpress/Plugin/ResponsiveWalls/Definitions");
  return $locations;
}

add_action('init', '_responsive_walls_register_definitions_hook');

/**
 * Hooks in the custom type definition loading
 */
function _responsive_walls_register_definitions_hook() {
  add_filter(Bitmama\Wordpress\Plugin\FastAcf\DefinitionsLoader::HOOK_NAME, '_responsive_walls_load_definitions');
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////  TEMPLATING  ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the path for a template
 * 
 * @global string $_bit_api_suite_base_dir
 * @param string $template_name
 * @return string
 */
function responsive_walls_get_template_file($template_name) {
  $_bit_api_suite_base_dir = __FILE__;
  return sprintf("%stemplates/%s.phtml", plugin_dir_path($_bit_api_suite_base_dir), $template_name);
}

if (function_exists("register_field_group")) {
  register_field_group(array(
      'id' => 'acf_winners-page',
      'title' => 'Winners page',
      'fields' => array(
          array(
              'key' => 'field_58d9127fcced6',
              'label' => 'Winners',
              'name' => 'page_win_winners',
              'type' => 'repeater',
              'instructions' => 'Add winners',
              'sub_fields' => array(
                  array(
                      'key' => 'field_58d912b7cced7',
                      'label' => 'Place',
                      'name' => 'page_win_winners_place',
                      'type' => 'select',
                      'column_width' => '',
                      'choices' => array(
                          'first' => 'First',
                          'second' => 'Second',
                          'third' => 'Third',
                      ),
                      'default_value' => '',
                      'allow_null' => 0,
                      'multiple' => 0,
                  ),
                  array(
                      'key' => 'field_58d9141accedd',
                      'label' => 'Place image',
                      'name' => 'page_win_winners_place_image',
                      'type' => 'image',
                      'column_width' => '',
                      'save_format' => 'object',
                      'preview_size' => 'thumbnail',
                      'library' => 'all',
                  ),
                  array(
                      'key' => 'field_58d9134dcced8',
                      'label' => 'Team name',
                      'name' => 'page_win_winners_team_name',
                      'type' => 'text',
                      'column_width' => '',
                      'default_value' => '',
                      'placeholder' => '',
                      'prepend' => '',
                      'append' => '',
                      'formatting' => 'none',
                      'maxlength' => '',
                  ),
                  array(
                      'key' => 'field_58d914af979e6',
                      'label' => 'Team Idea',
                      'name' => 'page_win_winners_team_idea',
                      'type' => 'text',
                      'column_width' => '',
                      'default_value' => '',
                      'placeholder' => '',
                      'prepend' => '',
                      'append' => '',
                      'formatting' => 'none',
                      'maxlength' => '',
                  ),
                  array(
                      'key' => 'field_58d91388cceda',
                      'label' => 'Team Challenge',
                      'name' => 'page_win_winners_challenge',
                      'type' => 'text',
                      'column_width' => '',
                      'default_value' => '',
                      'placeholder' => '',
                      'prepend' => '',
                      'append' => '',
                      'formatting' => 'none',
                      'maxlength' => '',
                  ),
                  array(
                      'key' => 'field_58d913a8ccedb',
                      'label' => 'Team Description',
                      'name' => 'page_win_winners_description',
                      'type' => 'textarea',
                      'column_width' => '',
                      'default_value' => '',
                      'placeholder' => '',
                      'prepend' => '',
                      'append' => '',
                      'formatting' => 'html',
                      'maxlength' => '',
                  ),
                  array(
                      'key' => 'field_58d913cfccedc',
                      'label' => 'Team members',
                      'name' => 'page_win_winners_team_members',
                      'type' => 'text',
                      'instructions' => 'Insert a comma to separate team members',
                      'column_width' => '',
                      'default_value' => '',
                      'placeholder' => 'John Doe, Robert Smith, Mark Foo',
                      'prepend' => '',
                      'append' => '',
                      'formatting' => 'none',
                      'maxlength' => '',
                  ),
              ),
              'row_min' => '',
              'row_limit' => '',
              'layout' => 'row',
              'button_label' => 'Add Winner',
          ),
      ),
      'location' => array(
          array(
              array(
                  'param' => 'page_template',
                  'operator' => '==',
                  'value' => 'page-templates/winners.php',
                  'order_no' => 0,
                  'group_no' => 0,
              ),
          ),
      ),
      'options' => array(
          'position' => 'normal',
          'layout' => 'default',
          'hide_on_screen' => array(
              0 => 'the_content',
              1 => 'featured_image',
          ),
      ),
      'menu_order' => 0,
  ));
}
