<?php

/*
  Plugin Name: Novathon ACL
  Description: ACL for custom roles
  Version: 1.0
  Author: Bitmama
 * Author URI: http://www.bitmama.it
 */

//Catch anyone trying to directly acess the plugin - which isn't allowed
if (!function_exists('add_action')) {
  header('Status: 403 Forbidden');
  header('HTTP/1.1 403 Forbidden');
  exit();
}

// Aggiunge il nuovo ruolo nel momento in cui viene registrato il plugin
register_activation_hook(__FILE__, '_bit_add_novathon_editor_role');

// Aggiunge le capa
add_action('admin_init', '_bit_add_cpt_caps', 999);

/**
 * Aggiunge il ruolo bit_with_editor
 */
function _bit_add_novathon_editor_role() {
  add_role('bit_novathon_editor', 'With Editor', array(
      'read' => true,
      'edit_posts' => false,
      'delete_posts' => false,
      'publish_posts' => false,
      'upload_files' => false,
          )
  );
}

/**
 * Aggiunge capabilities
 */
function _bit_add_cpt_caps() {
// the roles
  $roles = array('bit_novathon_editor', 'editor', 'administrator');
//the cpts
  $cpts = array(
      array(
          'sing' => 'b_brick',
          'plur' => 'b_bricks'
      ),
      array(
          'sing' => 'b_wall',
          'plur' => 'b_walls'
      ),
      array(
          'sing' => 'page',
          'plur' => 'pages'
      ),
      array(
          'sing' => 'b_partners',
          'plur' => 'b_partnerss'
      ),
      array(
          'sing' => 'b_speakers',
          'plur' => 'b_speakerss'
      ),
  );
  foreach ($cpts as $cpt) {
    _bit_add_role_caps($roles, $cpt["sing"], $cpt["plur"]);
  }
}

/**
 * Aggiunge capabilities
 *
 * @param array $roles
 * @param string $singular
 * @param string $plural
 */
function _bit_add_role_caps($roles, $singular, $plural) {
  foreach ($roles as $the_role) {
    $role = get_role($the_role);
    $role->add_cap('upload_files');
    $role->add_cap('read');
//catalogslide - read
    $role->add_cap("read_$singular");
    $role->add_cap("read_private_$plural");
//catalogslide - edit
    $role->add_cap("edit_$singular");
    $role->add_cap("edit_$plural");
    $role->add_cap("edit_others_$plural");
    $role->add_cap("edit_published_$plural");
//catalogslide - publish
    $role->add_cap("publish_$plural");
//catalogslide - delete
    $role->add_cap("delete_$singular");
    $role->add_cap("delete_others_$plural");
    $role->add_cap("delete_private_$plural");
    $role->add_cap("delete_published_$plural");

    $role->add_cap('edit_theme_options');
  }
}

