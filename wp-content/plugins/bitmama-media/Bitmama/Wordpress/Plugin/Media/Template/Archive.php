<?php

namespace Bitmama\Wordpress\Plugin\Media\Template;

use Bitmama\Wordpress\Plugin\FastAcf\CustomTypeObjectPrintable;
use Bitmama\Wordpress\Plugin\Media\Definitions\MediaDefinition;

/**
 * Description of Product
 *
 * @author andou
 */
class Archive extends CustomTypeObjectPrintable {

  const TEMPLATE_NAME = 'archive';

  public function getTemplate() {
    return media_get_template_file(self::TEMPLATE_NAME);
  }

  /**
   * 
   * @return string
   */
  public function getTypeIdentifier() {
    return MediaDefinition::TYPE_IDENTIFIER;
  }

}

