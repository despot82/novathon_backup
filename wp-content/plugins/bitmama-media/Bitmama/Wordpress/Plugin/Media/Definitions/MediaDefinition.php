<?php

namespace Bitmama\Wordpress\Plugin\Media\Definitions;

use \Bitmama\Wordpress\Plugin\FastAcf\CustomTypeDefinition;

class MediaDefinition extends CustomTypeDefinition {
  /**
   * Define the type identifier
   */

  const TYPE_IDENTIFIER = 'b_media';

  /**
   * Il nome del campo con la tipologia di media
   */
  const MEDIA_TYPE_FIELD = 'media_type';
  /**
   * Slug
   */
  const SLUG = 'event_media';
  /**
   * Media type photo
   */
  const TYPE_PHOTO = 'photo';
  /**
   * Media type YouTube Video
   */
  const TYPE_YOUTUBE_VIDEO = 'youtube_video';

  /**
   * Returns the identifier
   *
   * @return string
   */
  public static function getIdentifier() {
    return self::TYPE_IDENTIFIER;
  }

  public static function registerType() {
    add_action('init', __CLASS__ . '::defineType');
  }

  public static function getName($name) {
    return sprintf("%s_%s", self::TYPE_IDENTIFIER, $name);
  }

  public static function defineFields() {
    if (self::isAcfActive()) {

      $acf = self::getFastAcf(array("cpt" => self::TYPE_IDENTIFIER));

//      $photo_fields_conditional_logic = array(
//          'status' => 1,
//          'rules' => array(
//              array(
//                  'field' => $acf->getKey(self::MEDIA_TYPE_FIELD),
//                  'operator' => '==',
//                  'value' => self::TYPE_PHOTO
//              ),
//          ),
//          'allorany' => 'all',
//      );
//
//      $youtube_video_fields_conditional_logic = array(
//          'status' => 1,
//          'rules' => array(
//              array(
//                  'field' => $acf->getKey(self::MEDIA_TYPE_FIELD),
//                  'operator' => '==',
//                  'value' => self::TYPE_YOUTUBE_VIDEO
//              ),
//          ),
//          'allorany' => 'all',
//      );
//
//
//      $fields = array(
//          $acf->getTextField('Title', self::getName('name'), FALSE, 'html'),
//          $acf->getTextAreaField('Description', self::getName('description'), FALSE, 'html'),
//          array(
//              'key' => $acf->getKey(self::MEDIA_TYPE_FIELD),
//              'label' => 'Media type',
//              'name' => self::MEDIA_TYPE_FIELD,
//              'type' => 'radio',
//              'instructions' => 'Select the media type',
//              'required' => 1,
//              'choices' => array(
//                  self::TYPE_PHOTO => 'Photo',
//                  self::TYPE_YOUTUBE_VIDEO => 'YouTube Video',
//              ),
//              'other_choice' => 0,
//              'save_other_choice' => 0,
//              'default_value' => 'canvas',
//              'layout' => 'horizontal',
//          ),
//          $acf->getTextField('YouTube ID', self::getName('youtube_id'), $youtube_video_fields_conditional_logic),
//          $acf->getImageField('Photo', self::getName('image'), $photo_fields_conditional_logic),
//      );

      $fields = array(
//          $acf->getTextField('Title', self::getName('name'), FALSE, 'html'),
//          $acf->getTextAreaField('Description', self::getName('description'), FALSE, 'html'),
          $acf->getTextField('YouTube ID', self::getName('youtube_id')), 
      );

      register_field_group(array(
          'id' => 'acf_media-content',
          'title' => 'Event media item',
          'fields' => $fields,
          'location' => array(
              array(
                  array(
                      'param' => 'post_type',
                      'operator' => '==',
                      'value' => self::TYPE_IDENTIFIER,
                      'order_no' => 0,
                      'group_no' => 0,
                  ),
              ),
          ),
          'options' => array(
              'position' => 'normal',
              'layout' => 'default',
              'hide_on_screen' => array(
                  0 => 'the_content',
              ),
          ),
          'menu_order' => 0,
      ));
    }
  }

  /**
   * Defines the custom post type
   */
  public static function defineType() {
    $singular = "Event Video";
    $plural = "Event Videos";
    $labels = array(
        'name' => _x($singular, 'post type general name'),
        'singular_name' => _x("$singular", 'post type singular name'),
        'add_new' => _x("Add New $singular", 'home item'),
        'add_new_item' => __("Add $singular"),
        'edit_item' => __("Edit $singular Item"),
        'new_item' => __("New $singular Item"),
        'view_item' => __("View $singular Item"),
        'search_items' => __("Search $plural"),
        'not_found' => __("No $singular found"),
        'not_found_in_trash' => __("No $plural found in Trash"),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => false,
        'capability_type' => array('b_speakers', 'b_speakerss'),
        'hierarchical' => false, //non presenta gerarchia
        'menu_position' => 5,
        'has_archive' => false,
        'supports' => array('title', 'editor')
    );

    register_post_type(self::getIdentifier(), $args);
    self::defineFields();
  }

}