<?php

/*
  Plugin Name: Bitmama Media
  Description: A general WordPress plugin to manage media content
  Version: 3.0
  Author: Bitmama
  Author URI: http://www.bitmama.it
 */

//Un poco di sicurezza che non si sa mai
if (!function_exists('add_action')) {
  header('Status: 403 Forbidden');
  header('HTTP/1.1 403 Forbidden');
  exit();
}

//require_once ABSPATH . "../vendor/autoload.php";

function BitMediaAutoloader($classname) {
  $_fl = str_replace("\\", "/", sprintf("%s/%s.php", __DIR__, $classname));
  if (file_exists($_fl)) {
    include_once $_fl;
  }
}

spl_autoload_register('BitMediaAutoloader');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////  CUSTOM TYPE DEFINITIONS  ////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Loads Custom Types definitions
 * 
 * @param array $locations
 * @return array
 */
function _media_load_definitions($locations) {
  $locations[] = array(__DIR__, "/Bitmama/Wordpress/Plugin/Media/Definitions");
  return $locations;
}

add_action('init', '_media_register_definitions_hook');

/**
 * Hooks in the custom type definition loading
 */
function _media_register_definitions_hook() {
  add_filter(Bitmama\Wordpress\Plugin\FastAcf\DefinitionsLoader::HOOK_NAME, '_media_load_definitions');
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////  TEMPLATING  ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the path for a template
 * 
 * @global string $_bit_api_suite_base_dir
 * @param string $template_name
 * @return string
 */
function media_get_template_file($template_name) {
  $_bit_api_suite_base_dir = __FILE__;
  return sprintf("%stemplates/%s.phtml", plugin_dir_path($_bit_api_suite_base_dir), $template_name);
}

if (function_exists("register_field_group")) {
  register_field_group(array(
      'id' => 'acf_media-page-fields',
      'title' => 'Media Page Fields',
      'fields' => array(
          array(
              'key' => 'field_58da2bbf102bd',
              'label' => 'Title of Video block',
              'name' => 'media_page_video_headline',
              'type' => 'text',
              'default_value' => 'Videos',
              'placeholder' => 'Videos',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
          ),
          array(
              'key' => 'field_58da2b13679a1',
              'label' => 'Videos',
              'name' => 'media_page_videos',
              'type' => 'relationship',
              'return_format' => 'object',
              'post_type' => array(
                  0 => 'b_media',
              ),
              'taxonomy' => array(
                  0 => 'all',
              ),
              'filters' => array(
                  0 => 'search',
              ),
              'result_elements' => array(
                  0 => 'post_type',
                  1 => 'post_title',
              ),
              'max' => '',
          ),
          array(
              'key' => 'field_58da2b61679a3',
              'label' => 'Displayed text for Playlist CTA',
              'name' => 'media_page_video_playlist_text',
              'type' => 'text',
              'default_value' => 'View Playlist',
              'placeholder' => 'View Playlist',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
          ),
          array(
              'key' => 'field_58da2b34679a2',
              'label' => 'Playlist URL',
              'name' => 'media_page_video_playlist_link',
              'type' => 'text',
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
          ),
          array(
              'key' => 'field_58da2ba6102bc',
              'label' => 'Title of photo block',
              'name' => 'media_page_photos_headline',
              'type' => 'text',
              'default_value' => 'Photos',
              'placeholder' => 'Photos',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
          ),
          array(
              'key' => 'field_58da25d3aad1b',
              'label' => 'Photos',
              'name' => 'media_page_photos',
              'type' => 'relationship',
              'return_format' => 'object',
              'post_type' => array(
                  0 => 'attachment',
              ),
              'taxonomy' => array(
                  0 => 'all',
              ),
              'filters' => array(
                  0 => 'search',
              ),
              'result_elements' => array(
                  0 => 'post_type',
                  1 => 'post_title',
              ),
              'max' => '',
          ),
          array(
              'key' => 'field_58da30581c8c3',
              'label' => 'Displayed text for "view more" CTA',
              'name' => 'media_page_photos_view_more',
              'type' => 'text',
              'default_value' => 'View more',
              'placeholder' => 'View more',
              'prepend' => '',
              'append' => '',
              'formatting' => 'html',
              'maxlength' => '',
          ),
//          array(
//              'key' => 'field_58da308e1c8c4',
//              'label' => 'Photos show at start',
//              'name' => 'media_page_photos_show_at_start',
//              'type' => 'number',
//              'default_value' => 12,
//              'placeholder' => 12,
//              'prepend' => '',
//              'append' => '',
//              'min' => '',
//              'max' => '',
//              'step' => '',
//          ),
      ),
      'location' => array(
          array(
              array(
                  'param' => 'page_template',
                  'operator' => '==',
                  'value' => 'page-templates/media.php',
                  'order_no' => 0,
                  'group_no' => 0,
              ),
          ),
      ),
      'options' => array(
          'position' => 'normal',
          'layout' => 'default',
          'hide_on_screen' => array(
              0 => 'the_content',
          ),
      ),
      'menu_order' => 0,
  ));
}
