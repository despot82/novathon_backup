<?php

namespace Bitmama\Wordpress\Plugin\Partners\Template;

use Bitmama\Wordpress\Plugin\FastAcf\CustomTypeObjectPrintable;
use Bitmama\Wordpress\Plugin\Partners\Definitions\PartnersDefinition;

/**
 * Description of Product
 *
 * @author andou
 */
class Archive extends CustomTypeObjectPrintable {

  const TEMPLATE_NAME = 'archive';

  public function getTemplate() {
    return partners_get_template_file(self::TEMPLATE_NAME);
  }

  /**
   * 
   * @return string
   */
  public function getTypeIdentifier() {
    return PartnersDefinition::TYPE_IDENTIFIER;
  }

}

