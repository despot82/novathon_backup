<?php

namespace Bitmama\Wordpress\Plugin\Partners\Definitions;

use \Bitmama\Wordpress\Plugin\FastAcf\CustomTypeDefinition;

class PartnersDefinition extends CustomTypeDefinition {
  /**
   * Define the type identifier
   */

  const TYPE_IDENTIFIER = 'b_partners';
  /**
   * Slug
   */
  const SLUG = 'partners';

  /**
   * Returns the identifier
   *
   * @return string
   */
  public static function getIdentifier() {
    return self::TYPE_IDENTIFIER;
  }

  public static function registerType() {
    add_action('init', __CLASS__ . '::defineType');
  }

  public static function getName($name) {
    return sprintf("%s_%s", self::TYPE_IDENTIFIER, $name);
  }

  public static function defineFields() {
    if (self::isAcfActive()) {

      $acf = self::getFastAcf(array("cpt" => self::TYPE_IDENTIFIER));
      $fields = array(
          $acf->getTextField('Name', self::getName('name'), FALSE, 'html'),
          $acf->getTextField('Link URL', self::getName('read_more_link')),
          $acf->getImageField('Image', self::getName('image')),
      );

      register_field_group(array(
          'id' => 'acf_partners-content',
          'title' => 'Partner item',
          'fields' => $fields,
          'location' => array(
              array(
                  array(
                      'param' => 'post_type',
                      'operator' => '==',
                      'value' => self::TYPE_IDENTIFIER,
                      'order_no' => 0,
                      'group_no' => 0,
                  ),
              ),
          ),
          'options' => array(
              'position' => 'normal',
              'layout' => 'default',
              'hide_on_screen' => array(
                  0 => 'the_content',
              ),
          ),
          'menu_order' => 0,
      ));
    }
  }

  /**
   * Defines the custom post type
   */
  public static function defineType() {
    $singular = "Partner";
    $plural = "Partners";
    $labels = array(
        'name' => _x($singular, 'post type general name'),
        'singular_name' => _x("$singular", 'post type singular name'),
        'add_new' => _x("Add New $singular", 'home item'),
        'add_new_item' => __("Add $singular"),
        'edit_item' => __("Edit $singular Item"),
        'new_item' => __("New $singular Item"),
        'view_item' => __("View $singular Item"),
        'search_items' => __("Search $plural"),
        'not_found' => __("No $singular found"),
        'not_found_in_trash' => __("No $plural found in Trash"),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => false,
        'capability_type' => array('b_partners', 'b_partnerss'),
        'hierarchical' => false, //non presenta gerarchia
        'menu_position' => 5,
        'has_archive' => false,
        'supports' => array('title', 'editor')
    );

    register_post_type(self::getIdentifier(), $args);
    self::defineFields();
  }

}