<?php

namespace Bitmama\Wordpress\Plugin\Sponsors\Template;

use Bitmama\Wordpress\Plugin\FastAcf\CustomTypeObjectPrintable;
use Bitmama\Wordpress\Plugin\Sponsors\Definitions\SponsorsDefinition;

/**
 * Description of Product
 *
 * @author andou
 */
class Single extends CustomTypeObjectPrintable {

  const TEMPLATE_NAME = 'single';

  public function getTemplate() {
    return sponsors_get_template_file(self::TEMPLATE_NAME);
  }

  /**
   * 
   * @return string
   */
  public function getTypeIdentifier() {
    return SponsorsDefinition::TYPE_IDENTIFIER;
  }

}

