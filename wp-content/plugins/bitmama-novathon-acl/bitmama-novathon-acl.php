<?php

/*
 Plugin Name: Bitmama Novathon ACL
 Plugin URI: http://
 Description: Plugin for ACL (Access Control List) for pages related to Novathon
 Version: 1.0
 Author: Vladimir Despotovic
 Author URI: http://
 License:GPL2
 */


register_activation_hook( __FILE__, 'addRoles' );


function addRoles() {
	
	//Remove previously added roles (if any)
	remove_role("subscriber");
	remove_role("participant");
	remove_role("team_leader");
	remove_role("hostess");
	remove_role("juror");
	
	remove_role("Subscriber");
	remove_role("Participant");
	remove_role("Team_Leader");
	remove_role("Hostess");
	remove_role("Juror");
	
	
	//Add roles
	add_role("participant", "Novathon Participant");
	add_role("team_leader", "Novathon Team Leader");
	add_role("hostess", "Novathon Hostess");
	add_role("juror", "Novathon Juror");
	
	//Assign capabilities to roles
	
	
	$participant_role=get_role("participant"); 
	$team_leader_role=get_role("team_leader"); 
	$hostess_role=get_role("hostess"); 
	$juror_role=get_role("juror"); 
	
	$participant_role->add_cap("view_team"); 
	
	$team_leader_role->add_cap("view_team"); 
	$team_leader_role->add_cap("manage_team"); 
	
	$hostess_role->add_cap("view_teams"); 
	$hostess_role->add_cap("manage_teams"); 
	
	$juror_role->add_cap("view_teams"); // View all teams
	$juror_role->add_cap("can_grade"); // Can give out marks
	
	
	
}
