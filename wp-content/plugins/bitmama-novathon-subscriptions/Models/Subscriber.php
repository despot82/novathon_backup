<?php

namespace WordPress\ORM;

class Subscriber
{
    public $id;
    public $name;
    public $last_name;
    public $email;
    public $created_date;
    public $challenge;
    public $mobilephone;
    public $role;
    public $wordpress_user_id;
    public $in_waitlist;
    public $confirmed;
    public $last_update_date;
    public $confirm_date;
    public $profile_image_url;
    public $reset_password_guid;
    public $verify_guid;
    public $wordpressUser;
    public $profile_completed;
    public $profession;
    public $email_verified;
    public $companyname;
    
    public function save()
    {
        global $wpdb;
        
        $query = $wpdb->prepare("
        INSERT INTO `subscriptions`(`name`, `last_name`, `email`, `wordpress_user_id`, `created_date`, `challenge`, `role`,
        profile_image_url, in_waitlist, confirmed, confirm_date, verify_guid, email_verified)
        VALUES (%s,%s,%s,%d,%s,%s,%s,%s,%d,%d,%d,%s,%s)
        ", $this->name, $this->last_name, $this->email, $this->wordpress_user_id, $this->created_date, $this->challenge,
        $this->role, $this->profile_image_url, $this->in_waitlist, $this->confirmed, $this->confirm_date,$this->verify_guid,
        $this->email_verified);
        
        //echo $query;
        
        $wpdb->query($query);
    }
    
    public function total_count()
    {
        global $wpdb;
        
        return $wpdb->get_var("SELECT COUNT(*) FROM subscriptions");
    }
    
    public function confirmed_total_count()
    {
        global $wpdb;
        
        return $wpdb->get_var('SELECT COUNT(*) FROM subscriptions WHERE confirmed = 1');
    }
    
    public function waitlist_total_count()
    {
        global $wpdb;
        
        return $wpdb->get_var('SELECT COUNT(*) FROM subscriptions where in_waitlist = 1');
    }
    
    public function get_user_by_wordpress_user_id($userid)
    {
        global $wpdb;
        
        return $wpdb->get_row($wpdb->prepare("SELECT * FROM subscriptions WHERE wordpress_user_id = %d", $userid));
    }
    
    public function get_user_by_id($userid)
    {
        global $wpdb;
        
        return $wpdb->get_row($wpdb->prepare("SELECT * FROM subscriptions WHERE id = %d", $userid));
    }
    
    public function set_reset_password_guid($user)
    {
        global $wpdb;
        
        $guid = uniqid();
        $wpdb->query($wpdb->prepare("UPDATE subscriptions SET reset_password_guid = %s WHERE id = %d", $guid, $user->id));
        
        return $guid;
    }
    
    public function get_user_by_reset_guid($guid)
    {
        global $wpdb;
        
        return $wpdb->get_row($wpdb->prepare("SELECT * FROM subscriptions WHERE reset_password_guid = %s", $guid));
    }
    
    public function get_user_by_verify_guid($guid)
    {
        global $wpdb;
        
        return $wpdb->get_row($wpdb->prepare("SELECT * FROM subscriptions WHERE verify_guid = %s", $guid));
    }
    
    public function set_email_verified($email, $guid)
    {
        global $wpdb;
        
        $query = $wpdb->prepare("UPDATE subscriptions SET email_verified = 1 WHERE email = %s AND verify_guid = %s", $email, $guid);
        
        $wpdb->query($query);
    }
    
    public function update_profile($user)
    {
        global $wpdb;
        
        $query = $wpdb->prepare("UPDATE subscriptions SET role = %s, challenge = %s, mobilephone = %s, birthday = %s, profile_completed = 1,
        profession = %s, last_update_date = UNIX_TIMESTAMP(),
        name = %s, last_name = %s, email = %s, companyname = %s
        WHERE id = %d",
        $user->role, $user->challenge, $user->mobilephone, $user->birthday, $user->profession,
        $user->name, $user->last_name, $user->email, $user->companyname, $user->id);
        
        //echo $query;
        $wpdb->query($query);
    }
    
    public function delete_profile($user)
    {
        global $wpdb;
        
        $query = $wpdb->prepare("DELETE FROM subscriptions WHERE id = %s", $user->id);
        
        $wpdb->query($query);
    }
    
    public function admin_search($inwaitlist, $orderbyField, $orderByVersus, $currentPage, $textSearch, $status, $paginate)
    {
        $count_per_page = 10;
        $next_offset = ($currentPage-1) * $count_per_page;
        
        global $wpdb;
        
        $query = "SELECT * FROM subscriptions ";
        
        if ($inwaitlist)
            $query .= " WHERE in_waitlist = 1 ";
        else
            $query .= " WHERE in_waitlist = 0 ";
        
        if ($textSearch != "")
        {
            $query .= " AND (name like %s or last_name like %s or email like %s) ";
        }
        
        if ($status == "confirmed")
        {
            $query .= " AND confirmed = 1";
        }
        
        if ($status == "registered")
        {
            $query .= " AND confirmed = 0";
        }
        
        $total_query = $query;
        
        $total_query = $wpdb->prepare($total_query, '%' . $textSearch . '%', '%' . $textSearch . '%', '%' . $textSearch . '%');
        
        //echo $total_query;
        
        $query .= " ORDER BY created_date DESC, " . $orderbyField . " " . $orderByVersus;
        
        /*if ($paginate)
        {
        $query .= " LIMIT " . $count_per_page . " OFFSET " . $next_offset;
        }*/
        
        //echo $query;
        
        $query = $wpdb->prepare($query,  '%' . $textSearch . '%', '%' . $textSearch . '%', '%' . $textSearch . '%');
        
        //echo $query;
        
        $result = array(
        "total" => $wpdb->get_results($query, ARRAY_A ),
        "total_rows" => $wpdb->get_results($total_query),
        "results" => $wpdb->get_results($query)
        );
        
        return $result;
    }
    
    public function confirm_user($userid)
    {
        global $wpdb;
        
        //echo $userid;
        
        //echo $wpdb->prepare("UPDATE subscriptions SET confirmed = 1 WHERE id = %d", $userid);
        
        $wpdb->query($wpdb->prepare("UPDATE subscriptions SET confirmed = 1 WHERE id = %d", $userid));
    }
    
    public function insert_complete_user($user)
    {
        global $wpdb;
        
        $query = $wpdb->prepare("
        INSERT INTO `subscriptions`(`name`, `last_name`, `email`, `wordpress_user_id`, `created_date`, `challenge`, `role`,
        profile_image_url, in_waitlist, confirmed, confirm_date, verify_guid, email_verified, profession, mobilephone, birthday,companyname)
        VALUES (%s,%s,%s,%d,%s,%s,%s,%s,%d,%d,%d,%s,%s,%s,%s,%s,%s)
        ", $user->name, $user->last_name, $user->email, $user->wordpress_user_id, $user->created_date, $user->challenge,
        $user->role, $user->profile_image_url, $user->in_waitlist, $user->confirmed, $user->confirm_date,$user->verify_guid,
        $user->email_verified, $user->profession, $user->mobilephone, $user->birthday, $user->compayname);
        
        //echo $query;
        
        $wpdb->query($query);
    }
    
    public function add_to_participant($userid)
    {
        global $wpdb;
        $wpdb->query($wpdb->prepare("UPDATE subscriptions SET in_waitlist = 0 WHERE id = %d", $userid));
    }
}

?>