
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <title>Novathon - Registration cancelled</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,300italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <!-- Prevents iOS from auto detecting and styling phone numbers. Remember to link tel numbers and style yourself. -->
    <meta name="format-detection" content="telephone=no">
    <style type="text/css">
        body {
            width: 100%;
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            margin: 0;
            padding: 0;
        }

        img {
            border: 0;
            outline: none;
            -ms-interpolation-mode: bicubic;
            max-width: 640px;
        }

        a img {
            border: 0;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        table {
            font-size: 1px;
            mso-line-height-alt: 0;
            line-height: 0;
            mso-margin-top-alt: 1px;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            border-collapse: collapse;
        }

        @media only screen and (max-width: 480px) {
            table[class=device-width],
            td[class=device-width],
            img[class=device-width] {
                width: 320px !important;
                height: auto !important;
            }
            .mobPad {
              padding: 10px
            }
            .img-block {
              display: block;
              margin: 0 auto;
            }
            .logoMob {
              padding-top: 20px;
            }
            .logoMob img {
              width: 150px;
            }
        }

        @media only screen and (max-width: 480px) {
            table[class=inner-width],
            td[class=inner-width],
            img[class=inner-width] {
                width: 240px !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 480px) {
            td[class=hero-1] {
                width: 320px !important;
                height: auto !important;
                background-position: center !important;
                background-size: cover !important;
                padding-top: 60px !important;
                padding-bottom: 60px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            td[class=hero-2] {
                width: 320px !important;
                height: auto !important;
                background-position: center !important;
                background-size: cover !important;
            }
        }

        @media only screen and (max-width: 480px) {
            td[class=hero-3] {
                width: 320px !important;
                height: auto !important;
                padding-top: 60px !important;
                padding-bottom: 60px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            td[class=hero-feature] {
                padding-bottom: 30px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            td[class=stack] {
                display: block !important;
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
        }

        @media only screen and (max-width: 480px) {
            td[class=small-push] {
                padding-top: 30px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            td[class=wrapper] {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
        }

        @media only screen and (max-width: 480px) {
            td[class=logo] {
                width: 240px !important;
                height: auto !important;
                text-align: center !important;
                padding-top: 20px !important;
            }
            td[class=logo] img {
              width: 30%;
              height: auto;
            }
        }

        @media only screen and (max-width: 480px) {
            td[class=logo] img {
                display: inline !important;
            }
        }

        @media only screen and (max-width: 480px) {
            td[class=center] {
                width: 240px !important;
                height: auto !important;
                text-align: center !important;
            }
        }
    </style>
    <!--[if lte lte ]>
        <style type="text/css">
            td[class=logo] {
                padding-bottom: 40px;
            }
        </style>
    <![endif]-->
</head>

<body style="width: 100%; -webkit-text-size-adjust: 100% !important; -ms-text-size-adjust: 100% !important; margin: 0; padding: 0;">

    <!--[if lte lte ]>
    	<xml>
         <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
        </xml>
    <![endif]-->
    <!-- /\/\/\/\/\/\/\/\/ START Inbox Preview Text /\/\/\/\/\/\/\/\/ -->
    <div style="width: 0px; height: 0px; overflow: hidden; position: absolute; min-height: 0px; line-height: 0px; font-size: 0px; color: #ffffff;">
        Registration cancelled
    </div>
    <!-- /\/\/\/\/\/\/\/\/ END Inbox Preview Text /\/\/\/\/\/\/\/\/ -->
    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; table-layout: fixed;">
        <tr>
            <td>
                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                    <tr>
                        <!--
                For dark background, switch to this <td>:
                <td align="center" width="100%" style="background-color: #292c34;">
                -->
                        <td align="center" width="100%" style="background-color: #eaeaea;">
                            <table class="device-width" align="center" width="680" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                                <tr>
                                    <td class="wrapper" width="640" style="padding-left: 20px; padding-right: 20px; padding-top: 45px; padding-bottom: 45px;">
                                        <!-- /\/\/\/\/\/\/\/\/ START Header A /\/\/\/\/\/\/\/\/ -->
                                        <table class="device-width" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                                            <tr>
                                                <td class="device-width iefix" width="640" style="background-color: #b1c5cc;">
                                                    <table class="inner-width" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                                                        <tr>
                                                            <td class="stack">
                                                                <table class="device-width" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: center;" align="center">
                                                                    <tr>
                                                                        <td class="logo" style="text-align: center;">
                                                                            <!-- Logo link back to your website -->
                                                                            <a href="http://novathon.net">
                                                                                <!-- Remember to add styles to all <img> in order to style alt text -->
                                                                                <img alt="novathon logo" src="{{AssetsUrl}}/images/logo.png" width="150" height="207" style="display: block; border: 0 none; outline: none; color: #292c34; font-size: 30px;margin-top:30px; margin-left: auto; margin-right: auto;">
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="stack">
                                                                <table class="device-width" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; text-align: center;">
                                                                    <tr>
                                                                        <td class="" style="font-family: 'Open Sans', Arial; font-size: 24px; line-height: 25px; color: #ffffff; text-align: center; font-weight: lighter; padding-top: 25px; padding-bottom: 25px;">
                                                                            THINK, INNOVATE AND CREATE<br>
                                                                            <small>23-24 September 2017 - Zagreb</small>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- /\/\/\/\/\/\/\/\/ START Hero A /\/\/\/\/\/\/\/\/ -->
                                        <table class="device-width" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                                            <tr>
                                                <td class="hero-1" style="background-color: #ffffff; padding-top: 40px; padding-bottom: 40px;" width="640">
                                                    <div>
                                                        <table class="device-width" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                                                            <tr>
                                                                <td class="device-width" valign="middle" width="640">
                                                                    <table class="device-width" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;" align="center">
                                                                        <tr>
                                                                            <!-- Hero headline goes here. Manually add <br /> to insert line breaks to fine tune. -->
                                                                            <td style="text-align: center; padding-bottom: 20px;">
                                                                                <img src="{{AssetsUrl}}/images/demImg/mark-cancelled.jpg" width="110" height="76" alt="alert-cancel.png">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="device-width" valign="middle" width="640">
                                                                    <table class="device-width" width="320" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;" align="center">
                                                                        <tr>
                                                                            <!-- Hero headline goes here. Manually add <br /> to insert line breaks to fine tune. -->
                                                                            <td style="font-family: 'Open Sans', Arial; font-size: 23px; line-height: 25px; color: #303030; text-align: center;">
                                                                              <span style="font-family: 'Open Sans', Arial; color: #303030; font-weight: 700;">Hello {{Name}},</span><br><br>
                                                                                Your registration is cancelled
                                                                                <div class="mobPad" style="font-family: 'Open Sans', Arial; font-size: 16px; line-height: 24px; color: #303030; text-align: center; padding-top: 20px; font-weight: lighter;">Please note that the QR code you received previously by email will not be valid for entering the event.<br>
                                                                                If you change your mind and would like to participate at Novathon #withPBZ, please register again.<br><br>
                                                                                <span style="font-size: 13px; font-style: italic;">Best regards,<br>
                                                                                Privredna Banka Zagreb</span></div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- /\/\/\/\/\/\/\/\/ END Hero A /\/\/\/\/\/\/\/\/ -->
                                        <!-- /\/\/\/\/\/\/\/\/ START Color Accent Bar /\/\/\/\/\/\/\/\/ -->
                                        <table class="device-width" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                                            <tr>
                                                <td width="640" height="2" style="background-color: #cdcdcd; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                            </tr>
                                        </table>
                                        <!-- /\/\/\/\/\/\/\/\/ END Color Accent Bar /\/\/\/\/\/\/\/\/ -->
                                        <!-- /\/\/\/\/\/\/\/\/ START Hero A /\/\/\/\/\/\/\/\/ -->
                                        <table class="device-width" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;">
                                            <tr>
                                                <td style="background-color: #474747; padding-bottom: 20px;" width="640">
                                                    <table class="device-width" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;" align="center">
                                                        <tr>
                                                            <td class="device-width" valign="middle" width="640">
                                                                <table class="device-width" width="640" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;" align="center">
                                                                    <tr>
                                                                        <!-- Hero headline goes here. Manually add <br /> to insert line breaks to fine tune. -->
                                                                        <td style="font-family: 'Open Sans', Arial; font-size: 24px; line-height: 25px; color: #303030; text-align: center; padding-top: 20px;">
                                                                            <img width="150" height="76" class="img-block" src="{{AssetsUrl}}/images/demImg/innovation-center-logo-footer.jpg" alt="innovation center logo" width="200">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #474747; padding-bottom: 20px;" width="640">
                                                    <table class="device-width" width="320" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;" align="left">
                                                        <tr>
                                                            <td class="device-width" valign="middle" width="320">
                                                                <table class="device-width" width="320" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;" align="center">
                                                                    <tr>
                                                                        <!-- Hero headline goes here. Manually add <br /> to insert line breaks to fine tune. -->
                                                                        <td class="logoMob"  style="font-family: 'Open Sans', Arial; font-size: 24px; line-height: 25px; color: #303030; text-align: center;">
                                                                            <img class="img-block" src="{{AssetsUrl}}/images/demImg/pbz.jpg" alt="pbz logo" width="200">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table class="device-width" width="320" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;" align="right">
                                                        <tr>
                                                            <td class="device-width" valign="middle" width="320">
                                                                <table class="device-width" width="320" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse;" align="center">
                                                                    <tr>
                                                                        <!-- Hero headline goes here. Manually add <br /> to insert line breaks to fine tune. -->
                                                                        <td class="logoMob" style="font-family: 'Open Sans', Arial; font-size: 24px; line-height: 25px; color: #303030; text-align: center;">
                                                                            <img class="img-block" src="{{AssetsUrl}}/images/demImg/intesa-sanpaolo-logo.jpg" alt="sanpaolo logo" width="200">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- end module -->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
