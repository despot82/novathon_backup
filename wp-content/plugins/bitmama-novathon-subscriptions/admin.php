<?php

if (!function_exists('add_action')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

add_action('admin_enqueue_scripts', 'bit_novathon_subscriptions_admin_add_files');
add_action( 'wp_ajax_bit_novathon_subscriptions_admin_search', 'bit_novathon_subscriptions_admin_search' );
add_action( 'wp_ajax_bit_novathon_subscriptions_admin_confirm_users', 'bit_novathon_subscriptions_admin_confirm_users' );
add_action( 'wp_ajax_bit_novathon_subscriptions_admin_import_csv', 'bit_novathon_subscriptions_admin_import_csv' );
add_action('admin_init', 'bit_novathon_subscriptions_admin_init', 100);
add_action( 'wp_ajax_bit_novathon_subscriptions_admin_delete_users', 'bit_novathon_subscriptions_admin_delete_users' );
add_action( 'wp_ajax_bit_novathon_subscriptions_admin_send_mail', 'bit_novathon_subscriptions_admin_send_mail' );
add_action( 'wp_ajax_bit_novathon_subscriptions_admin_edit_profile', 'bit_novathon_subscriptions_admin_edit_profile' );
add_action( 'wp_ajax_bit_novathon_subscriptions_admin_edit_profile_dialog', 'bit_novathon_subscriptions_admin_edit_profile_dialog' );
add_action( 'wp_ajax_bit_novathon_subscriptions_admin_add_to_participant', 'bit_novathon_subscriptions_admin_add_to_participant' );

$sRole = ['Developer', 'Designer', 'Marketer'];
$sProfession = ['Student' , 'Self Employed' , 'Employed', 'Other'];
$sChallenge = ['Smart Payments', 'Digital Wealth Management', 'Smart Branch' ,'Bridge Solutions' , 'Outside-in banking', 'Big Financial Data'];

function bit_novathon_subscriptions_admin_init()
{
    if (!wp_doing_ajax())
    {
        $redirect = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : home_url( '/' );
        global $current_user, $wordpress_subscriber_role;
        $user_roles = $current_user->roles;
        $user_role = array_shift($user_roles);
        if($user_role === $wordpress_subscriber_role) {
            exit( wp_redirect( $redirect ) );
        }
    }
}

function bit_novathon_subscriptions_admin_add_menu()
{
    add_dashboard_page( 'Novathon Subscriptions', 'Novathon Subscriptions', 'can_manage_novathon_subscription', 'novathon-subscriptions/admin.php', 'bit_novathon_subscriptions_admin');

    add_submenu_page(
    null            // -> Set to null - will hide menu link
    , 'Export'    // -> Page Title
    , 'Export'    // -> Title that would otherwise appear in the menu
    , 'can_manage_novathon_subscription' // -> Capability level
    , 'novathon-subscribers-export'   // -> Still accessible via admin.php?page=menu_handle
    , 'bit_novathon_subscriptions_admin_export' // -> To render the page
    );
}

if (isset($_GET['page']) && $_GET['page'] === 'novathon-subscribers-export') {
    bit_novathon_subscriptions_admin_export();
}

function bit_novathon_subscriptions_admin_add_files()
{
    wp_enqueue_script( 'SubscriptionsManagerAdminPanelAjaxForm', get_template_directory_uri(). '/admin/jquery.ajax.form.js', array( 'jquery' ) );
    wp_enqueue_script( 'SubscriptionsManagerAdminPanelValidator', get_template_directory_uri(). '/admin/jquery.validate.js', array( 'jquery' ) );
    wp_enqueue_script( 'SubscriptionsManagerAdminPanelValidatorAdditionalMethods', get_template_directory_uri(). '/admin/additional-methods.js', array( 'jquery', 'SubscriptionsManagerAdminPanelValidator' ) );
    wp_enqueue_script( 'SubscriptionsManagerAdminPanel', get_template_directory_uri(). '/admin/SubscriptionsManagerAdminPanel.js', array( 'jquery' ) );

}

function bit_novathon_subscriptions_admin()
{
    global $total_max_subscribers_before_whitelist, $wordpress_subscriber_role_manager, $wordpress_subscriber_role;

    add_role( $wordpress_subscriber_role, 'Novathon Subscriber', array( 'read' => true ) );
    add_role( $wordpress_subscriber_role_manager, 'Novathon Subscribers Manager', array( 'read' => true, "can_manage_novathon_subscription" => true ) );

    $sub = new Wordpress\ORM\Subscriber();
    $total = $sub->total_count();
    $waitlist = $sub->waitlist_total_count();
    $confirmed = $sub->confirmed_total_count();

    $available = $total_max_subscribers_before_whitelist-$total;

    if ($available < 0)
    {
        $available = 0;
    }

    //$r = add_role( $wordpress_subscriber_role_manager, 'Novathon Subscribers Manager', array( 'read' => true, "can_manage_novathon_subscription" => true ) );
    //var_dump($r);

    ?>


  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,300italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="/wp-content/themes/novathon/css/admin.min.css" type="text/css">
  <input type="hidden" id="orderBy" name="orderBy" value="name" />
  <input type="hidden" id="orderByVersus" name="orderByVersus" value="DESC" />
  <input type="hidden" name="currentPage" id="currentPage" value="1" />

  <div id="importDialog" style="display:none" class="dialogBox">
    <form name="import" method="post" action="<?php echo  admin_url( 'admin-ajax.php' ) ?>" enctype="multipart/form-data">
      <input type="hidden" name="action" value="bit_novathon_subscriptions_admin_import_csv" />
      <center>
        <h3>Import csv</h3>
        <input type="file" name="csv" id="csv" />
        <br/>
        <br/>
        <button class="buttonDialog--confirm" name="importCsv" data-action="import-csv-action">Import</button>
      </center>
    </form>
  </div>

  <div id="sendMailDialog" style="display:none" class="dialogBox">
    <form name="import">
      <input type="hidden" name="action" value="bit_novathon_subscriptions_admin_send_mail" />
      <input type="hidden" name="send-mail-userid" value="-1" />
      <center>
        <h3>Send email</h3>
        <div class="inputForm--select">
          <select name="mailTemplate" id="mailTemplate">
            <option value="verifyemail">Verify Email</option>
            <option value="ticket">Ticket</option>
            <option value="cancelregistration">Cancel registration</option>
            <option value="cancelregistration_admin">Cancel registration (Admin)</option>
            <option value="confirmpresence">Confirm presence</option>
          </select>
        </div>
        <br/>
        <br/>
        <button class="buttonDialog--confirm" type="button" name="sendMailAction" data-action="send-mail-action">Send</button>
      </center>
    </form>
  </div>
  
  <div id="sendNewsletterMailDialog" style="display:none" class="dialogBox">
    <form name="newsletter">
      <input type="hidden" name="action" value="bit_novathon_subscriptions_admin_send_newsletter" />
      <input type="hidden" name="send-mail-userid" value="-1" />
      <center>
        <h3>Send newsletter</h3>
        <div class="inputForm--select">
          <select name="newsletterTemplate" id="newsletterTemplate">
          
            <option value="newsletter1">Newsletter 1</option>
            <option value="newsletter2">Newsletter 2</option>
            <option value="newsletter3">Newsletter 3</option>
            <option value="newsletter4">Newsletter 4</option>
            
          </select>
        </div>
        <br/>
        <br/>
        <button class="buttonDialog--confirm" type="button" name="sendNewsletterAction" data-action="send-newsletter-action">Send</button>
      </center>
    </form>
  </div>

  <div class="adminPanel">
    <header class="adminPanel__header">
      <div class="adminPanel__header__nav">
        <h1>NOVATHON EVENT  | <span>23 - 24 September 2017</span></h1>
        <div class="adminPanel__header__nav--link">
          <a href="#">partecipants</a>
          <a href="#">groups</a>
        </div>
      </div>
    </header>
    <div class="adminPanel__infoPanel">
      <div class="wrap">
        <p>total ticket: <span>180</span></p>
        <div class="adminPanel__infoPanel__panel">
          <div class="adminPanel__infoPanel__panel--infoSingle">
            <span class="count"><?php echo $total ?></span>
            <span class="desc">registered</span>
          </div>
          <div class="adminPanel__infoPanel__panel--infoSingle">
            <span class="count"><?php echo $available ?></span>
            <span class="desc">available</span>
          </div>
          <div class="adminPanel__infoPanel__panel--infoSingle">
            <span class="count"><?php echo $waitlist ?></span>
            <span class="desc">waiting list</span>
          </div>
          <div class="adminPanel__infoPanel__panel--infoSingle">
            <span class="count"> <?php echo $confirmed ?></span>
            <span class="desc">confirmed</span>
          </div>
        </div>
      </div>
    </div>
    <div class="adminPanel__tab">
      <div class="wrap">
        <div class="adminPanel__tab__nav">
          <div class="adminPanel__tab__nav--control">
            <a data-action="filter-active" class="active" href="javascript:;">Participants</a>
            <a data-action="filter-waitlist" href="javascript:;">Waiting list</a>
          </div>
          <div class="adminPanel__tab__nav--action">
            <a data-action="export-action" href="javascript:;">export csv</a>
            <a data-action="import" href="#">import csv</a>
            <a data-action="add-participiant-dialog" id="add-partecipant" href="#">Add participant</a>
          </div>
        </div>
      </div>
      <div class="adminPanel__tab__filter">
        <div class="wrap">
          <div class="adminPanel__tab__filter--selectAll">
            <input class="inputCheck" type="checkbox" data-action="subscriptions-select-all" />
            <label for="selAll">Select all</label>
          </div>
          <div class="adminPanel__tab__search">
            <div class="adminPanel__tab__search--searchBar">
              <input type="text" name="textSearch" value="" placeholder="Search a participant">
            </div>
            <div class="adminPanel__tab__search--filterSelect">
              <select name="filterSearch">
                <option value="">Filter</option>
                <option value="">All</option>
                <option value="registered">Registered</option>
                <option value="confirmed">Confirmed</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="adminPanel__table">
        <div class="wrap">
          <div id="subscriptions-admin-results"></div>
        </div>
      </div>
    </div>
    <div class="adminPanel_banner" style="display:none">
      <div class="wrap">
        <span class="totSelected"><a href="javascript:;" name="mobile-toggle"><span class="icon-uni32"></span></a><strong>Selected: <strong data-content="total-selected"></strong></strong>
        </span>
        <button class="adminButton--confirm" type="button" data-action="massive-confirm" name="confirm">Confirm</button>
        <button class="adminButton--confirm" type="button" data-action="massive-add-to-participant" style="display:none" name="add-partecipant">Add to partecipant</button>
        <button class="adminButton" type="button" data-action="massive-send-mail-newsletter" name="send-email-newsletter">Send Newsletter</button>
        <button class="adminButton" type="button" data-action="massive-send-mail" name="send-email">Send Email</button>
        <button class="adminButton" type="button" data-action="massive-delete" name="delete">Delete</button>
      </div>
    </div>
    <!-- <div id="dialog" class="dialogBox">
    <div class="addPartecipant" data-target="add-partecipant">
    <h3>Add a new participant</h3>
    <form class="dialogBox__form" method="post">
    <div class="inputForm">
    <label for="name">Name</label>
    <input type="text" name="name" value="">
    </div>
    <div class="inputForm">
    <label for="Surname">Surname</label>
    <input type="text" name="name" value="">
    </div>
    <div class="inputForm">
    <label for="Email">Email</label>
    <input type="email" name="email" value="">
    </div>
    <div class="inputForm">
    <label for="phone">MobilePhone</label>
    <input type="email" name="phone" value="">
    </div>
    <div class="inputForm--select">
    <label>Date of birth</label>
    <select name="day">
    <option value="">dd</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    </select>
    <select name="month">
    <option value="">mm</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    </select>
    <select name="year">
    <option value="">yyyy</option>
    <option value="2017">2017</option>
    <option value="2016">2016</option>
    </select>
    </div>
    <div class="inputForm--select">
    <label>Challenges</label>
    <select name="challenges" class="required">
    <option value="">Select challenges</option>
    <option name="Developer">Developer</option>
    <option name="Designer">Designer</option>
    <option name="Marketer">Marketer</option>
    </select>
    </div>
    <div class="inputForm--select">
    <label>Role</label>
    <select name="role" class="required">
    <option value="">Select challenges</option>
    <option name="Developer">Developer</option>
    <option name="Designer">Designer</option>
    <option name="Marketer">Marketer</option>
    <option name="Other">Other</option>
    </select>
    <input placeholder="Please specify" class="required" type="text" name="role_other" value="">
    </div>
    <div class="inputForm--select">
    <label>Profession</label>
    <select name="profession" class="required">
    <option value="">Select challenges</option>
    <option name="Developer">Developer</option>
    <option name="Designer">Designer</option>
    <option name="Marketer">Marketer</option>
    <option name="Other">Other</option>
    </select>
    <input placeholder="Please specify" class="required" type="text" name="profession_other" value="">
    </div>
    </form>
    <div class="buttonWrap">
    <button class="cancel" type="button" name="cancel" data-action="close-dialog">Cancel</button>
    <button class="add" type="button" name="addPartecipant">Add participant</button>
    </div>
    </div>
    <div class="editPartecipant" data-target="edit">
    <h3>Edit participant</h3>
    <form class="dialogBox__form" method="post">
    <div class="inputForm">
    <label for="name">Name</label>
    <input type="text" name="name" value="">
    </div>
    <div class="inputForm">
    <label for="Surname">Surname</label>
    <input type="text" name="name" value="">
    </div>
    <div class="inputForm">
    <label for="Email">Email</label>
    <input type="email" name="email" value="">
    </div>
    <div class="inputForm">
    <label for="phone">MobilePhone</label>
    <input type="email" name="phone" value="">
    </div>
    <div class="inputForm--select">
    <label>Date of birth</label>
    <select name="day">
    <option value="">dd</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    </select>
    <select name="month">
    <option value="">mm</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    </select>
    <select name="year">
    <option value="">yyyy</option>
    <option value="2017">2017</option>
    <option value="2016">2016</option>
    </select>
    </div>
    <div class="inputForm--select">
    <label>Challenges</label>
    <select name="challenges" class="required">
    <option value="">Select challenges</option>
    <option name="Developer">Developer</option>
    <option name="Designer">Designer</option>
    <option name="Marketer">Marketer</option>
    </select>
    </div>
    <div class="inputForm--select">
    <label>Role</label>
    <select name="role" class="required">
    <option value="">Select challenges</option>
    <option name="Developer">Developer</option>
    <option name="Designer">Designer</option>
    <option name="Marketer">Marketer</option>
    <option name="Other">Other</option>
    </select>
    <input placeholder="Please specify" class="required" type="text" name="role_other" value="">
    </div>
    <div class="inputForm--select">
    <label>Profession</label>
    <select name="profession" class="required">
    <option value="">Select challenges</option>
    <option name="Developer">Developer</option>
    <option name="Designer">Designer</option>
    <option name="Marketer">Marketer</option>
    <option name="Other">Other</option>
    </select>
    <input placeholder="Please specify" class="required" type="text" name="profession_other" value="">
    </div>
    </form>
    <div class="buttonWrap">
    <button class="cancel" type="button" name="cancel" data-action="close-dialog">Cancel</button>
    <button class="add" type="button" name="addPartecipant">Add participant</button>
    </div>
    </div>
    <div class="editPartecipant" data-target="delete">
    <h3>Are you sure you want to delete this partecipant?</h3>
    <div class="buttonWrap">
    <button type="button" data-action="close-dialog" name="cancel">Cancel</button>
    <button type="button" name="delete">Delete</button>
    </div>
    </div>
    <div class="addSuccessful" data-target="add-partecipant-waiting">
    <img src="" alt="">
    <h3>New participant added successfully to <strong>Participants</strong></h3>
    <div class="buttonWrap">
    <button type="button" name="done">done</button>
    </div>
    </div>
    </div>-->
  </div>

  <script>
    jQuery(document).ready(function(_$) {
      new SubscriptionsManagerAdminPanel();
    });

    jQuery('[name="mobile-toggle"]').on('click', function() {
      jQuery('.adminPanel_banner').toggleClass('open');
    });
  </script>
  <?php
}

function bit_novathon_subscriptions_admin_search()
{
    $sub = new Wordpress\ORM\Subscriber();
    $textSearch = $_POST["textSearch"];
    $currentPage = $_POST["currentPage"];
    $orderBy = $_POST["orderBy"];
    $orderByVersus = $_POST["orderByVersus"];
    $waitlist = $_POST["waitlist"] == 1 ? true : false;
    $status = $_POST["status"];

    $result = $sub->admin_search($waitlist, $orderBy, $orderByVersus, $currentPage, $textSearch, $status, true);

    $total = $result["total"];
    $list = $result["results"];


    ?>
    <div class="adminPanel__table__pagination">
        <span> <?php echo count($total) ?> total</span>
    </div>
    <div class="adminPanel__table__body">
      <table>
        <thead>
          <tr>
            <th>&nbsp;</th>
            <th>Name</th>
            <th>Surname</th>
            <th>Email</th>
            <th>Challenges</th>
            <th>Role</th>
            <th>Profession</th>
            <th class="numeric">Mobile</th>
          </tr>
        </thead>
        <tbody>
          <?php

    foreach ($list as $key => $row)
    {
        ?>

            <tr>
              <td class="first">
                <input class="inputCheck" type="checkbox" name="subscriptions-select-user-check" data-action="subscriptions-select-user" data-user-id="<?php echo $row->id ?>" />
                <?php
        if ($row->confirmed) { ?>
                  <span data-title="Confirmed" class="bulletConfirm"></span>
                  <?php } else { ?>
                    <span data-title="Not Confirmed" class="bulletNotConfirm"></span>
                    <?php } ?>
              </td>
              <td data-title="Name">
                <?php echo $row->name ?>
              </td>
              <td data-title="Surname">
                <?php echo $row->last_name ?>
              </td>
              <td data-title="Email">
                <?php echo $row->email ?>
              </td>
              <td data-title="Challenge">
                <?php echo $row->challenge ?>
              </td>
              <td data-title="Role">
                <?php echo $row->role ?>
              </td>
              <td data-title="Profession">
                <?php echo $row->profession ?>
              </td>
              <td data-title="Mobilephone">
                <?php echo $row->mobilephone ?>
              </td>
              <td>
                <a href="#" name="action-menu" data-target="<?php echo $row->id ?>">
                  <img src="/wp-content/themes/novathon/images/adminPage/menu-ico.png" alt=""></a>
                <div name="dialog-menu" class="menu-single" data-rel="<?php echo $row->id ?>">
                  <a href="#_" data-action="close-box">close</a>

                  <?php if ($row->confirmed == 0) { ?>
                    <a data-action="single-user-confirm" id="confirm" data-target="<?php echo $row->id ?>" href="#"><span class="icon-verification-mark"></span>Confirm</a>
                    <?php } ?>
                      <a data-action="single-user-edit" id="edit" data-target="<?php echo $row->id ?>" href="#"><span class="icon-funding"></span>Edit</a>
                      <a data-action="single-user-send-mail" id="sendMail" data-target="<?php echo $row->id ?>" href="#"><span class="icon-mail"></span>Send email</a>
                      <a data-action="single-user-delete" id="delete" data-target="<?php echo $row->id ?>" href="#"><span class="icon-uni22"></span>Delete</a>
                </div>
              </td>
            </tr>
            <?php
    }

    ?>

        </tbody>
      </table>
    </div>
    <script>
      jQuery(document).ready(function(_$) {
        /* sandro */
        _$('#dialog').dialog({
          autoOpen: false,
          modal: true,
          width: '100%',
          fluid: true,
        });

        _$('[data-action="open-dialog"]').on('click', function(e) {
          e.preventDefault();
          var type = this.id;
          _$('#dialog').dialog('open');
          _$('#dialog [data-target]').hide();
          _$('[data-target="' + type + '"]').show();
        });

        _$('[data-action="close-box"]').click(function(e) {
          _$(this).parent().hide();
        });

        _$('[name="action-menu"]').on('click', function(e) {
          e.preventDefault();
          var target = _$(this).attr('data-target');
          _$('[data-rel="' + target + '"]').fadeIn(300);
        });

      });
    </script>
    <?php

    wp_die();
}

function bit_novathon_subscriptions_admin_confirm_users()
{
    $usersArray = json_decode($_POST["users"]);

    //var_dump($usersArray);

    $sub = new Wordpress\ORM\Subscriber();

    foreach ($usersArray as $u)
    {
        $sub->confirm_user($u);
    }

    echo json_encode(array(
    "result" => true
    ));
    wp_die();
}

function bit_novathon_subscriptions_admin_import_csv()
{
    $imported = 0;
    $not_imported = 0;

    global $wordpress_subscriber_role, $total_max_subscribers_before_whitelist;

    $csv = $_FILES["csv"];

    if (!empty($_FILES))
    {
        $content = file_get_contents($_FILES['csv']['tmp_name']);
        $lines = explode(PHP_EOL, $content);
        //$rows = array_map('str_getcsv', $content, ';');

        //var_dump($content);

        $array = array();

        $counter = 1;

        foreach ($lines as $line) {

            if ($counter > 1)
            {
                $user_exists = false;

                $row = str_getcsv($line, ';');

                //print_r($row);

                if ($row[0] != "")
                {
                    $name = $row[0];
                    $lastname = $row[1];
                    $email = $row[2];
                    $confirmed = $row[3];
                    $waitlist = $row[4];
                    $challenge = $row[5];
                    $profession = $row[6];
                    $role = $row[7];
                    $mobilephone = $row[9];
                    $birthday = $row[8];
                    $companyname = $row[10];

                    /* $password = wp_generate_password( $length=8, $include_standard_special_chars=false );

                    $user_id = username_exists( $email );

                    if ( !$user_id and email_exists($email) == false ) {
                    $user_id = wp_create_user( $email, $password, $email );

                    wp_update_user( array( 'ID' => $user_id, 'role' => $wordpress_subscriber_role ) );

                    $sub = new Wordpress\ORM\Subscriber();

                    $total = $sub->total_count();

                    $sub->email_verified = 1;

                    if ($total > $total_max_subscribers_before_whitelist)
                    {
                    $sub->in_waitlist = 1;
                    $sub->confirmed = $confirmed;
                    }
                    else
                    {
                    $sub->in_waitlist = 0;
                    $sub->confirmed = $confirmed;
                    $sub->confirm_date = time();
                    }

                    $sub->name = $name;
                    $sub->last_name = $lastname;
                    $sub->email = $email;
                    $sub->created_date = time();
                    $sub->wordpress_user_id = $user_id;
                    $sub->profile_image_url = '';
                    $sub->verify_guid = uniqid();
                    $sub->profession = $profession;
                    $sub->role = $role;
                    $sub->challenge = $challenge;
                    $sub->mobilephone = $mobilephone;
                    $sub->birthday = $birthday;

                    $sub->insert_complete_user();

                    $imported++;
                    }
                    else
                    $not_imported++;*/

                    //$email, $confirmed, $name, $lastname, $profession, $role, $challenge, $birthday, $mobilephone
                    $saved = bit_novathon_subscriptions_admin_create_complete_profile($email, 0, $name, $lastname, $profession, $role, $challenge,
                    $birthday, $mobilephone, $companyname);

                    if ($saved)
                        $imported++;
                    else
                        $not_imported++;
                }
            }

            $counter++;
        }
    }

    echo json_encode(array(
    "imported" => $imported,
    "not_imported" => $not_imported
    ));

    wp_die();
}

function bit_novathon_subscriptions_admin_delete_users()
{
    $usersArray = json_decode($_POST["users"]);

    //var_dump($usersArray);

    $sub = new Wordpress\ORM\Subscriber();

    foreach ($usersArray as $u)
    {
        $novathonUser = $sub->get_user_by_id($u);
        wp_delete_user($novathonUser->wordpress_user_id);
        $sub->delete_profile($novathonUser);
    }

    echo json_encode(array(
    "result" => true
    ));
    wp_die();
}

function bit_novathon_subscriptions_admin_send_mail()
{
	
    $usersArray = json_decode($_POST["users"]);
    $template = $_POST["template"];
    
    error_log("in bit_novathon_subscriptions_admin_send_mail, template is $template");
    
    //var_dump($usersArray);

    $sub = new Wordpress\ORM\Subscriber();

    $sent = 0;

    foreach ($usersArray as $u)
    {
        $novathonUser = $sub->get_user_by_id(intval($u));

        switch ($template)
        {
            case "verifyemail":
                bit_novathon_subscriptions_send_verification_email($novathonUser);
                break;
            case "confirmpresence":
                bit_novathon_subscriptions_send_confirm_presence_email($novathonUser);
                break;
            case "cancelregistration":
                bit_novathon_subscriptions_send_cancel_registration_email($novathonUser);
                break;
            case "cancelregistration_admin":
                bit_novathon_subscriptions_send_cancel_registration_email_admin($novathonUser);
                break;
            case "ticket":
                bit_novathon_subscriptions_send_ticket_email($novathonUser);
                break;
            case "newsletter1":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 1);
                break;
            case "newsletter2":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 2);
                break;
            case "newsletter3":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 3);
                break;
            case "newsletter4":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 4);
                break;
            case "newsletter5":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 5);
                break;
            case "newsletter6":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 6);
                break;
            case "newsletter7":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 7);
                break;
            case "newsletter8":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 8);
                break;
            case "newsletter9":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 9);
                break;
            case "newsletter10":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 10);
                break;
            case "newsletter11":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 11);
                break;
            case "newsletter12":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 12);
                break;
            case "newsletter13":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 13);
                break;
            case "newsletter14":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 14);
                break;
            case "newsletter15":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 15);
                break;
            case "newsletter16":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 16);
                break;
            case "newsletter17":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 17);
                break;
            case "newsletter18":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 18);
                break;
            case "newsletter19":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 19);
                break;
            case "newsletter20":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 20);
                break;
            case "newsletter21":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 21);
                break;
            case "newsletter22":
                bit_novathon_subscriptions_send_newsletter_email($novathonUser, 22);
                break;
               
    }

    $sent++;
}

echo json_encode(array(
"result" => true,
"sent" => $sent
));

wp_die();
}

function bit_novathon_subscriptions_admin_edit_profile_dialog()
{
    global $sChallenge, $sProfession, $sRole;

    $userid = intval($_POST["userid"]);
    $edit = false;

    if ($userid > -1)
        $edit = true;

    $sub = new Wordpress\ORM\Subscriber();

    if ($userid > -1)
    {
        //var_dump("edit");
        $novathonUser = $sub->get_user_by_id($userid);
        $arrayDate = explode("/",$novathonUser->birthday);

        $professionSelected = false;
        $roleSelected = false;

        if (in_array($novathonUser->profession, $sProfession))
            $professionSelected = true;

        //var_dump($professionSelected);

        if (in_array($novathonUser->role, $sRole))
            $roleSelected = true;

        $profession_value = $novathonUser->profession;
        $role_value = $novathonUser->role;

        if (!$professionSelected)
            $profession_value = "Other";

        if (!$roleSelected)
            $role_value = "Other";
    }

    if ($userid == -1)
    {
        $professionSelected = true;
        $roleSelected = true;
    }

    ?>

      <div id="edit-profile-dialog" class="dialogBox">
        <div class="addPartecipant" data-target="add-partecipant">
          <center>
            <h3>
    <?php if (!$edit) { ?>
        Add a new participant
    <?php } ?>
    <?php if ($edit) { ?>
        Edit participant
    <?php } ?>
    </h3>
          </center>
          <form class="dialogBox__form" method="post" name="edit-profile-form">
            <input type="hidden" name="userid" id="userid" value="<?php echo $userid ?>" />
            <input type="hidden" name="action" value="bit_novathon_subscriptions_admin_edit_profile" />
            <div class="inputForm">
              <label for="name">Name</label>
              <input type="text" class="required" name="name" value="<?php echo $novathonUser->name ?>" />
            </div>
            <div class="inputForm">
              <label for="Surname">Surname</label>
              <input type="text" name="lastname" class="required" value="<?php echo $novathonUser->last_name ?>">
            </div>
            <div class="inputForm">
              <label for="Email">Email</label>
              <input type="email" name="email" class="required email" value="<?php echo $novathonUser->email ?>">
            </div>
            <div class="inputForm">
              <label for="phone">MobilePhone</label>
              <input type="text" name="mobilephone" class="required startWith" value="<?php echo $novathonUser->mobilephone ?>">
            </div>
            <div class="inputForm--select">
              <label>Date of birth</label>
              <select class="required" name="birthdayday">
                <?php
    for ($i=1; $i <= 31; $i++) {
        if($i !== intval($arrayDate[0])){
            ?>
                  <option value="<?php echo $i ?>">
                    <?php echo $i ?>
                  </option>
                  <?php
        } else {
            ?>
                    <option value="<?php echo $i ?>" selected>
                      <?php echo $i ?>
                    </option>
                    <?php
        }
    }
    ?>
              </select>
              <select class="required" name="birthdaymonth">
                <?php
    for ($i=1; $i <= 12; $i++) {
        if($i !== intval($arrayDate[1])){
            ?>
                  <option value="<?php echo $i ?>">
                    <?php echo substr(date('F', mktime(0, 0, 0, $i, 10)),0,3) ?>
                  </option>
                  <?php
        } else {
            ?>
                    <option value="<?php echo $i ?>" selected>
                      <?php echo substr(date('F', mktime(0, 0, 0, $i, 10)),0,3) ?>
                    </option>
                    <?php
        }
    }
    ?>
              </select>
              <select class="required" name="birthdayyear">
                <?php
    for ($i=1940; $i < 2017; $i++) {
        if($i !== intval($arrayDate[2]) && $i !== 1980 ){
            ?>
                  <option value="<?php echo $i ?>">
                    <?php echo $i ?>
                  </option>
                  <?php
        } else {
            ?>
                    <option value="<?php echo $i ?>" selected>
                      <?php echo $i ?>
                    </option>
                    <?php
        }
    }
    ?>
              </select>
            </div>
            <div class="inputForm--select">
              <label>Challenges</label>
              <select name="challenge" class="required">
                <?php
    forEach($sChallenge as $val){
        if($val !== $novathonUser->challenge){
            ?>
                  <option value="<?php echo $val ?>">
                    <?php echo $val ?>
                  </option>
                  <?php
        } else {
            ?>
                    <option value="<?php echo $val ?>" selected>
                      <?php echo $val ?>
                    </option>
                    <?php
        }
    }
    ?>
              </select>
            </div>
            <div class="inputForm--select">
              <label>Role</label>
              <select name="role" class="required">
                <?php
    forEach($sRole as $val){
        if($val !== $role_value){
            ?>
                  <option value="<?php echo $val ?>">
                    <?php echo $val ?>
                  </option>
                  <?php
        } else {
            ?>
                    <option value="<?php echo $val ?>" selected>
                      <?php echo $val ?>
                    </option>
                    <?php
        }
    }
    ?>
              </select>
              <input placeholder="Please specify" style="<?php echo ($roleSelected ? " display: none; " : " ") ?>" data-related="role" class="required" type="text" name="role_other" value="<?php echo $novathonUser->role ?>">
            </div>
            <div class="inputForm--select">
              <label>Profession</label>
              <select name="profession" class="required">
                <?php
    forEach($sProfession as $val){
        if($val !== $profession_value){
            ?>
                  <option value="<?php echo $val ?>">
                    <?php echo $val ?>
                  </option>
                  <?php
        } else {
            ?>
                    <option value="<?php echo $val ?>" selected>
                      <?php echo $val ?>
                    </option>
                    <?php
        }
    }
    ?>
              </select>
              </select>
              <input placeholder="Please specify" class="required" style="<?php echo ($professionSelected ? " display: none; " : " ") ?>" data-related="profession" type="text" name="profession_other" value="<?php echo $novathonUser->profession ?>">

              <input placeholder="Company name" class="required" style="<?php echo ($novathonUser->profession != " Employed " ? " display: none; " : " ") ?>" type="text" name="companyname" value="<?php echo $novathonUser->companyname ?>">
            </div>
          </form>
          <div class="buttonWrap">
            <button class="buttonDialog cancel" type="button" name="cancel" data-action="edit-profile-close-dialog">Cancel</button>
            <button type="button" class="buttonDialog--confirm add" data-action="edit-profile-save">
              <?php if (!$edit) { ?>
                Add
                <?php } ?>
                  <?php if ($edit) { ?>
                    Edit
                    <?php } ?>
            </button>
          </div>
        </div>
        <div class="addSuccessful" data-target="add-partecipant-success" style="display:none">
          <img src="" alt="">
          <h3>New participant added successfully to <strong>Participants</strong></h3>
          <div class="buttonWrap">
            <button type="button" data-action="edit-profile-close-dialog" class="buttonDialog--confirm add" name="done">done</button>
          </div>
        </div>
        <div class="addWaitingList" data-target="add-partecipant-waiting" style="display:none">
          <img src="" alt="">
          <h3>New participant added successfully to <strong>Waiting list</strong></h3>
          <div class="buttonWrap">
            <button type="button" data-action="edit-profile-close-dialog" class="buttonDialog--confirm add" name="done">done</button>
          </div>
        </div>
      </div>

      <?php
    wp_die();
}

function bit_novathon_subscriptions_admin_create_complete_profile($email, $confirmed, $name, $lastname,
$profession, $role, $challenge, $birthday, $mobilephone, $companyname)
{
    global $total_max_subscribers_before_whitelist, $wordpress_subscriber_role;

    $password = wp_generate_password( $length=8, $include_standard_special_chars=false );

    $user_id = username_exists( $email );

    if ( !$user_id and email_exists($email) == false ) {
        $user_id = wp_create_user( $email, $password, $email );

        wp_update_user( array( 'ID' => $user_id, 'role' => $wordpress_subscriber_role ) );

        $sub = new Wordpress\ORM\Subscriber();

        $total = $sub->total_count();

        $sub->email_verified = 1;

        if ($total >= $total_max_subscribers_before_whitelist)
        {
            $sub->in_waitlist = 1;
            $sub->confirmed = $confirmed;
        }
        else
        {
            $sub->in_waitlist = 0;
            $sub->confirmed = $confirmed;
            $sub->confirm_date = time();
        }

        $sub->name = $name;
        $sub->last_name = $lastname;
        $sub->email = $email;
        $sub->created_date = time();
        $sub->wordpress_user_id = $user_id;
        $sub->profile_image_url = '';
        $sub->verify_guid = uniqid();
        $sub->profession = $profession;
        $sub->role = $role;
        $sub->challenge = $challenge;
        $sub->mobilephone = $mobilephone;
        $sub->birthday = $birthday;

        if ($sub->profession == "Employed")
            $sub->companyname = $companyname;
        else
            $sub->companyname = "";

        $sub->insert_complete_user($sub);

        return true;
    }
    else
        return false;
}

function bit_novathon_subscriptions_admin_edit_profile()
{
    $userid = intval($_POST["userid"]);
    $challenge = $_POST["challenge"];
    $mobilephone = $_POST["mobilephone"];
    $role = $_POST["role"];
    $birthdayDay = $_POST["birthdayday"];
    $birthdayMonth = $_POST["birthdaymonth"];
    $birthdayYear = $_POST["birthdayyear"];
    $name = $_POST["name"];
    $surname = $_POST["lastname"];
    $email = $_POST["email"];
    $profession = $_POST["profession"];
    $birthday = $birthdayDay . '/' . $birthdayMonth . '/' . $birthdayYear;
    $companyname = $_POST["companyname"];

    if (!bit_novathon_subscriptions_checkdate($birthday, $birthdayMonth, $birthdayYear))
    {
        echo json_encode(array("result" => false, "invaliddate" => true));

        wp_die();
    }

    if ($role == 'Other')
        $role = $_POST["role_other"];

    if ($profession == 'Other')
    {
        $profession = $_POST["profession_other"];
    }

    $sub = new Wordpress\ORM\Subscriber();

    if ($userid > -1)
    {
        $novathonUser = $sub->get_user_by_id($userid);
        $novathonUser->challenge = $challenge;
        $novathonUser->mobilephone = $mobilephone;
        $novathonUser->role = $role;
        $novathonUser->birthday = $birthday;
        $novathonUser->profession = $profession;
        $novathonUser->name = $name;
        $novathonUser->last_name = $surname;
        $novathonUser->email = $email;
        $novathonUser->id = $userid;
        $novathonUser->companyname = $companyname;

        $sub->update_profile($novathonUser);

        $response = array(
        "result" => true
        );
    }
    else
    {
        $addToWaitinglist = bit_novathon_subscriptions_check_total_is_over();

        //$email, $confirmed, $name, $lastname, $profession, $role, $challenge, $birthday, $mobilephone
        $saved = bit_novathon_subscriptions_admin_create_complete_profile($email, 0, $name, $surname, $profession,
        $role, $challenge, $birthday, $mobilephone, $companyname);

        if ($saved)
        {
            $response = array(
            "result" => true,
            "addedToWaitinglist" => $addToWaitinglist
            );
        }
        else
        {
            $response = array(
            "result" => false,
            "userExists" => true
            );
        }
    }

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_admin_add_to_participant()
{
    $usersArray = json_decode($_POST["users"]);

    //var_dump($usersArray);

    $not_added = 0;
    $added = 0;

    $sub = new Wordpress\ORM\Subscriber();

    foreach ($usersArray as $u)
    {
        if (bit_novathon_subscriptions_check_total_is_over())
        {
            $not_added++;
        }
        else
        {
            $sub->add_to_participant(intval($u));
            $added++;
        }
    }

    echo json_encode(array(
    "result" => true,
    "added" => $added,
    "not_added" => $not_added
    ));
    wp_die();
}

function bit_novathon_subscriptions_admin_export()
{
    //var_dump($_GET);

    $sub = new Wordpress\ORM\Subscriber();
    $textSearch = $_GET["textSearch"];
    $orderBy = $_GET["orderBy"];
    $orderByVersus = $_POST["orderByVersus"];
    $waitlist = $_GET["waitlist"] == "1" ? true : false;
    $status = $_GET["status"];

    $result = $sub->admin_search($waitlist, $orderBy, $orderByVersus, 1, $textSearch, $status, false);

    $total = $result["total_rows"];
    $list = $result["results"];

    //var_dump($total);

    $csv = "Name;LastName;Email;Confirmed;WaitList;Role;Profession;Challenge;Birthday;Phone" . PHP_EOL;

    foreach ($total as $key => $row)
    {
        $csv .= $row->name . ';';
        $csv .= $row->last_name . ';';
        $csv .= $row->email . ';';
        $csv .= $row->confirmed . ';';
        $csv .= $row->in_waitlist . ';';
        $csv .= $row->role . ';';
        $csv .= $row->profession . ';';
        $csv .= $row->challenge . ';';
        $csv .= $row->birthday . ';';
        $csv .= $row->mobilephone . PHP_EOL;

    }

    header('Content-Description: File Transfer');
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="export.csv"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header("Content-Transfer-Encoding: UTF-8");

    echo $csv;

    exit;
}

function bit_novathon_subscriptions_check_total_is_over()
{
    global $total_max_subscribers_before_whitelist;

    $sub = new Wordpress\ORM\Subscriber();
    $total = $sub->total_count();

    return $total >= $total_max_subscribers_before_whitelist;
}
?>
