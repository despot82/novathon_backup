<?php

/*
Plugin Name: Bitmama Novathon Contest Subscriptions
Description: A general WordPress plugin to manage form subscriptions
Version: 1.0
Author: Bitmama
Author URI: http://www.bitmama.it
*/

if (!function_exists('add_action')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

// global variables

$facebook_app_id = '717581245080040';
$facebook_app_secret = '7cbfe16d50e260a24ab751b027453acd';
$wordpress_subscriber_role = "novathon-subscriber";
$wordpress_subscriber_role_manager = "novathon-subscriber-manager";
$total_max_subscribers_before_whitelist = 180;
$recaptcha_public_key = '6LdRCx8UAAAAAP3t50bwKcTFML9qUdDeKjTlCkwp';
$recaptcha_private_key = '6LdRCx8UAAAAAGl-ymtDWembNcEcldPeY7qwERVr';


/*$twitter_consumer_key = "afc3FjNv7juCrkSvrWD6KlIJ3";
$twitter_consumer_secret = "Fw1b9gnRxVpthGMUOI7CJGE0TWhqk0MELRsWd5Wr6yDETE6rnr";
$twitter_oauth_callback = "";*/

// including libs and plugins
require_once __DIR__ . '/Facebook/autoload.php';
require_once __DIR__ . '/Models/Subscriber.php';
require_once __DIR__ . '/Emailer.php';
require_once __DIR__ . '/admin.php';
require_once __DIR__ . '/mpdf/vendor/autoload.php';
//require_once __DIR__ . '/recaptchalib.php';

// adding ajax actions

add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_register_facebook', 'bit_novathon_subscriptions_register_facebook' );
add_action( 'wp_ajax_bit_novathon_subscriptions_register_facebook', 'bit_novathon_subscriptions_register_facebook' );

add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_register_google', 'bit_novathon_subscriptions_register_google' );
add_action( 'wp_ajax_bit_novathon_subscriptions_register_google', 'bit_novathon_subscriptions_register_google' );

add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_register_standalone', 'bit_novathon_subscriptions_register_standalone' );
add_action( 'wp_ajax_bit_novathon_subscriptions_register_standalone', 'bit_novathon_subscriptions_register_standalone' );

add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_login_standalone', 'bit_novathon_subscriptions_login_standalone' );
add_action( 'wp_ajax_bit_novathon_subscriptions_login_standalone', 'bit_novathon_subscriptions_login_standalone' );

/*add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_register_twitter', 'bit_novathon_subscriptions_register_twitter' );
add_action( 'wp_ajax_bit_novathon_subscriptions_register_twitter', 'bit_novathon_subscriptions_register_twitter' );*/

add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_login_facebook', 'bit_novathon_subscriptions_login_facebook' );
add_action( 'wp_ajax_bit_novathon_subscriptions_login_facebook', 'bit_novathon_subscriptions_login_facebook' );

add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_login_google', 'bit_novathon_subscriptions_login_google' );
add_action( 'wp_ajax_bit_novathon_subscriptions_login_google', 'bit_novathon_subscriptions_login_google' );

add_action( 'wp_ajax_bit_novathon_subscriptions_complete_registration', 'bit_novathon_subscriptions_complete_registration' );

add_action( 'wp_ajax_bit_novathon_subscriptions_cancel_registration', 'bit_novathon_subscriptions_cancel_registration' );

add_action( 'wp_ajax_bit_novathon_subscriptions_send_ticket', 'bit_novathon_subscriptions_send_ticket' );

add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_reset_password', 'bit_novathon_subscriptions_reset_password' );
add_action( 'wp_ajax_bit_novathon_subscriptions_reset_password', 'bit_novathon_subscriptions_reset_password' );

add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_reset_password_confirm', 'bit_novathon_subscriptions_reset_password_confirm' );
add_action( 'wp_ajax_bit_novathon_subscriptions_reset_password_confirm', 'bit_novathon_subscriptions_reset_password_confirm' );

add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_resend_verify_email', 'bit_novathon_subscriptions_resend_verify_email' );
add_action( 'wp_ajax_bit_novathon_subscriptions_resend_verify_email', 'bit_novathon_subscriptions_resend_verify_email' );

add_action( 'wp_ajax_nopriv_bit_novathon_subscriptions_newsletter', 'bit_novathon_subscriptions_send_newsletter_email' );
add_action( 'wp_ajax_bit_novathon_subscriptions_newsletter', 'bit_novathon_subscriptions_send_newsletter_email' );

add_action( 'wp_ajax_bit_novathon_subscriptions_update_profile', 'bit_novathon_subscriptions_update_profile' );

add_action('admin_menu', 'bit_novathon_subscriptions_admin_add_menu');

add_action("wp_ajax_nopriv_gcr_check", "gcr_check");
add_action("wp_ajax_gcr_check", "gcr_check");

// adding hooks
register_activation_hook( __FILE__, 'bit_novathon_subscriptions_add_roles_on_plugin_activation' );

// init hook
function bit_novathon_subscriptions_add_roles_on_plugin_activation() {
    global $wordpress_subscriber_role, $wordpress_subscriber_role_manager;

    $adminRole = get_role('administrator');
    $adminRole->add_cap('can_manage_novathon_subscription');

    /*remove_role($wordpress_subscriber_role);
    remove_role($wordpress_subscriber_role_manager);*/

    /*add_role( $wordpress_subscriber_role, 'Novathon Subscriber', array( 'read' => true ) );
    add_role( $wordpress_subscriber_role_manager, 'Novathon Subscribers Manager', array( 'read' => true, "can_manage_novathon_subscription" => true ) );*/
}

/* SUBSCRIPTION */
function bit_novathon_subscriptions_register_facebook()
{
    header('Content-Type: application/json');

    $access_token = $_POST["accessToken"];

    try {
    	$user = bit_novathon_subscriptions_load_facebook_data($access_token);

        $email = $user["email"]; 
        
        if(empty($email)) {
        	$call_response = array(
        			"user_exists" => "",
        			"result" => false
        	);
        	echo json_encode($call_response);
        } else {
            $name = $user["first_name"];
            $lastname = $user["last_name"];
            $picture = $user["picture"]["url"];

            $user_exists = bit_novathon_subscriptions_register($email, "", $name, $lastname, $picture, "facebook");

            $call_response = array(
            "user_exists" => $user_exists,
            "result" => true
            );
            
            echo json_encode($call_response);
        }
        

    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        //echo 'Graph returned an error: ' . $e->getMessage();
        $call_response = array(
        		"user_exists" => "",
        		"result" => false,
        		"error" => $e->getMessage()
        );
        
        echo json_encode($call_response);  
        wp_die();
        
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        //echo 'Facebook SDK returned an error: ' . $e->getMessage();
        $call_response = array(
        	"user_exists" => "",
        	"result" => false,
        	"error" => $e->getMessage()
        );
        
        echo json_encode($call_response);
        wp_die();
        exit;
    }
    catch (Exception $e)
    {
        $call_response = array(
        "result" => false,
        "error" => $e->getMessage()
        );

        echo json_encode($call_response);
        wp_die();
    }

    wp_die();
}

function bit_novathon_subscriptions_load_facebook_data($access_token)
{
    global $facebook_app_id, $facebook_app_secret;

    $fb = new Facebook\Facebook([
    'app_id' => $facebook_app_id,
    'app_secret' => $facebook_app_secret,
    'default_graph_version' => 'v2.8',
    ]);

    $response = $fb->get('/me?fields=id,name,email,first_name,last_name,picture', $access_token);

    error_log("DEV:response to fb->get  is:<pre> " . print_r($response, 1) . "</pre>");

    $user = $response->getGraphUser();

    return $user;
}

function bit_novathon_subscriptions_register_google()
{
    header('Content-Type: application/json');

    $user = bit_novathon_subscriptions_load_google_data($_POST["token"]);

    $email = $user->email;
    $name = $user->given_name;
    $lastname = $user->family_name;
    $picture = $user->picture;

    $user_exists = bit_novathon_subscriptions_register($email, "", $name, $lastname, $picture, "google");

    $call_response = array(
    "user_exists" => $user_exists,
    "result" => true
    );

    echo json_encode($call_response);

    wp_die();
}

function bit_novathon_subscriptions_load_google_data($access_token)
{
    $url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' . $access_token;

    $json = file_get_contents($url);

    $user = json_decode($json);

    return $user;
}

/*function bit_novathon_subscriptions_register_twitter()
{
    global $twitter_consumer_key, $twitter_consumer_secret;

    try
    {
        $oauthCallback = "http://" . get_site_url() . "/twitter_oauth";

        $connection = new TwitterOAuth($twitter_consumer_key, $twitter_consumer_secret);
        $request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => $oauthCallback));

        setcookie('oauth_token', $request_token['oauth_token'], time() + (86400 * 30), "/"); // 86400 = 1 day
        setcookie('oauth_token_secret', $request_token['oauth_token_secret'], time() + (86400 * 30), "/"); // 86400 = 1 day

        $url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));

        //var_dump($request_token);

        $response = array(
        "result" => true,
        "oauth_url" => $url
        );

        echo json_encode($response);
    }
    catch (Exception $ex)
    {
        var_dump($ex);
    }

    wp_die();
}
*/
/*function bit_novathon_subscriptions_get_twitter_data()
{
    global $twitter_consumer_key, $twitter_consumer_secret;

    $request_token = [];
    $request_token['oauth_token'] = $_COOKIE['oauth_token'];
    $request_token['oauth_token_secret'] = $_COOKIE['oauth_token_secret'];

    $connection = new TwitterOAuth($twitter_consumer_key, $twitter_consumer_secret, $request_token['oauth_token'], $request_token['oauth_token_secret']);

    $access_token = $connection->oauth("oauth/access_token", ["oauth_verifier" => $_GET['oauth_verifier']]);

    //setcookie("access_token", $access_token, time() + (86400 * 30), "/");

    $connection = new TwitterOAuth($twitter_consumer_key, $twitter_consumer_secret, $access_token['oauth_token'], $access_token['oauth_token_secret']);

    $user = $connection->get("account/verify_credentials", [ "include_email" => true ]);

    var_dump($user);
}*/

function bit_novathon_subscriptions_register_standalone()
{
    try
    {
        $email = $_POST["email"];
        $password = $_POST["password"];
        $name = $_POST["name"];
        $lastname = $_POST["lastname"];

        $user_exists = bit_novathon_subscriptions_register($email, $password, $name, $lastname, "", "");

        $call_response = array(
        "user_exists" => $user_exists,
        "result" => true
        );

        echo json_encode($call_response);
    }
    catch (Exception $ex)
    {
        $call_response = array(
        "result" => false,
        "error" => $ex->getMessage()
        );

        echo json_encode($call_response);
    }

    wp_die();

}

function bit_novathon_subscriptions_register($email, $password, $name, $lastname, $profile_image_url, $service)
{
    $user_exists = false;

    global $wordpress_subscriber_role, $total_max_subscribers_before_whitelist;

    /*if ($service == "")
    {
        if (!bit_novathon_subscriptions_recaptcha_check_response())
        {
            echo json_encode(array("result" => false, "captchaerror" => true));

            wp_die();
        }
    }*/

    if ($password == "")
    {
        $password = wp_generate_password( $length=8, $include_standard_special_chars=false );
    }

    $username = $email;

    $user_id = username_exists( $username );

    if ( !$user_id and email_exists($email) == false ) {
        $user_id = wp_create_user( $username, $password, $email );

        wp_update_user( array( 'ID' => $user_id, 'role' => $wordpress_subscriber_role ) );

        $sub = new Wordpress\ORM\Subscriber();

        $total = $sub->total_count();

        if ($total >= $total_max_subscribers_before_whitelist)
        {
            $sub->in_waitlist = 1;
            $sub->confirmed = 0;
        }
        else
        {
            $sub->in_waitlist = 0;
            $sub->confirmed = 0;
            $sub->confirm_date = time();
        }

        $sub->name = $name;
        $sub->last_name = $lastname;
        $sub->email = $email;
        $sub->created_date = time();
        $sub->wordpress_user_id = $user_id;
        $sub->profile_image_url = $profile_image_url;
        $sub->verify_guid = uniqid();

        if ($service == "")
        {
            $sub->email_verified = 1;
        }
        else
        {
            $sub->email_verified = 1;
        }

        $sub->save();

        // if ($service == "")
        // {
        //     bit_novathon_subscriptions_send_verification_email($sub);
        // }
        //else
        //{
            bit_novathon_subscriptions_force_login_user($email, $password);
        //}

    } else {
        $user_exists = true;
    }

    return $user_exists;
}

function bit_novathon_subscriptions_force_login_user($email, $password)
{
    $creds = array();
    $creds['user_login'] = $email;
    $creds['user_password'] = $password;
    $creds['remember'] = true;
    $user = wp_signon( $creds, false );

    if ( is_wp_error($user) )
        echo $user->get_error_message();
}

function bit_novathon_subscriptions_send_verification_email($user)
{
    $mailer = new Emailer($user->email);
    $mailer->subject = "Verify your email";
    $template = new EmailTemplate(__DIR__ . '/MailTemplates/verify_email.php');
    $template->variables = array(
    "Host" => get_site_url(),
    "Guid" => $user->verify_guid,
    "Name" => $user->name,
    "Email" => md5($user->email),
    "Host" => get_site_url(),
    "AssetsUrl" => get_template_directory_uri()
    );

    $mailer->SetTemplate($template);

    $mailer->send();
}

function bit_novathon_subscriptions_verify_email()
{
    $result = false;

    $guid = $_GET["guid"];
    $email = $_GET["email"];

    $sub = new Wordpress\ORM\Subscriber();

    $user = $sub->get_user_by_verify_guid($guid);

    if ($user != NULL)
    {
        if ($email == md5($user->email))
        {
            $sub->set_email_verified($user->email, $guid);

            $result = true;
        }
    }

    return $result;
}

function bit_novathon_subscriptions_login_facebook()
{
    $access_token = $_POST["accessToken"];

    $user = bit_novathon_subscriptions_load_facebook_data($access_token);

    //var_dump($user);

    $wpUser = get_user_by('email', $user["email"]);

    //var_dump($wpUser);

    $response = array();

    if ( ! empty( $wpUser ) ) {

        wp_set_auth_cookie($wpUser->ID, true);

        $response = array(
        "exists" => true,
        "result" => true
        );
    }
    else
    {
        $response = array(
        "exists" => false,
        "result" => false
        );
    }

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_login_google()
{
    $user = bit_novathon_subscriptions_load_google_data($_POST["token"]);

    $wpUser = get_user_by('email', $user->email);

    //var_dump($wpUser);

    $response = array();

    if ( ! empty( $wpUser ) ) {

        wp_set_auth_cookie($wpUser->ID, true);

        $response = array(
        "exists" => true,
        "result" => true
        );
    }
    else
    {
        $response = array(
        "exists" => false,
        "result" => false
        );
    }

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_login_standalone()
{
    $creds = array();
    $creds['user_login'] = $_POST["email"];
    $creds['user_password'] = $_POST["password"];
    $creds['remember'] = true;
    $user = wp_signon( $creds, false );

    $response = array();

    if ( is_wp_error($user) )
    {
        $response = array(
        "error" => $user->get_error_message(),
        "result" => false
        );
    }
    else
    {
        $response = array(
        "result" => true
        );
    }

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_complete_registration()
{
    $challenge = $_POST["challenge"];
    $mobilephone = $_POST["mobilephone"];
    $role = $_POST["role"];
    $birthdayDay = $_POST["birthdayday"];
    $birthdayMonth = $_POST["birthdaymonth"];
    $birthdayYear = $_POST["birthdayyear"];
    $companyname = $_POST["companyname"];

    if (!bit_novathon_subscriptions_checkdate($birthdayDay, $birthdayMonth, $birthdayYear))
    {
        echo json_encode(array("invalidinput" => true, "invaliddate" => true));

        wp_die();
    }

    if ($role == 'Other')
        $role = $_POST["role_other"];

    $sub = new Wordpress\ORM\Subscriber();
    $wpUser = wp_get_current_user();
    $user = $sub->get_user_by_wordpress_user_id($wpUser->ID);

    $user->challenge = $challenge;
    $user->mobilephone = $mobilephone;
    $user->role = $role;
    $user->birthday = $birthdayDay . '/' . $birthdayMonth . '/' . $birthdayYear;
    $user->profession = $_POST["profession"];

    if ($user->profession == "Employed")
    {
        $user->companyname = $companyname;
    }
    else
    {
        $user->companyname = "";
    }

    if ($user->profession == 'Other')
    {
        $user->profession = $_POST["profession_other"];
    }

    $sub->update_profile($user);

    if($user->in_waitlist == 0){
      bit_novathon_subscriptions_send_ticket_email($user);
    }

    $response = array(
    "result" => true
    );

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_cancel_registration()
{
    $sub = new Wordpress\ORM\Subscriber();
    $wpUser = wp_get_current_user();
    $user = $sub->get_user_by_wordpress_user_id($wpUser->ID);

    wp_delete_user($wpUser->ID);
    $sub->delete_profile($user);
    wp_logout();

    bit_novathon_subscriptions_send_cancel_registration_email($user);

    $response = array(
    "result" => true
    );

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_send_cancel_registration_email($user)
{
    $mailer = new Emailer($user->email);
    $mailer->subject = "Registration cancelled";
    $template = new EmailTemplate(__DIR__ . '/MailTemplates/profile_cancelled.php');

    $template->variables = array(
    "Host" => get_site_url(),
    "Name" => $user->name,
    "AssetsUrl" => get_template_directory_uri()
    );
    $mailer->SetTemplate($template);

    $mailer->send();
}

function bit_novathon_subscriptions_send_ticket()
{
    $sub = new Wordpress\ORM\Subscriber();
    $wpUser = wp_get_current_user();
    $user = $sub->get_user_by_wordpress_user_id($wpUser->ID);

    bit_novathon_subscriptions_send_ticket_email($user);

    $response = array(
    "result" => true
    );

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_send_ticket_email($user)
{
	error_log("in bit_novathon_subscriptions_send_ticket_email");
	$mailer = new Emailer($user->email);
    $mailer->subject = "Your Registration";
    $template = new EmailTemplate(__DIR__ . '/MailTemplates/ticket_email.php');

    $template->variables = array(
    //"TicketURL" => bit_novathon_subscriptions_get_qr_code($user, 250, 250),
    "Name" => $user->name,
    "Host" => get_site_url(),
    "AssetsUrl" => get_template_directory_uri()
    );

    $mailer->SetTemplate($template);

    $mailer->send();
}

function bit_novathon_subscriptions_send_newsletter_email($user, $newsletter_ordinal)
{
	error_log("in sending the newsletter number $newsletter_ordinal");
	$mailer = new Emailer($user->email);
	$mailer->subject = "Your Newsletter";
	$template = new EmailTemplate(__DIR__ . "/MailTemplates/newsletter" . $newsletter_ordinal . ".php");
	
	$template->variables = array(
			//"TicketURL" => bit_novathon_subscriptions_get_qr_code($user, 250, 250),
			"Name" => $user->name,
			"Host" => get_site_url(),
			"AssetsUrl" => get_template_directory_uri()
	);
	
	$mailer->SetTemplate($template);
	
	$mailer->send();
}


function bit_novathon_subscriptions_get_qr_code($user, $w, $h)
{
    $qrParameters = urlencode($user->email) . "|" . urlencode($user->name) . "|" . urlencode($user->last_name) . "|" . urlencode($user->role);

    $url = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=" . $qrParameters . "&choe=UTF-8&chld=L|1";

    return $url;
}

function bit_novathon_subscriptions_reset_password()
{
    $email = $_POST["email"];

    $wpUser = get_user_by('email', $email);

    $sub = new Wordpress\ORM\Subscriber();

    if ($wpUser)
    {
        $user = $sub->get_user_by_wordpress_user_id($wpUser->ID);

        $guid = $sub->set_reset_password_guid($user);

        $mailer = new Emailer($user->email);
        $mailer->subject = "Reset your password";
        $template = new EmailTemplate(__DIR__ . '/MailTemplates/reset_password.php');

        //var_dump($guid);

        $template->variables = array(
        "Name" => $user->name,
        "Host" => get_site_url(),
        "Guid" => $guid,
        "EmailMD5" => md5($user->email),
        "AssetsUrl" => get_template_directory_uri()
        );

        $mailer->SetTemplate($template);

        $mailer->send();

        $response = array(
        "result" => true
        );
    }
    else
    {
        $response = array(
        "result" => false
        );
    }

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_reset_password_confirm()
{
    $email = $_POST["email"];
    $guid = $_POST["guid"];
    $newpassword = $_POST["newpassword"];
    $newpasswordconfirm = $_POST["newpasswordconfirm"];

    $response = array(
    "result" => false
    );

    if ($newpassword == $newpasswordconfirm)
    {
        $sub = new Wordpress\ORM\Subscriber();
        $user = $sub->get_user_by_reset_guid($guid);

        if ($user)
        {
            wp_set_password($newpassword, $user->wordpress_user_id);

            $response = array(
            "result" => true
            );
        }
    }

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_resend_verify_email()
{
    $sub = new Wordpress\ORM\Subscriber();
    $wpUser = wp_get_current_user();
    $user = $sub->get_user_by_wordpress_user_id($wpUser->ID);

    bit_novathon_subscriptions_send_verification_email($user);

    $response = array(
    "result" => true
    );

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_is_logged_in()
{
    return is_user_logged_in() && !current_user_can('administrator');
}

function bit_novathon_subscriptions_update_profile()
{
    $challenge = $_POST["challenge"];
    $mobilephone = $_POST["mobilephone"];
    $role = $_POST["role"];
    $birthdayDay = $_POST["birthdayday"];
    $birthdayMonth = $_POST["birthdaymonth"];
    $birthdayYear = $_POST["birthdayyear"];
    $companyname = $_POST["companyname"];

    if (!bit_novathon_subscriptions_checkdate($birthdayDay, $birthdayMonth, $birthdayYear))
    {
        echo json_encode(array("invalidinput" => true, "invaliddate" => true));

        wp_die();
    }

    if ($role == 'Other')
        $role = $_POST["role_other"];

    $sub = new Wordpress\ORM\Subscriber();
    $wpUser = wp_get_current_user();
    $user = $sub->get_user_by_wordpress_user_id($wpUser->ID);

    $user->challenge = $challenge;
    $user->mobilephone = $mobilephone;
    $user->role = $role;
    $user->birthday = $birthdayDay . '/' . $birthdayMonth . '/' . $birthdayYear;
    $user->profession = $_POST["profession"];

    if ($user->profession == "Employed")
    {
        $user->companyname = $companyname;
    }
    else
    {
        $user->companyname = "";
    }

    if ($user->profession == 'Other')
    {
        $user->profession = $_POST["profession_other"];
    }

    $sub->update_profile($user);

    $response = array(
    "result" => true
    );

    echo json_encode($response);

    wp_die();
}

function bit_novathon_subscriptions_download_ticket()
{
    $html = <<<EOS
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Novathon Ticket</title>
</head>
<body style="background-color: #f5f5f5;">
  <div style="padding: 10px; background-color: #b1c5cc;">
    <div style="float: left; width: 50%; padding-left: 30px;">
      <img src="{{AssetsUrl}}/images/logo.png" width="100" alt="Novathon logo">
    </div>
    <div style="float: left; widht: 30%; text-align: left; color: #ffffff; font-size:26px; line-height: 26px; padding-top: 40px;">
      THINK, INNOVATE AND CREATE<br>
      <span style="font-family: 'Open Sans'; font-weight: light; font-size:26px; color:#ffffff;">23-24 September 2017 - Zagreb</span>
    </div>
  </div>
  <div style="padding-top: 30px; padding-left: 30px; padding-right: 30px; background-color: #fff;">
    <div style="width: 59%; float: left; border-right: 2px solid #e6e6e6; height: 450px;">
      <h2 style="font-family: 'Open Sans'; font-weight: bold; color: #f36f20; font-size: 26px;margin: 0px; padding-bottom: 0px; line-height: 26px;">YOUR REGISTRATION</h2>
      <h3 style="font-family: 'Open Sans'; font-weight: light; font-size:20px; color:#000; margin-top: 0px;">NOVATHON #withPBZ</h3>
      <div style="color: #303030;">
        <div style="float: left; width: 50%;">
          <div style="margin-bottom: 40px;">
            <img style="width: 20px; float: left; margin-right: 10px; margin-top: 6px;" src="{{AssetsUrl}}/images/pdfImg/ic_profile.png" alt="">
            <div style="float: left; width: 100%;">
              <div style="font-size: 20px; margin: 0; padding: 0px; color: #303030;">PARTICIPANT</div>
              <div style="font-size: 16px; display: block; padding-left: 30px; font-weight: bold;">{{Name}}</div>
            </div>
          </div>
          <div style="margin-bottom: 60px;">
            <img style="width: 20px; float: left; margin-right: 10px; margin-top: 6px;" src="{{AssetsUrl}}/images/pdfImg/challenge.png" alt="">
            <div style="float: left; width: 100%;">
              <div style="font-size: 20px; margin: 0; padding: 0px; color: #303030;">CHALLENGE</div>
              <div style="font-size: 16px; display: block; padding-left: 30px; font-weight: bold;">{{Challenge}}</div>
            </div>
          </div>
          <div style="margin-bottom: 15px;">
            <img style="width: 20px; float: left; margin-right: 10px; margin-top: 6px;" src="{{AssetsUrl}}/images/pdfImg/Role.png" alt="">
            <div style="float: left; width: 100%;">
              <div style="font-size: 20px; margin: 0; padding: 0px; color: #303030;">ROLE</div>
              <div style="font-size: 16px; display: block; padding-left: 30px; font-weight: bold;">{{Role}}</div>
            </div>
          </div>
        </div>
        <div style="float: left; width: 50%;">
          <div style="margin-bottom: 15px;">
            <img style="width: 20px; float: left; margin-right: 10px; margin-top: 6px;" src="{{AssetsUrl}}/images/pdfImg/ic_calendar.png" alt="">
            <div style="float: left; width: 100%;">
              <div style="font-size: 20px; margin: 0; padding: 0px; color: #303030;">DATE AND TIME</div>
              <div style="font-size: 16px; display: block; padding-left: 30px; font-weight: bold;">23 - 24 September 2017<br>From 9:00 to 18:00</div>
            </div>
          </div>
          <div style="margin-bottom: 15px;">
            <img style="width: 20px; float: left; margin-right: 10px; margin-top: 6px;" src="{{AssetsUrl}}/images/pdfImg/ic_locator.png" alt="">
            <div style="float: left; width: 100%;">
              <div style="font-size: 20px; margin: 0; padding: 0px; color: #303030;">WHERE</div>
              <div style="font-size: 16px; display: block; padding-left: 30px; font-weight: bold;">LAUBA<br>Baruna Filipovića 23a, 10000<br>Zagreb - Croatia</div>
            </div>
          </div>
        </div>
      </div>
      <div style="float: left; border-top: 1px solid #e6e6e6;">
        <div style="float: left; width: 20%; color: #303030; padding-top: 15px">
          Next Steps
        </div>
        <div style="float: left; width: 70%; color: #303030; padding-top: 5px">
          <ul>
            <li>Your attendance will be evaluete by the organizers based on the eligibility criteria of the competition</li>
            <li>Upon the organizers final Confirmation, you will receive an e-mail asking you to check in at the event.</li>
            <li>Once you checked in, you will receive the participation ticket and will be able to create the team.</li>
          </ul>
        </div>
      </div>
    </div>
    <div style="width: 39%; float: left; color: #303030; padding-left: 1%;">
      <div>
        <h3 style="border-bottom: 1px solid #e6e6e6;">Notes/Instruction</h3>
        <p>Organizer of the competition is Privredna banka Zagreb d.d., OIB: 02535697732
          This ticket is issued through the sign up system via the web site <a title="novathon" href="http://www.novathon.net/" >www.novathon.net</a>.
          For possible reclamations please contact the Organizer.
          Unauthorized copying and duplication of tickets is prohibited. Any such action is subject to the criminal responsibility.
          Organizer is not responsible for lost items.</p>
      </div>
      <div>
        <h3 style="border-bottom: 1px solid #e6e6e6;">Gold Partners</h3>
        <img src="{{AssetsUrl}}/images/pdfImg/gold-partners.jpg" alt="gold partners" style="display: block; width: 100%;">
      </div>
      <div>
        <h3 style="border-bottom: 1px solid #e6e6e6;">Partners</h3>
        <img src="{{AssetsUrl}}/images/pdfImg/partners.jpg" width="300" alt="gold partners" style="display: block; width: 100%;">
      </div>
    </div>
  </div>
</body>
</html>
EOS;

    $sub = new Wordpress\ORM\Subscriber();
    $wpUser = wp_get_current_user();
    $user = $sub->get_user_by_wordpress_user_id($wpUser->ID);

    $mpdf = new mPDF('utf-8', 'A4-L', 9, 'opensans');
    $html = str_replace('{{AssetsUrl}}', get_template_directory_uri(), $html);
    $html = str_replace('{{Name}}', $user->name . ' ' . $user->last_name, $html);
    //$html = str_replace('{{QrCode}}', bit_novathon_subscriptions_get_qr_code($user, 250, 250), $html);
    $html = str_replace('{{Challenge}}' , $user->challenge , $html);
    $html = str_replace('{{Role}}' , $user->role , $html);
    $mpdf->WriteHTML($html);

    // Output a PDF file directly to the browser
    $mpdf->Output();
}

function bit_novathon_subscriptions_send_confirm_presence_email($user)
{
    $mailer = new Emailer($user->email);
    $mailer->subject = "Confirm your presence";
    $template = new EmailTemplate(__DIR__ . '/MailTemplates/confirm_presence.php');

    $template->variables = array(
    "Name" => $user->name,
    "Host" => get_site_url(),
    "AssetsUrl" => get_template_directory_uri()
    );

    $mailer->SetTemplate($template);

    $mailer->send();
}

function bit_novathon_subscriptions_send_cancel_registration_email_admin($user)
{
    $mailer = new Emailer($user->email);
    $mailer->subject = "Registration cancelled";
    $template = new EmailTemplate(__DIR__ . '/MailTemplates/profile_cancelled_admin.php');

    $template->variables = array(
    "Host" => get_site_url(),
    "Name" => $user->name,
    "AssetsUrl" => get_template_directory_uri()
    );
    $mailer->SetTemplate($template);

    $mailer->send();
}

function bit_novathon_subscriptions_checkdate($day, $month, $year)
{
    return checkdate($month, $day, $year);
}

function bit_novathon_subscriptions_recaptcha_check_response()
{
    global $recaptcha_private_key;

    $captcha=$_POST['g-recaptcha-response'];

    $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $recaptcha_private_key . "&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

    if($response['success'] == false)
        {
            return false;
        }
        else
        {
          return true;
        }
}

function gcr_check() {
	
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	header("Access-Control-Allow-Headers: X-Requested-With");
	
	if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) && !$_GET['cors']) {
		header('location: /'); // not an ajax request
	} else {
		
		//header('Content-type: application/json');
		$url='https://www.google.com/recaptcha/api/siteverify';
		
		$response=$_POST['g-recaptcha-response'];
		
		$secret = "6LfoUyMUAAAAAG1l_B-WoRE78XwJsV0pUM9r2vaI";
		$params = array('secret'=> $secret, 'response'=> $response);
		$json=file_get_contents( $url  . '?secret=' . $secret . '&response=' . $response);
		
		//error_log(print_r($json, true));
		
		echo $json;wp_die();
	}
}
?>
