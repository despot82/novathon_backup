<?php

class EmailTemplate
{
    public $variables = array();

    var $path_to_file= array();

    function __construct($path_to_file)
    {
        if(!file_exists($path_to_file))
        {
            trigger_error('Template File not found!',E_USER_ERROR);
            return;
        }
        $this->path_to_file = $path_to_file;
    }

    public function compile()
    {
        $template = file_get_contents($this->path_to_file);

        foreach($this->variables as $key => $value)
        {
            $template = str_replace('{{'.$key.'}}', $value, $template);
        }

        //var_dump($template);

        return $template;
    }
}

class Emailer
{
    var $recipients = array();
    var $to = '';
    var $subject = '';
    var $EmailTemplate;
    var $EmailContents;

    public function __construct($to = false)
    {
        $this->to = $to;
    }

    public function SetTemplate(EmailTemplate $EmailTemplate)
    {
        $this->EmailTemplate = $EmailTemplate;
    }

    public function send()
    {
        $body = $this->EmailTemplate->compile();

        $headers  = "From: Novathon <info@novathon.net>\n";
        //$headers .= "Cc: shardick <shardick@gmail.com>\n";
        $headers .= "X-Sender: Novathon <info@novathon.net>\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\n";

        mail($this->to, $this->subject, $body, $headers);
    }
}

?>
