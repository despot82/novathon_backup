<?php

require( dirname(__FILE__) . '/../wp-load.php' );


$mail_sent = FALSE;
$res = array();
$secret = get_option("_bitnovathon_recaptcha_secret");
$mail_to = get_option("_bitnovathon_mail_to");

if (($secret || empty($secret)) && ($mail_to || empty($mail_to))) {
  $name = $_POST['name'];
  $email = $_POST['email'];
  $comment = $_POST['comment'];
  $g_recaptcha_response = $_POST['g-recaptcha-response'];
  $ip = get_client_ip();

//get verify response data
  $verifyResponse = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$g_recaptcha_response}&remoteip={$ip}");
  $google_response = json_decode($verifyResponse, TRUE);

  $res["success"] = $google_response['success'];
  $res["message"] = $google_response['success'] ? "ok" : "recaptcha validation failed";

  if ($google_response['success']) {
    $mail_sent = send_mail($mail_to, $name, $email, $comment);
    if (!$mail_sent) {
      $res["success"] = false;
      $res["message"] = "mail not sent";
    }
  } else {
    $res["message"] = "recaptcha validation failed";
  }
} else {
  $res["success"] = false;
  $res["message"] = "recaptcha secret not set";
}


die(json_encode($res));

function get_client_ip() {
  $ipaddress = '';
  if (getenv('HTTP_CLIENT_IP'))
    $ipaddress = getenv('HTTP_CLIENT_IP');
  else if (getenv('HTTP_X_FORWARDED_FOR'))
    $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
  else if (getenv('HTTP_X_FORWARDED'))
    $ipaddress = getenv('HTTP_X_FORWARDED');
  else if (getenv('HTTP_FORWARDED_FOR'))
    $ipaddress = getenv('HTTP_FORWARDED_FOR');
  else if (getenv('HTTP_FORWARDED'))
    $ipaddress = getenv('HTTP_FORWARDED');
  else if (getenv('REMOTE_ADDR'))
    $ipaddress = getenv('REMOTE_ADDR');
  else
    $ipaddress = 'UNKNOWN';
  return $ipaddress;
}

function send_mail($mail_to, $name, $email, $comment) {
  return wp_mail($mail_to, "A new contact message has been sent through Novathon", sprintf("Sent by: %s (%s). Message: %s", $name, $email, $comment));
}
